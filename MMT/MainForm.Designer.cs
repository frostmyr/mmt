﻿namespace MMT
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem(new string[] {
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""}, -1);
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem(new string[] {
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""}, -1);
            System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem(new string[] {
            "",
            "",
            "",
            "",
            "",
            ""}, -1);
            System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem(new string[] {
            "",
            "",
            "",
            "",
            "",
            ""}, -1);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem(new string[] {
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""}, -1);
            System.Windows.Forms.ListViewItem listViewItem6 = new System.Windows.Forms.ListViewItem(new string[] {
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""}, -1);
            this.PanelJualBeli = new System.Windows.Forms.Panel();
            this.JBPanelFooter = new System.Windows.Forms.Panel();
            this.JBLabelJumlahJual = new System.Windows.Forms.Label();
            this.JBLabelJumlahNettoJual = new System.Windows.Forms.Label();
            this.JBLabelJumlahBeli = new System.Windows.Forms.Label();
            this.JBLabelJumlahNettoBeli = new System.Windows.Forms.Label();
            this.JBLabelRugi = new System.Windows.Forms.Label();
            this.JBLabelLabaBersih = new System.Windows.Forms.Label();
            this.JBLabelLaba = new System.Windows.Forms.Label();
            this.JBLabelSisa = new System.Windows.Forms.Label();
            this.JBLabelJumlah = new System.Windows.Forms.Label();
            this.JBUnderlineTableJual = new System.Windows.Forms.Panel();
            this.JBUnderlineTableBeli = new System.Windows.Forms.Panel();
            this.JBLabelTerjual = new System.Windows.Forms.Label();
            this.JBPanelHeader = new System.Windows.Forms.Panel();
            this.JBHeaderPenjualan = new System.Windows.Forms.ListView();
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader19 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.JBLabelPembelian = new System.Windows.Forms.Label();
            this.JBLabelPenjualan = new System.Windows.Forms.Label();
            this.JBHeaderPembelian = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader48 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.JBPanelButton = new System.Windows.Forms.Panel();
            this.JBPanelFilter = new System.Windows.Forms.Panel();
            this.JBFilterBox2Block2 = new System.Windows.Forms.TextBox();
            this.JBFilterBox2Block1 = new System.Windows.Forms.TextBox();
            this.JBFilterBox1Block2 = new System.Windows.Forms.TextBox();
            this.JBFilterBox1Block1 = new System.Windows.Forms.TextBox();
            this.JBFilterDropdown = new System.Windows.Forms.ComboBox();
            this.JBFilterGroup1 = new System.Windows.Forms.GroupBox();
            this.JBFilterGroup1Opt4 = new System.Windows.Forms.RadioButton();
            this.JBFilterGroup1Opt3 = new System.Windows.Forms.RadioButton();
            this.JBFilterGroup1Opt2 = new System.Windows.Forms.RadioButton();
            this.JBFilterGroup1Opt1 = new System.Windows.Forms.RadioButton();
            this.JBFilterButtonSearch = new System.Windows.Forms.Button();
            this.JBFilterPicker2 = new System.Windows.Forms.DateTimePicker();
            this.JBFilterPicker1 = new System.Windows.Forms.DateTimePicker();
            this.JBFilterLabel4 = new System.Windows.Forms.Label();
            this.JBFilterLabel3 = new System.Windows.Forms.Label();
            this.JBFilterLabel2 = new System.Windows.Forms.Label();
            this.JBFilterLabel1 = new System.Windows.Forms.Label();
            this.JBFilterMode = new System.Windows.Forms.ComboBox();
            this.JBFilterGroup2 = new System.Windows.Forms.GroupBox();
            this.JBFilterGroup2Opt3 = new System.Windows.Forms.RadioButton();
            this.JBFilterGroup2Opt2 = new System.Windows.Forms.RadioButton();
            this.JBFilterGroup2Opt1 = new System.Windows.Forms.RadioButton();
            this.JBButtonAutoSize = new System.Windows.Forms.Button();
            this.JBIconKeyboard = new System.Windows.Forms.Button();
            this.JBButtonLaporan = new System.Windows.Forms.Button();
            this.JBButtonFilter = new System.Windows.Forms.Button();
            this.JBButtonSimpan = new System.Windows.Forms.Button();
            this.JBPanelContent = new System.Windows.Forms.Panel();
            this.JBTablePembelian = new System.Windows.Forms.ListView();
            this.columnHeader23 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader24 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader25 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader26 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader27 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader28 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader29 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader30 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader31 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader33 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader49 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader20 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.JBScroll = new System.Windows.Forms.VScrollBar();
            this.JBTablePenjualan = new System.Windows.Forms.ListView();
            this.columnHeader41 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader42 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader43 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader44 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader45 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader46 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader47 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader66 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TabButtonJualBeli = new System.Windows.Forms.Button();
            this.TabButtonPemasukan = new System.Windows.Forms.Button();
            this.PanelPemasukan = new System.Windows.Forms.Panel();
            this.TabButtonPetani = new System.Windows.Forms.Button();
            this.PanelPetani = new System.Windows.Forms.Panel();
            this.PPPanelContent = new System.Windows.Forms.Panel();
            this.PPScroll = new System.Windows.Forms.VScrollBar();
            this.PPTablePembeli = new System.Windows.Forms.ListView();
            this.columnHeader60 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader61 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader62 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader63 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.PPTablePetani = new System.Windows.Forms.ListView();
            this.columnHeader50 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader51 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader52 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.PPPanelHeader = new System.Windows.Forms.Panel();
            this.PPButtonSearchPembeli = new System.Windows.Forms.Button();
            this.PPInputSearchPembeli = new System.Windows.Forms.TextBox();
            this.PPButtonSearchPetani = new System.Windows.Forms.Button();
            this.PPInputSearchPetani = new System.Windows.Forms.TextBox();
            this.PPLabelPembeli = new System.Windows.Forms.Label();
            this.PPLabelPetani = new System.Windows.Forms.Label();
            this.PPHeaderPembeli = new System.Windows.Forms.ListView();
            this.columnHeader53 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader54 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader55 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader56 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.PPHeaderPetani = new System.Windows.Forms.ListView();
            this.columnHeader57 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader58 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader59 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TabButtonSetting = new System.Windows.Forms.Button();
            this.PanelSetting = new System.Windows.Forms.Panel();
            this.TabBlocker = new System.Windows.Forms.Panel();
            this.PanelTitleBar = new System.Windows.Forms.Panel();
            this.LabelTitle = new System.Windows.Forms.Label();
            this.ButtonMinimize = new System.Windows.Forms.Button();
            this.ButtonClose = new System.Windows.Forms.Button();
            this.PanelBlocker = new System.Windows.Forms.Panel();
            this.TooltipsKeyboard = new System.Windows.Forms.Label();
            this.TabButtonHutang = new System.Windows.Forms.Button();
            this.PanelHutang = new System.Windows.Forms.Panel();
            this.HPPanelFooter = new System.Windows.Forms.Panel();
            this.HPLabelJumlahPiutang = new System.Windows.Forms.Label();
            this.HPLabelJumlahHutang = new System.Windows.Forms.Label();
            this.HPLabelJumlahSisaPiutang = new System.Windows.Forms.Label();
            this.HPLabelJumlahSisaHutang = new System.Windows.Forms.Label();
            this.HPUnderlineTablePiutang = new System.Windows.Forms.Panel();
            this.HPUnderlineTableHutang = new System.Windows.Forms.Panel();
            this.HPPanelContent = new System.Windows.Forms.Panel();
            this.HPTablePiutang = new System.Windows.Forms.ListView();
            this.columnHeader77 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader78 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader79 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader80 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader81 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader82 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader83 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader84 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.HPScroll = new System.Windows.Forms.VScrollBar();
            this.HPTableHutang = new System.Windows.Forms.ListView();
            this.columnHeader89 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader90 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader91 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader92 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader93 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader94 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader95 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.HPPanelButton = new System.Windows.Forms.Panel();
            this.HPPanelFilter = new System.Windows.Forms.Panel();
            this.HPFilterGroup1 = new System.Windows.Forms.GroupBox();
            this.HPFilterGroup1Opt4 = new System.Windows.Forms.RadioButton();
            this.HPFilterGroup1Opt3 = new System.Windows.Forms.RadioButton();
            this.HPFilterGroup1Opt2 = new System.Windows.Forms.RadioButton();
            this.HPFilterGroup1Opt1 = new System.Windows.Forms.RadioButton();
            this.HPFilterBox2 = new System.Windows.Forms.TextBox();
            this.HPFilterBox1 = new System.Windows.Forms.TextBox();
            this.HPFilterButtonSearch = new System.Windows.Forms.Button();
            this.HPFilterPicker2 = new System.Windows.Forms.DateTimePicker();
            this.HPFilterPicker1 = new System.Windows.Forms.DateTimePicker();
            this.HPFilterLabel4 = new System.Windows.Forms.Label();
            this.HPFilterLabel3 = new System.Windows.Forms.Label();
            this.HPFilterLabel2 = new System.Windows.Forms.Label();
            this.HPFilterLabel1 = new System.Windows.Forms.Label();
            this.HPFilterMode = new System.Windows.Forms.ComboBox();
            this.HPFilterGroup2 = new System.Windows.Forms.GroupBox();
            this.HPFilterGroup2Opt3 = new System.Windows.Forms.RadioButton();
            this.HPFilterGroup2Opt2 = new System.Windows.Forms.RadioButton();
            this.HPFilterGroup2Opt1 = new System.Windows.Forms.RadioButton();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.HPButtonAutoSize = new System.Windows.Forms.Button();
            this.HPIconKeyboard = new System.Windows.Forms.Button();
            this.HPButtonLaporan = new System.Windows.Forms.Button();
            this.HPButtonFilter = new System.Windows.Forms.Button();
            this.HPButtonSimpan = new System.Windows.Forms.Button();
            this.HPPanelHeader = new System.Windows.Forms.Panel();
            this.HPLabelPiutang = new System.Windows.Forms.Label();
            this.HPLabelHutang = new System.Windows.Forms.Label();
            this.HPHeaderPiutang = new System.Windows.Forms.ListView();
            this.columnHeader32 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader34 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader35 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader36 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader37 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader38 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader39 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader40 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.HPHeaderHutang = new System.Windows.Forms.ListView();
            this.columnHeader64 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader65 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader67 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader68 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader69 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader70 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader71 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.LabelScroll = new System.Windows.Forms.Label();
            this.TimerRefreshFooter = new System.Windows.Forms.Timer(this.components);
            this.TabButtonFitur = new System.Windows.Forms.Button();
            this.PanelFitur = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.PanelTitleBarCorner = new System.Windows.Forms.PictureBox();
            this.NNTableTyam = new System.Windows.Forms.ListView();
            this.columnHeader21 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader22 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.PanelJualBeli.SuspendLayout();
            this.JBPanelFooter.SuspendLayout();
            this.JBPanelHeader.SuspendLayout();
            this.JBPanelButton.SuspendLayout();
            this.JBPanelFilter.SuspendLayout();
            this.JBFilterGroup1.SuspendLayout();
            this.JBFilterGroup2.SuspendLayout();
            this.JBPanelContent.SuspendLayout();
            this.PanelPetani.SuspendLayout();
            this.PPPanelContent.SuspendLayout();
            this.PPPanelHeader.SuspendLayout();
            this.PanelHutang.SuspendLayout();
            this.HPPanelFooter.SuspendLayout();
            this.HPPanelContent.SuspendLayout();
            this.HPPanelButton.SuspendLayout();
            this.HPPanelFilter.SuspendLayout();
            this.HPFilterGroup1.SuspendLayout();
            this.HPFilterGroup2.SuspendLayout();
            this.HPPanelHeader.SuspendLayout();
            this.PanelFitur.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PanelTitleBarCorner)).BeginInit();
            this.SuspendLayout();
            // 
            // PanelJualBeli
            // 
            this.PanelJualBeli.BackColor = System.Drawing.Color.White;
            this.PanelJualBeli.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelJualBeli.Controls.Add(this.JBPanelFooter);
            this.PanelJualBeli.Controls.Add(this.JBPanelHeader);
            this.PanelJualBeli.Controls.Add(this.JBPanelButton);
            this.PanelJualBeli.Controls.Add(this.JBPanelContent);
            this.PanelJualBeli.Location = new System.Drawing.Point(45, 43);
            this.PanelJualBeli.Name = "PanelJualBeli";
            this.PanelJualBeli.Size = new System.Drawing.Size(572, 215);
            this.PanelJualBeli.TabIndex = 0;
            // 
            // JBPanelFooter
            // 
            this.JBPanelFooter.Controls.Add(this.JBLabelJumlahJual);
            this.JBPanelFooter.Controls.Add(this.JBLabelJumlahNettoJual);
            this.JBPanelFooter.Controls.Add(this.JBLabelJumlahBeli);
            this.JBPanelFooter.Controls.Add(this.JBLabelJumlahNettoBeli);
            this.JBPanelFooter.Controls.Add(this.JBLabelRugi);
            this.JBPanelFooter.Controls.Add(this.JBLabelLabaBersih);
            this.JBPanelFooter.Controls.Add(this.JBLabelLaba);
            this.JBPanelFooter.Controls.Add(this.JBLabelSisa);
            this.JBPanelFooter.Controls.Add(this.JBLabelJumlah);
            this.JBPanelFooter.Controls.Add(this.JBUnderlineTableJual);
            this.JBPanelFooter.Controls.Add(this.JBUnderlineTableBeli);
            this.JBPanelFooter.Controls.Add(this.JBLabelTerjual);
            this.JBPanelFooter.Location = new System.Drawing.Point(16, 182);
            this.JBPanelFooter.Name = "JBPanelFooter";
            this.JBPanelFooter.Size = new System.Drawing.Size(518, 32);
            this.JBPanelFooter.TabIndex = 8;
            // 
            // JBLabelJumlahJual
            // 
            this.JBLabelJumlahJual.AutoSize = true;
            this.JBLabelJumlahJual.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.JBLabelJumlahJual.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JBLabelJumlahJual.Location = new System.Drawing.Point(174, 15);
            this.JBLabelJumlahJual.Name = "JBLabelJumlahJual";
            this.JBLabelJumlahJual.Size = new System.Drawing.Size(18, 20);
            this.JBLabelJumlahJual.TabIndex = 3;
            this.JBLabelJumlahJual.Text = "0";
            // 
            // JBLabelJumlahNettoJual
            // 
            this.JBLabelJumlahNettoJual.AutoSize = true;
            this.JBLabelJumlahNettoJual.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.JBLabelJumlahNettoJual.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JBLabelJumlahNettoJual.Location = new System.Drawing.Point(156, 15);
            this.JBLabelJumlahNettoJual.Name = "JBLabelJumlahNettoJual";
            this.JBLabelJumlahNettoJual.Size = new System.Drawing.Size(18, 20);
            this.JBLabelJumlahNettoJual.TabIndex = 2;
            this.JBLabelJumlahNettoJual.Text = "0";
            // 
            // JBLabelJumlahBeli
            // 
            this.JBLabelJumlahBeli.AutoSize = true;
            this.JBLabelJumlahBeli.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.JBLabelJumlahBeli.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JBLabelJumlahBeli.Location = new System.Drawing.Point(137, 15);
            this.JBLabelJumlahBeli.Name = "JBLabelJumlahBeli";
            this.JBLabelJumlahBeli.Size = new System.Drawing.Size(18, 20);
            this.JBLabelJumlahBeli.TabIndex = 1;
            this.JBLabelJumlahBeli.Text = "0";
            // 
            // JBLabelJumlahNettoBeli
            // 
            this.JBLabelJumlahNettoBeli.AutoSize = true;
            this.JBLabelJumlahNettoBeli.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.JBLabelJumlahNettoBeli.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JBLabelJumlahNettoBeli.Location = new System.Drawing.Point(118, 15);
            this.JBLabelJumlahNettoBeli.Name = "JBLabelJumlahNettoBeli";
            this.JBLabelJumlahNettoBeli.Size = new System.Drawing.Size(18, 20);
            this.JBLabelJumlahNettoBeli.TabIndex = 0;
            this.JBLabelJumlahNettoBeli.Text = "0";
            // 
            // JBLabelRugi
            // 
            this.JBLabelRugi.AutoSize = true;
            this.JBLabelRugi.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JBLabelRugi.Location = new System.Drawing.Point(349, 21);
            this.JBLabelRugi.Name = "JBLabelRugi";
            this.JBLabelRugi.Size = new System.Drawing.Size(167, 18);
            this.JBLabelRugi.TabIndex = 11;
            this.JBLabelRugi.Text = "Rugi: 0 keranjang (Rp 0)";
            // 
            // JBLabelLabaBersih
            // 
            this.JBLabelLabaBersih.AutoSize = true;
            this.JBLabelLabaBersih.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JBLabelLabaBersih.Location = new System.Drawing.Point(478, 8);
            this.JBLabelLabaBersih.Name = "JBLabelLabaBersih";
            this.JBLabelLabaBersih.Size = new System.Drawing.Size(123, 18);
            this.JBLabelLabaBersih.TabIndex = 10;
            this.JBLabelLabaBersih.Text = "Laba bersih: Rp 0";
            // 
            // JBLabelLaba
            // 
            this.JBLabelLaba.AutoSize = true;
            this.JBLabelLaba.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JBLabelLaba.Location = new System.Drawing.Point(347, 8);
            this.JBLabelLaba.Name = "JBLabelLaba";
            this.JBLabelLaba.Size = new System.Drawing.Size(169, 18);
            this.JBLabelLaba.TabIndex = 9;
            this.JBLabelLaba.Text = "Laba: 0 keranjang (Rp 0)";
            // 
            // JBLabelSisa
            // 
            this.JBLabelSisa.AutoSize = true;
            this.JBLabelSisa.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JBLabelSisa.Location = new System.Drawing.Point(125, 21);
            this.JBLabelSisa.Name = "JBLabelSisa";
            this.JBLabelSisa.Size = new System.Drawing.Size(166, 18);
            this.JBLabelSisa.TabIndex = 8;
            this.JBLabelSisa.Text = "Sisa: 0 keranjang (Rp 0)";
            // 
            // JBLabelJumlah
            // 
            this.JBLabelJumlah.AutoSize = true;
            this.JBLabelJumlah.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JBLabelJumlah.Location = new System.Drawing.Point(17, 8);
            this.JBLabelJumlah.Name = "JBLabelJumlah";
            this.JBLabelJumlah.Size = new System.Drawing.Size(140, 18);
            this.JBLabelJumlah.TabIndex = 6;
            this.JBLabelJumlah.Text = "Jumlah: 0 keranjang";
            // 
            // JBUnderlineTableJual
            // 
            this.JBUnderlineTableJual.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.JBUnderlineTableJual.Location = new System.Drawing.Point(246, 4);
            this.JBUnderlineTableJual.Name = "JBUnderlineTableJual";
            this.JBUnderlineTableJual.Size = new System.Drawing.Size(200, 1);
            this.JBUnderlineTableJual.TabIndex = 5;
            // 
            // JBUnderlineTableBeli
            // 
            this.JBUnderlineTableBeli.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.JBUnderlineTableBeli.Location = new System.Drawing.Point(9, 4);
            this.JBUnderlineTableBeli.Name = "JBUnderlineTableBeli";
            this.JBUnderlineTableBeli.Size = new System.Drawing.Size(200, 1);
            this.JBUnderlineTableBeli.TabIndex = 4;
            // 
            // JBLabelTerjual
            // 
            this.JBLabelTerjual.AutoSize = true;
            this.JBLabelTerjual.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JBLabelTerjual.Location = new System.Drawing.Point(125, 8);
            this.JBLabelTerjual.Name = "JBLabelTerjual";
            this.JBLabelTerjual.Size = new System.Drawing.Size(181, 18);
            this.JBLabelTerjual.TabIndex = 7;
            this.JBLabelTerjual.Text = "Terjual: 0 keranjang (Rp 0)";
            // 
            // JBPanelHeader
            // 
            this.JBPanelHeader.BackColor = System.Drawing.Color.White;
            this.JBPanelHeader.Controls.Add(this.JBHeaderPenjualan);
            this.JBPanelHeader.Controls.Add(this.JBLabelPembelian);
            this.JBPanelHeader.Controls.Add(this.JBLabelPenjualan);
            this.JBPanelHeader.Controls.Add(this.JBHeaderPembelian);
            this.JBPanelHeader.Location = new System.Drawing.Point(16, 100);
            this.JBPanelHeader.Name = "JBPanelHeader";
            this.JBPanelHeader.Size = new System.Drawing.Size(514, 40);
            this.JBPanelHeader.TabIndex = 5;
            // 
            // JBHeaderPenjualan
            // 
            this.JBHeaderPenjualan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.JBHeaderPenjualan.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader12,
            this.columnHeader13,
            this.columnHeader14,
            this.columnHeader15,
            this.columnHeader16,
            this.columnHeader17,
            this.columnHeader18,
            this.columnHeader19});
            this.JBHeaderPenjualan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JBHeaderPenjualan.FullRowSelect = true;
            this.JBHeaderPenjualan.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.JBHeaderPenjualan.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1});
            this.JBHeaderPenjualan.Location = new System.Drawing.Point(41, 20);
            this.JBHeaderPenjualan.Name = "JBHeaderPenjualan";
            this.JBHeaderPenjualan.Scrollable = false;
            this.JBHeaderPenjualan.Size = new System.Drawing.Size(535, 175);
            this.JBHeaderPenjualan.TabIndex = 4;
            this.JBHeaderPenjualan.TabStop = false;
            this.JBHeaderPenjualan.UseCompatibleStateImageBehavior = false;
            this.JBHeaderPenjualan.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Tanggal";
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Pembeli";
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "Bruto";
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "Tembak";
            this.columnHeader15.Width = 56;
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "Netto";
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "Harga";
            // 
            // columnHeader18
            // 
            this.columnHeader18.Text = "Jumlah";
            // 
            // columnHeader19
            // 
            this.columnHeader19.Text = "Laba/Rugi";
            // 
            // JBLabelPembelian
            // 
            this.JBLabelPembelian.BackColor = System.Drawing.Color.White;
            this.JBLabelPembelian.Font = new System.Drawing.Font("Palatino Linotype", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JBLabelPembelian.Location = new System.Drawing.Point(4, 7);
            this.JBLabelPembelian.Name = "JBLabelPembelian";
            this.JBLabelPembelian.Size = new System.Drawing.Size(100, 23);
            this.JBLabelPembelian.TabIndex = 2;
            this.JBLabelPembelian.Text = "Pembelian";
            this.JBLabelPembelian.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // JBLabelPenjualan
            // 
            this.JBLabelPenjualan.BackColor = System.Drawing.Color.White;
            this.JBLabelPenjualan.Font = new System.Drawing.Font("Palatino Linotype", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JBLabelPenjualan.Location = new System.Drawing.Point(120, 7);
            this.JBLabelPenjualan.Name = "JBLabelPenjualan";
            this.JBLabelPenjualan.Size = new System.Drawing.Size(113, 28);
            this.JBLabelPenjualan.TabIndex = 3;
            this.JBLabelPenjualan.Text = "Penjualan";
            this.JBLabelPenjualan.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // JBHeaderPembelian
            // 
            this.JBHeaderPembelian.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.JBHeaderPembelian.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader10,
            this.columnHeader48,
            this.columnHeader11});
            this.JBHeaderPembelian.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JBHeaderPembelian.FullRowSelect = true;
            this.JBHeaderPembelian.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.JBHeaderPembelian.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem2});
            this.JBHeaderPembelian.Location = new System.Drawing.Point(6, 6);
            this.JBHeaderPembelian.Name = "JBHeaderPembelian";
            this.JBHeaderPembelian.Scrollable = false;
            this.JBHeaderPembelian.Size = new System.Drawing.Size(535, 175);
            this.JBHeaderPembelian.TabIndex = 0;
            this.JBHeaderPembelian.TabStop = false;
            this.JBHeaderPembelian.UseCompatibleStateImageBehavior = false;
            this.JBHeaderPembelian.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "No";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Tanggal";
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Nama";
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Desa";
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Pembeli";
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Bruto";
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Tembak";
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Netto";
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Tyam";
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Harga";
            // 
            // columnHeader48
            // 
            this.columnHeader48.Text = "Tumplek";
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Jumlah";
            // 
            // JBPanelButton
            // 
            this.JBPanelButton.Controls.Add(this.JBPanelFilter);
            this.JBPanelButton.Controls.Add(this.JBButtonAutoSize);
            this.JBPanelButton.Controls.Add(this.JBIconKeyboard);
            this.JBPanelButton.Controls.Add(this.JBButtonLaporan);
            this.JBPanelButton.Controls.Add(this.JBButtonFilter);
            this.JBPanelButton.Controls.Add(this.JBButtonSimpan);
            this.JBPanelButton.Location = new System.Drawing.Point(16, 9);
            this.JBPanelButton.Name = "JBPanelButton";
            this.JBPanelButton.Size = new System.Drawing.Size(518, 91);
            this.JBPanelButton.TabIndex = 4;
            // 
            // JBPanelFilter
            // 
            this.JBPanelFilter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.JBPanelFilter.Controls.Add(this.JBFilterBox2Block2);
            this.JBPanelFilter.Controls.Add(this.JBFilterBox2Block1);
            this.JBPanelFilter.Controls.Add(this.JBFilterBox1Block2);
            this.JBPanelFilter.Controls.Add(this.JBFilterBox1Block1);
            this.JBPanelFilter.Controls.Add(this.JBFilterDropdown);
            this.JBPanelFilter.Controls.Add(this.JBFilterGroup1);
            this.JBPanelFilter.Controls.Add(this.JBFilterButtonSearch);
            this.JBPanelFilter.Controls.Add(this.JBFilterPicker2);
            this.JBPanelFilter.Controls.Add(this.JBFilterPicker1);
            this.JBPanelFilter.Controls.Add(this.JBFilterLabel4);
            this.JBPanelFilter.Controls.Add(this.JBFilterLabel3);
            this.JBPanelFilter.Controls.Add(this.JBFilterLabel2);
            this.JBPanelFilter.Controls.Add(this.JBFilterLabel1);
            this.JBPanelFilter.Controls.Add(this.JBFilterMode);
            this.JBPanelFilter.Controls.Add(this.JBFilterGroup2);
            this.JBPanelFilter.Location = new System.Drawing.Point(0, 29);
            this.JBPanelFilter.Name = "JBPanelFilter";
            this.JBPanelFilter.Size = new System.Drawing.Size(518, 59);
            this.JBPanelFilter.TabIndex = 9;
            // 
            // JBFilterBox2Block2
            // 
            this.JBFilterBox2Block2.Location = new System.Drawing.Point(136, 29);
            this.JBFilterBox2Block2.MaxLength = 3;
            this.JBFilterBox2Block2.Name = "JBFilterBox2Block2";
            this.JBFilterBox2Block2.Size = new System.Drawing.Size(29, 20);
            this.JBFilterBox2Block2.TabIndex = 18;
            this.JBFilterBox2Block2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.JBFilterBox2Block2.Visible = false;
            // 
            // JBFilterBox2Block1
            // 
            this.JBFilterBox2Block1.Location = new System.Drawing.Point(107, 29);
            this.JBFilterBox2Block1.MaxLength = 2;
            this.JBFilterBox2Block1.Name = "JBFilterBox2Block1";
            this.JBFilterBox2Block1.Size = new System.Drawing.Size(17, 20);
            this.JBFilterBox2Block1.TabIndex = 17;
            this.JBFilterBox2Block1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.JBFilterBox2Block1.Visible = false;
            // 
            // JBFilterBox1Block2
            // 
            this.JBFilterBox1Block2.Location = new System.Drawing.Point(40, 31);
            this.JBFilterBox1Block2.MaxLength = 3;
            this.JBFilterBox1Block2.Name = "JBFilterBox1Block2";
            this.JBFilterBox1Block2.Size = new System.Drawing.Size(29, 20);
            this.JBFilterBox1Block2.TabIndex = 16;
            this.JBFilterBox1Block2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.JBFilterBox1Block2.Visible = false;
            // 
            // JBFilterBox1Block1
            // 
            this.JBFilterBox1Block1.Location = new System.Drawing.Point(11, 31);
            this.JBFilterBox1Block1.MaxLength = 2;
            this.JBFilterBox1Block1.Name = "JBFilterBox1Block1";
            this.JBFilterBox1Block1.Size = new System.Drawing.Size(17, 20);
            this.JBFilterBox1Block1.TabIndex = 15;
            this.JBFilterBox1Block1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.JBFilterBox1Block1.Visible = false;
            // 
            // JBFilterDropdown
            // 
            this.JBFilterDropdown.FormattingEnabled = true;
            this.JBFilterDropdown.IntegralHeight = false;
            this.JBFilterDropdown.Location = new System.Drawing.Point(117, 33);
            this.JBFilterDropdown.Name = "JBFilterDropdown";
            this.JBFilterDropdown.Size = new System.Drawing.Size(96, 21);
            this.JBFilterDropdown.TabIndex = 14;
            this.JBFilterDropdown.Visible = false;
            // 
            // JBFilterGroup1
            // 
            this.JBFilterGroup1.Controls.Add(this.JBFilterGroup1Opt4);
            this.JBFilterGroup1.Controls.Add(this.JBFilterGroup1Opt3);
            this.JBFilterGroup1.Controls.Add(this.JBFilterGroup1Opt2);
            this.JBFilterGroup1.Controls.Add(this.JBFilterGroup1Opt1);
            this.JBFilterGroup1.Location = new System.Drawing.Point(296, 8);
            this.JBFilterGroup1.Name = "JBFilterGroup1";
            this.JBFilterGroup1.Size = new System.Drawing.Size(200, 100);
            this.JBFilterGroup1.TabIndex = 9;
            this.JBFilterGroup1.TabStop = false;
            // 
            // JBFilterGroup1Opt4
            // 
            this.JBFilterGroup1Opt4.AutoSize = true;
            this.JBFilterGroup1Opt4.Location = new System.Drawing.Point(70, 33);
            this.JBFilterGroup1Opt4.Name = "JBFilterGroup1Opt4";
            this.JBFilterGroup1Opt4.Size = new System.Drawing.Size(56, 17);
            this.JBFilterGroup1Opt4.TabIndex = 3;
            this.JBFilterGroup1Opt4.Text = "Nomor";
            this.JBFilterGroup1Opt4.UseVisualStyleBackColor = true;
            // 
            // JBFilterGroup1Opt3
            // 
            this.JBFilterGroup1Opt3.AutoSize = true;
            this.JBFilterGroup1Opt3.Location = new System.Drawing.Point(70, 13);
            this.JBFilterGroup1Opt3.Name = "JBFilterGroup1Opt3";
            this.JBFilterGroup1Opt3.Size = new System.Drawing.Size(62, 17);
            this.JBFilterGroup1Opt3.TabIndex = 2;
            this.JBFilterGroup1Opt3.Text = "Pembeli";
            this.JBFilterGroup1Opt3.UseVisualStyleBackColor = true;
            // 
            // JBFilterGroup1Opt2
            // 
            this.JBFilterGroup1Opt2.AutoSize = true;
            this.JBFilterGroup1Opt2.Location = new System.Drawing.Point(6, 33);
            this.JBFilterGroup1Opt2.Name = "JBFilterGroup1Opt2";
            this.JBFilterGroup1Opt2.Size = new System.Drawing.Size(55, 17);
            this.JBFilterGroup1Opt2.TabIndex = 1;
            this.JBFilterGroup1Opt2.Text = "Petani";
            this.JBFilterGroup1Opt2.UseVisualStyleBackColor = true;
            // 
            // JBFilterGroup1Opt1
            // 
            this.JBFilterGroup1Opt1.AutoSize = true;
            this.JBFilterGroup1Opt1.Checked = true;
            this.JBFilterGroup1Opt1.Location = new System.Drawing.Point(6, 13);
            this.JBFilterGroup1Opt1.Name = "JBFilterGroup1Opt1";
            this.JBFilterGroup1Opt1.Size = new System.Drawing.Size(64, 17);
            this.JBFilterGroup1Opt1.TabIndex = 0;
            this.JBFilterGroup1Opt1.TabStop = true;
            this.JBFilterGroup1Opt1.Text = "Tanggal";
            this.JBFilterGroup1Opt1.UseVisualStyleBackColor = true;
            // 
            // JBFilterButtonSearch
            // 
            this.JBFilterButtonSearch.AutoSize = true;
            this.JBFilterButtonSearch.BackColor = System.Drawing.Color.White;
            this.JBFilterButtonSearch.Location = new System.Drawing.Point(496, 24);
            this.JBFilterButtonSearch.Name = "JBFilterButtonSearch";
            this.JBFilterButtonSearch.Size = new System.Drawing.Size(65, 23);
            this.JBFilterButtonSearch.TabIndex = 13;
            this.JBFilterButtonSearch.Text = "Terapkan";
            this.JBFilterButtonSearch.UseVisualStyleBackColor = false;
            this.JBFilterButtonSearch.Click += new System.EventHandler(this.JBFilterButtonSearch_Click);
            // 
            // JBFilterPicker2
            // 
            this.JBFilterPicker2.CustomFormat = "dd/MM/yyyy";
            this.JBFilterPicker2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.JBFilterPicker2.Location = new System.Drawing.Point(173, 34);
            this.JBFilterPicker2.Name = "JBFilterPicker2";
            this.JBFilterPicker2.Size = new System.Drawing.Size(95, 20);
            this.JBFilterPicker2.TabIndex = 13;
            // 
            // JBFilterPicker1
            // 
            this.JBFilterPicker1.CustomFormat = "dd/MM/yyyy";
            this.JBFilterPicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.JBFilterPicker1.Location = new System.Drawing.Point(40, 34);
            this.JBFilterPicker1.Name = "JBFilterPicker1";
            this.JBFilterPicker1.Size = new System.Drawing.Size(95, 20);
            this.JBFilterPicker1.TabIndex = 12;
            // 
            // JBFilterLabel4
            // 
            this.JBFilterLabel4.AutoSize = true;
            this.JBFilterLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JBFilterLabel4.Location = new System.Drawing.Point(114, 34);
            this.JBFilterLabel4.Name = "JBFilterLabel4";
            this.JBFilterLabel4.Size = new System.Drawing.Size(62, 18);
            this.JBFilterLabel4.TabIndex = 11;
            this.JBFilterLabel4.Text = "Sampai:";
            // 
            // JBFilterLabel3
            // 
            this.JBFilterLabel3.AutoSize = true;
            this.JBFilterLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JBFilterLabel3.Location = new System.Drawing.Point(6, 34);
            this.JBFilterLabel3.Name = "JBFilterLabel3";
            this.JBFilterLabel3.Size = new System.Drawing.Size(39, 18);
            this.JBFilterLabel3.TabIndex = 10;
            this.JBFilterLabel3.Text = "Dari:";
            // 
            // JBFilterLabel2
            // 
            this.JBFilterLabel2.AutoSize = true;
            this.JBFilterLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JBFilterLabel2.Location = new System.Drawing.Point(189, 9);
            this.JBFilterLabel2.Name = "JBFilterLabel2";
            this.JBFilterLabel2.Size = new System.Drawing.Size(96, 18);
            this.JBFilterLabel2.TabIndex = 8;
            this.JBFilterLabel2.Text = "Berdasarkan:";
            // 
            // JBFilterLabel1
            // 
            this.JBFilterLabel1.AutoSize = true;
            this.JBFilterLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JBFilterLabel1.Location = new System.Drawing.Point(6, 10);
            this.JBFilterLabel1.Name = "JBFilterLabel1";
            this.JBFilterLabel1.Size = new System.Drawing.Size(48, 18);
            this.JBFilterLabel1.TabIndex = 7;
            this.JBFilterLabel1.Text = "Tabel:";
            // 
            // JBFilterMode
            // 
            this.JBFilterMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.JBFilterMode.FormattingEnabled = true;
            this.JBFilterMode.Items.AddRange(new object[] {
            "Pembelian",
            "Penjualan"});
            this.JBFilterMode.Location = new System.Drawing.Point(58, 10);
            this.JBFilterMode.Name = "JBFilterMode";
            this.JBFilterMode.Size = new System.Drawing.Size(96, 21);
            this.JBFilterMode.TabIndex = 0;
            this.JBFilterMode.SelectedIndexChanged += new System.EventHandler(this.JBFilterMode_SelectedIndexChanged);
            // 
            // JBFilterGroup2
            // 
            this.JBFilterGroup2.Controls.Add(this.JBFilterGroup2Opt3);
            this.JBFilterGroup2.Controls.Add(this.JBFilterGroup2Opt2);
            this.JBFilterGroup2.Controls.Add(this.JBFilterGroup2Opt1);
            this.JBFilterGroup2.Location = new System.Drawing.Point(327, 3);
            this.JBFilterGroup2.Name = "JBFilterGroup2";
            this.JBFilterGroup2.Size = new System.Drawing.Size(200, 100);
            this.JBFilterGroup2.TabIndex = 10;
            this.JBFilterGroup2.TabStop = false;
            this.JBFilterGroup2.Visible = false;
            // 
            // JBFilterGroup2Opt3
            // 
            this.JBFilterGroup2Opt3.AutoSize = true;
            this.JBFilterGroup2Opt3.Location = new System.Drawing.Point(70, 13);
            this.JBFilterGroup2Opt3.Name = "JBFilterGroup2Opt3";
            this.JBFilterGroup2Opt3.Size = new System.Drawing.Size(56, 17);
            this.JBFilterGroup2Opt3.TabIndex = 3;
            this.JBFilterGroup2Opt3.Text = "Nomor";
            this.JBFilterGroup2Opt3.UseVisualStyleBackColor = true;
            // 
            // JBFilterGroup2Opt2
            // 
            this.JBFilterGroup2Opt2.AutoSize = true;
            this.JBFilterGroup2Opt2.Location = new System.Drawing.Point(6, 33);
            this.JBFilterGroup2Opt2.Name = "JBFilterGroup2Opt2";
            this.JBFilterGroup2Opt2.Size = new System.Drawing.Size(62, 17);
            this.JBFilterGroup2Opt2.TabIndex = 2;
            this.JBFilterGroup2Opt2.Text = "Pembeli";
            this.JBFilterGroup2Opt2.UseVisualStyleBackColor = true;
            // 
            // JBFilterGroup2Opt1
            // 
            this.JBFilterGroup2Opt1.AutoSize = true;
            this.JBFilterGroup2Opt1.Checked = true;
            this.JBFilterGroup2Opt1.Location = new System.Drawing.Point(6, 13);
            this.JBFilterGroup2Opt1.Name = "JBFilterGroup2Opt1";
            this.JBFilterGroup2Opt1.Size = new System.Drawing.Size(64, 17);
            this.JBFilterGroup2Opt1.TabIndex = 0;
            this.JBFilterGroup2Opt1.TabStop = true;
            this.JBFilterGroup2Opt1.Text = "Tanggal";
            this.JBFilterGroup2Opt1.UseVisualStyleBackColor = true;
            // 
            // JBButtonAutoSize
            // 
            this.JBButtonAutoSize.AutoSize = true;
            this.JBButtonAutoSize.BackColor = System.Drawing.Color.White;
            this.JBButtonAutoSize.Location = new System.Drawing.Point(270, 9);
            this.JBButtonAutoSize.Name = "JBButtonAutoSize";
            this.JBButtonAutoSize.Size = new System.Drawing.Size(83, 23);
            this.JBButtonAutoSize.TabIndex = 8;
            this.JBButtonAutoSize.Text = "Auto Size [F4]";
            this.JBButtonAutoSize.UseVisualStyleBackColor = false;
            this.JBButtonAutoSize.Click += new System.EventHandler(this.JBButtonAutoSize_Click);
            // 
            // JBIconKeyboard
            // 
            this.JBIconKeyboard.BackColor = System.Drawing.Color.White;
            this.JBIconKeyboard.BackgroundImage = global::MMT.Properties.Resources.keyboard;
            this.JBIconKeyboard.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.JBIconKeyboard.FlatAppearance.BorderSize = 0;
            this.JBIconKeyboard.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.JBIconKeyboard.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.JBIconKeyboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.JBIconKeyboard.Location = new System.Drawing.Point(488, 0);
            this.JBIconKeyboard.Margin = new System.Windows.Forms.Padding(0);
            this.JBIconKeyboard.Name = "JBIconKeyboard";
            this.JBIconKeyboard.Size = new System.Drawing.Size(30, 30);
            this.JBIconKeyboard.TabIndex = 7;
            this.JBIconKeyboard.TabStop = false;
            this.JBIconKeyboard.UseVisualStyleBackColor = false;
            // 
            // JBButtonLaporan
            // 
            this.JBButtonLaporan.AutoSize = true;
            this.JBButtonLaporan.BackColor = System.Drawing.Color.White;
            this.JBButtonLaporan.Location = new System.Drawing.Point(9, 9);
            this.JBButtonLaporan.Name = "JBButtonLaporan";
            this.JBButtonLaporan.Size = new System.Drawing.Size(102, 23);
            this.JBButtonLaporan.TabIndex = 1;
            this.JBButtonLaporan.Text = "Buat Laporan [F1]";
            this.JBButtonLaporan.UseVisualStyleBackColor = false;
            this.JBButtonLaporan.Click += new System.EventHandler(this.JBButtonLaporan_Click);
            // 
            // JBButtonFilter
            // 
            this.JBButtonFilter.AutoSize = true;
            this.JBButtonFilter.BackColor = System.Drawing.Color.White;
            this.JBButtonFilter.Location = new System.Drawing.Point(183, 9);
            this.JBButtonFilter.Name = "JBButtonFilter";
            this.JBButtonFilter.Size = new System.Drawing.Size(90, 23);
            this.JBButtonFilter.TabIndex = 3;
            this.JBButtonFilter.Text = "Filter Tabel [F3]";
            this.JBButtonFilter.UseVisualStyleBackColor = false;
            this.JBButtonFilter.Click += new System.EventHandler(this.JBButtonFilter_Click);
            // 
            // JBButtonSimpan
            // 
            this.JBButtonSimpan.AutoSize = true;
            this.JBButtonSimpan.BackColor = System.Drawing.Color.White;
            this.JBButtonSimpan.Location = new System.Drawing.Point(96, 9);
            this.JBButtonSimpan.Name = "JBButtonSimpan";
            this.JBButtonSimpan.Size = new System.Drawing.Size(99, 23);
            this.JBButtonSimpan.TabIndex = 2;
            this.JBButtonSimpan.Text = "Simpan Data [F2]";
            this.JBButtonSimpan.UseVisualStyleBackColor = false;
            this.JBButtonSimpan.Click += new System.EventHandler(this.JBButtonRefresh_Click);
            // 
            // JBPanelContent
            // 
            this.JBPanelContent.Controls.Add(this.JBTablePembelian);
            this.JBPanelContent.Controls.Add(this.JBScroll);
            this.JBPanelContent.Controls.Add(this.JBTablePenjualan);
            this.JBPanelContent.Location = new System.Drawing.Point(16, 144);
            this.JBPanelContent.Name = "JBPanelContent";
            this.JBPanelContent.Size = new System.Drawing.Size(480, 83);
            this.JBPanelContent.TabIndex = 2;
            // 
            // JBTablePembelian
            // 
            this.JBTablePembelian.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.JBTablePembelian.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader23,
            this.columnHeader24,
            this.columnHeader25,
            this.columnHeader26,
            this.columnHeader27,
            this.columnHeader28,
            this.columnHeader29,
            this.columnHeader30,
            this.columnHeader31,
            this.columnHeader33,
            this.columnHeader49,
            this.columnHeader20});
            this.JBTablePembelian.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JBTablePembelian.FullRowSelect = true;
            this.JBTablePembelian.GridLines = true;
            this.JBTablePembelian.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.JBTablePembelian.HideSelection = false;
            this.JBTablePembelian.LabelWrap = false;
            this.JBTablePembelian.Location = new System.Drawing.Point(96, 8);
            this.JBTablePembelian.Name = "JBTablePembelian";
            this.JBTablePembelian.Scrollable = false;
            this.JBTablePembelian.Size = new System.Drawing.Size(535, 175);
            this.JBTablePembelian.TabIndex = 4;
            this.JBTablePembelian.TabStop = false;
            this.JBTablePembelian.UseCompatibleStateImageBehavior = false;
            this.JBTablePembelian.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader23
            // 
            this.columnHeader23.Text = "No";
            // 
            // columnHeader24
            // 
            this.columnHeader24.Text = "Tanggal";
            // 
            // columnHeader25
            // 
            this.columnHeader25.Text = "Nama";
            // 
            // columnHeader26
            // 
            this.columnHeader26.Text = "Desa";
            // 
            // columnHeader27
            // 
            this.columnHeader27.Text = "Pembeli";
            // 
            // columnHeader28
            // 
            this.columnHeader28.Text = "Berat";
            // 
            // columnHeader29
            // 
            this.columnHeader29.Text = "Tembak";
            // 
            // columnHeader30
            // 
            this.columnHeader30.Text = "Netto";
            // 
            // columnHeader31
            // 
            this.columnHeader31.Text = "Harga";
            // 
            // columnHeader33
            // 
            this.columnHeader33.Text = "Tumplek";
            // 
            // columnHeader49
            // 
            this.columnHeader49.Text = "Jumlah";
            // 
            // JBScroll
            // 
            this.JBScroll.Location = new System.Drawing.Point(0, -14);
            this.JBScroll.Name = "JBScroll";
            this.JBScroll.Size = new System.Drawing.Size(17, 80);
            this.JBScroll.TabIndex = 3;
            // 
            // JBTablePenjualan
            // 
            this.JBTablePenjualan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.JBTablePenjualan.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader41,
            this.columnHeader42,
            this.columnHeader43,
            this.columnHeader44,
            this.columnHeader45,
            this.columnHeader46,
            this.columnHeader47,
            this.columnHeader66});
            this.JBTablePenjualan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JBTablePenjualan.FullRowSelect = true;
            this.JBTablePenjualan.GridLines = true;
            this.JBTablePenjualan.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.JBTablePenjualan.LabelWrap = false;
            this.JBTablePenjualan.Location = new System.Drawing.Point(58, 39);
            this.JBTablePenjualan.Name = "JBTablePenjualan";
            this.JBTablePenjualan.Scrollable = false;
            this.JBTablePenjualan.Size = new System.Drawing.Size(535, 175);
            this.JBTablePenjualan.TabIndex = 2;
            this.JBTablePenjualan.TabStop = false;
            this.JBTablePenjualan.UseCompatibleStateImageBehavior = false;
            this.JBTablePenjualan.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader41
            // 
            this.columnHeader41.Text = "Tanggal";
            // 
            // columnHeader42
            // 
            this.columnHeader42.Text = "Pembeli";
            // 
            // columnHeader43
            // 
            this.columnHeader43.Text = "Berat";
            this.columnHeader43.Width = 185;
            // 
            // columnHeader44
            // 
            this.columnHeader44.Text = "Tembak";
            // 
            // columnHeader45
            // 
            this.columnHeader45.Text = "Harga";
            // 
            // columnHeader46
            // 
            this.columnHeader46.Text = "Jumlah";
            // 
            // columnHeader47
            // 
            this.columnHeader47.Text = "Laba/Rugi";
            // 
            // TabButtonJualBeli
            // 
            this.TabButtonJualBeli.AutoSize = true;
            this.TabButtonJualBeli.BackColor = System.Drawing.Color.White;
            this.TabButtonJualBeli.FlatAppearance.BorderSize = 0;
            this.TabButtonJualBeli.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.TabButtonJualBeli.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TabButtonJualBeli.Location = new System.Drawing.Point(45, 20);
            this.TabButtonJualBeli.Margin = new System.Windows.Forms.Padding(25, 3, 25, 3);
            this.TabButtonJualBeli.Name = "TabButtonJualBeli";
            this.TabButtonJualBeli.Size = new System.Drawing.Size(104, 27);
            this.TabButtonJualBeli.TabIndex = 1;
            this.TabButtonJualBeli.TabStop = false;
            this.TabButtonJualBeli.Text = "Jual / Beli";
            this.TabButtonJualBeli.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.TabButtonJualBeli.UseVisualStyleBackColor = false;
            // 
            // TabButtonPemasukan
            // 
            this.TabButtonPemasukan.AutoSize = true;
            this.TabButtonPemasukan.BackColor = System.Drawing.Color.White;
            this.TabButtonPemasukan.FlatAppearance.BorderSize = 0;
            this.TabButtonPemasukan.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.TabButtonPemasukan.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TabButtonPemasukan.Location = new System.Drawing.Point(120, 20);
            this.TabButtonPemasukan.Margin = new System.Windows.Forms.Padding(25, 3, 25, 3);
            this.TabButtonPemasukan.Name = "TabButtonPemasukan";
            this.TabButtonPemasukan.Size = new System.Drawing.Size(201, 27);
            this.TabButtonPemasukan.TabIndex = 2;
            this.TabButtonPemasukan.TabStop = false;
            this.TabButtonPemasukan.Text = "Pemasukan / Pengeluaran";
            this.TabButtonPemasukan.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.TabButtonPemasukan.UseVisualStyleBackColor = false;
            // 
            // PanelPemasukan
            // 
            this.PanelPemasukan.BackColor = System.Drawing.Color.White;
            this.PanelPemasukan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelPemasukan.Location = new System.Drawing.Point(31, 60);
            this.PanelPemasukan.Name = "PanelPemasukan";
            this.PanelPemasukan.Size = new System.Drawing.Size(572, 215);
            this.PanelPemasukan.TabIndex = 1;
            // 
            // TabButtonPetani
            // 
            this.TabButtonPetani.AutoSize = true;
            this.TabButtonPetani.BackColor = System.Drawing.Color.White;
            this.TabButtonPetani.FlatAppearance.BorderSize = 0;
            this.TabButtonPetani.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.TabButtonPetani.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TabButtonPetani.Location = new System.Drawing.Point(373, 20);
            this.TabButtonPetani.Margin = new System.Windows.Forms.Padding(25, 3, 25, 3);
            this.TabButtonPetani.Name = "TabButtonPetani";
            this.TabButtonPetani.Size = new System.Drawing.Size(145, 27);
            this.TabButtonPetani.TabIndex = 3;
            this.TabButtonPetani.TabStop = false;
            this.TabButtonPetani.Text = "Petani / Pembeli";
            this.TabButtonPetani.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.TabButtonPetani.UseVisualStyleBackColor = false;
            // 
            // PanelPetani
            // 
            this.PanelPetani.BackColor = System.Drawing.Color.White;
            this.PanelPetani.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelPetani.Controls.Add(this.PPPanelContent);
            this.PanelPetani.Controls.Add(this.PPPanelHeader);
            this.PanelPetani.Location = new System.Drawing.Point(9, 78);
            this.PanelPetani.Name = "PanelPetani";
            this.PanelPetani.Size = new System.Drawing.Size(572, 215);
            this.PanelPetani.TabIndex = 2;
            // 
            // PPPanelContent
            // 
            this.PPPanelContent.Controls.Add(this.PPScroll);
            this.PPPanelContent.Controls.Add(this.PPTablePembeli);
            this.PPPanelContent.Controls.Add(this.PPTablePetani);
            this.PPPanelContent.Location = new System.Drawing.Point(18, 59);
            this.PPPanelContent.Name = "PPPanelContent";
            this.PPPanelContent.Size = new System.Drawing.Size(518, 143);
            this.PPPanelContent.TabIndex = 7;
            // 
            // PPScroll
            // 
            this.PPScroll.Location = new System.Drawing.Point(4, 6);
            this.PPScroll.Name = "PPScroll";
            this.PPScroll.Size = new System.Drawing.Size(17, 80);
            this.PPScroll.TabIndex = 3;
            // 
            // PPTablePembeli
            // 
            this.PPTablePembeli.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PPTablePembeli.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader60,
            this.columnHeader61,
            this.columnHeader62,
            this.columnHeader63});
            this.PPTablePembeli.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PPTablePembeli.FullRowSelect = true;
            this.PPTablePembeli.GridLines = true;
            this.PPTablePembeli.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.PPTablePembeli.LabelWrap = false;
            this.PPTablePembeli.Location = new System.Drawing.Point(51, 30);
            this.PPTablePembeli.Name = "PPTablePembeli";
            this.PPTablePembeli.Scrollable = false;
            this.PPTablePembeli.Size = new System.Drawing.Size(535, 175);
            this.PPTablePembeli.TabIndex = 5;
            this.PPTablePembeli.TabStop = false;
            this.PPTablePembeli.UseCompatibleStateImageBehavior = false;
            this.PPTablePembeli.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader60
            // 
            this.columnHeader60.Text = " ";
            // 
            // columnHeader61
            // 
            this.columnHeader61.Text = " ";
            // 
            // columnHeader62
            // 
            this.columnHeader62.Text = " ";
            this.columnHeader62.Width = 185;
            // 
            // columnHeader63
            // 
            this.columnHeader63.Text = " ";
            // 
            // PPTablePetani
            // 
            this.PPTablePetani.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PPTablePetani.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader50,
            this.columnHeader51,
            this.columnHeader52});
            this.PPTablePetani.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PPTablePetani.FullRowSelect = true;
            this.PPTablePetani.GridLines = true;
            this.PPTablePetani.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.PPTablePetani.LabelWrap = false;
            this.PPTablePetani.Location = new System.Drawing.Point(36, 15);
            this.PPTablePetani.Name = "PPTablePetani";
            this.PPTablePetani.Scrollable = false;
            this.PPTablePetani.Size = new System.Drawing.Size(535, 175);
            this.PPTablePetani.TabIndex = 4;
            this.PPTablePetani.TabStop = false;
            this.PPTablePetani.UseCompatibleStateImageBehavior = false;
            this.PPTablePetani.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader50
            // 
            this.columnHeader50.Text = " ";
            // 
            // columnHeader51
            // 
            this.columnHeader51.Text = " ";
            // 
            // columnHeader52
            // 
            this.columnHeader52.Text = " ";
            this.columnHeader52.Width = 185;
            // 
            // PPPanelHeader
            // 
            this.PPPanelHeader.BackColor = System.Drawing.Color.White;
            this.PPPanelHeader.Controls.Add(this.PPButtonSearchPembeli);
            this.PPPanelHeader.Controls.Add(this.PPInputSearchPembeli);
            this.PPPanelHeader.Controls.Add(this.PPButtonSearchPetani);
            this.PPPanelHeader.Controls.Add(this.PPInputSearchPetani);
            this.PPPanelHeader.Controls.Add(this.PPLabelPembeli);
            this.PPPanelHeader.Controls.Add(this.PPLabelPetani);
            this.PPPanelHeader.Controls.Add(this.PPHeaderPembeli);
            this.PPPanelHeader.Controls.Add(this.PPHeaderPetani);
            this.PPPanelHeader.Location = new System.Drawing.Point(18, 14);
            this.PPPanelHeader.Name = "PPPanelHeader";
            this.PPPanelHeader.Size = new System.Drawing.Size(514, 40);
            this.PPPanelHeader.TabIndex = 6;
            // 
            // PPButtonSearchPembeli
            // 
            this.PPButtonSearchPembeli.AutoSize = true;
            this.PPButtonSearchPembeli.BackColor = System.Drawing.Color.White;
            this.PPButtonSearchPembeli.Location = new System.Drawing.Point(401, 17);
            this.PPButtonSearchPembeli.Name = "PPButtonSearchPembeli";
            this.PPButtonSearchPembeli.Size = new System.Drawing.Size(81, 23);
            this.PPButtonSearchPembeli.TabIndex = 7;
            this.PPButtonSearchPembeli.Text = "Search";
            this.PPButtonSearchPembeli.UseVisualStyleBackColor = false;
            // 
            // PPInputSearchPembeli
            // 
            this.PPInputSearchPembeli.Location = new System.Drawing.Point(295, 19);
            this.PPInputSearchPembeli.Name = "PPInputSearchPembeli";
            this.PPInputSearchPembeli.Size = new System.Drawing.Size(100, 20);
            this.PPInputSearchPembeli.TabIndex = 6;
            // 
            // PPButtonSearchPetani
            // 
            this.PPButtonSearchPetani.AutoSize = true;
            this.PPButtonSearchPetani.BackColor = System.Drawing.Color.White;
            this.PPButtonSearchPetani.Location = new System.Drawing.Point(355, 8);
            this.PPButtonSearchPetani.Name = "PPButtonSearchPetani";
            this.PPButtonSearchPetani.Size = new System.Drawing.Size(81, 23);
            this.PPButtonSearchPetani.TabIndex = 5;
            this.PPButtonSearchPetani.Text = "Search";
            this.PPButtonSearchPetani.UseVisualStyleBackColor = false;
            // 
            // PPInputSearchPetani
            // 
            this.PPInputSearchPetani.Location = new System.Drawing.Point(249, 10);
            this.PPInputSearchPetani.Name = "PPInputSearchPetani";
            this.PPInputSearchPetani.Size = new System.Drawing.Size(100, 20);
            this.PPInputSearchPetani.TabIndex = 4;
            // 
            // PPLabelPembeli
            // 
            this.PPLabelPembeli.BackColor = System.Drawing.Color.White;
            this.PPLabelPembeli.Font = new System.Drawing.Font("Palatino Linotype", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PPLabelPembeli.Location = new System.Drawing.Point(120, 7);
            this.PPLabelPembeli.Name = "PPLabelPembeli";
            this.PPLabelPembeli.Size = new System.Drawing.Size(113, 23);
            this.PPLabelPembeli.TabIndex = 3;
            this.PPLabelPembeli.Text = "Pembeli";
            this.PPLabelPembeli.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PPLabelPetani
            // 
            this.PPLabelPetani.BackColor = System.Drawing.Color.White;
            this.PPLabelPetani.Font = new System.Drawing.Font("Palatino Linotype", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PPLabelPetani.Location = new System.Drawing.Point(4, 7);
            this.PPLabelPetani.Name = "PPLabelPetani";
            this.PPLabelPetani.Size = new System.Drawing.Size(100, 23);
            this.PPLabelPetani.TabIndex = 2;
            this.PPLabelPetani.Text = "Petani";
            this.PPLabelPetani.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PPHeaderPembeli
            // 
            this.PPHeaderPembeli.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PPHeaderPembeli.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader53,
            this.columnHeader54,
            this.columnHeader55,
            this.columnHeader56});
            this.PPHeaderPembeli.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PPHeaderPembeli.FullRowSelect = true;
            this.PPHeaderPembeli.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.PPHeaderPembeli.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem3});
            this.PPHeaderPembeli.Location = new System.Drawing.Point(249, 15);
            this.PPHeaderPembeli.Name = "PPHeaderPembeli";
            this.PPHeaderPembeli.Scrollable = false;
            this.PPHeaderPembeli.Size = new System.Drawing.Size(479, 175);
            this.PPHeaderPembeli.TabIndex = 4;
            this.PPHeaderPembeli.TabStop = false;
            this.PPHeaderPembeli.UseCompatibleStateImageBehavior = false;
            this.PPHeaderPembeli.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader53
            // 
            this.columnHeader53.Text = "No";
            // 
            // columnHeader54
            // 
            this.columnHeader54.Text = "Kode";
            // 
            // columnHeader55
            // 
            this.columnHeader55.Text = "Nama";
            // 
            // columnHeader56
            // 
            this.columnHeader56.Text = "Alamat";
            // 
            // PPHeaderPetani
            // 
            this.PPHeaderPetani.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PPHeaderPetani.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader57,
            this.columnHeader58,
            this.columnHeader59});
            this.PPHeaderPetani.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PPHeaderPetani.FullRowSelect = true;
            this.PPHeaderPetani.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.PPHeaderPetani.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem4});
            this.PPHeaderPetani.Location = new System.Drawing.Point(227, 3);
            this.PPHeaderPetani.Name = "PPHeaderPetani";
            this.PPHeaderPetani.Scrollable = false;
            this.PPHeaderPetani.Size = new System.Drawing.Size(535, 175);
            this.PPHeaderPetani.TabIndex = 3;
            this.PPHeaderPetani.TabStop = false;
            this.PPHeaderPetani.UseCompatibleStateImageBehavior = false;
            this.PPHeaderPetani.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader57
            // 
            this.columnHeader57.Text = "No";
            // 
            // columnHeader58
            // 
            this.columnHeader58.Text = "Nama";
            // 
            // columnHeader59
            // 
            this.columnHeader59.Text = "Desa";
            // 
            // TabButtonSetting
            // 
            this.TabButtonSetting.AutoSize = true;
            this.TabButtonSetting.BackColor = System.Drawing.Color.White;
            this.TabButtonSetting.FlatAppearance.BorderSize = 0;
            this.TabButtonSetting.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.TabButtonSetting.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TabButtonSetting.Location = new System.Drawing.Point(549, 19);
            this.TabButtonSetting.Margin = new System.Windows.Forms.Padding(25, 3, 25, 3);
            this.TabButtonSetting.Name = "TabButtonSetting";
            this.TabButtonSetting.Size = new System.Drawing.Size(112, 27);
            this.TabButtonSetting.TabIndex = 4;
            this.TabButtonSetting.TabStop = false;
            this.TabButtonSetting.Text = "Pengaturan";
            this.TabButtonSetting.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.TabButtonSetting.UseVisualStyleBackColor = false;
            // 
            // PanelSetting
            // 
            this.PanelSetting.BackColor = System.Drawing.Color.White;
            this.PanelSetting.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelSetting.Location = new System.Drawing.Point(4, 91);
            this.PanelSetting.Name = "PanelSetting";
            this.PanelSetting.Size = new System.Drawing.Size(572, 215);
            this.PanelSetting.TabIndex = 3;
            // 
            // TabBlocker
            // 
            this.TabBlocker.BackColor = System.Drawing.Color.White;
            this.TabBlocker.Location = new System.Drawing.Point(4, 3);
            this.TabBlocker.Name = "TabBlocker";
            this.TabBlocker.Size = new System.Drawing.Size(69, 17);
            this.TabBlocker.TabIndex = 5;
            // 
            // PanelTitleBar
            // 
            this.PanelTitleBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.PanelTitleBar.Location = new System.Drawing.Point(478, 17);
            this.PanelTitleBar.Name = "PanelTitleBar";
            this.PanelTitleBar.Size = new System.Drawing.Size(114, 26);
            this.PanelTitleBar.TabIndex = 6;
            // 
            // LabelTitle
            // 
            this.LabelTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.LabelTitle.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelTitle.ForeColor = System.Drawing.Color.White;
            this.LabelTitle.Location = new System.Drawing.Point(177, 3);
            this.LabelTitle.Name = "LabelTitle";
            this.LabelTitle.Size = new System.Drawing.Size(279, 23);
            this.LabelTitle.TabIndex = 7;
            this.LabelTitle.Text = "- Program Tembakau MMT Tahun ";
            this.LabelTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ButtonMinimize
            // 
            this.ButtonMinimize.AutoSize = true;
            this.ButtonMinimize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.ButtonMinimize.FlatAppearance.BorderSize = 0;
            this.ButtonMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonMinimize.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonMinimize.ForeColor = System.Drawing.Color.White;
            this.ButtonMinimize.Location = new System.Drawing.Point(581, 29);
            this.ButtonMinimize.Margin = new System.Windows.Forms.Padding(25, 3, 25, 3);
            this.ButtonMinimize.Name = "ButtonMinimize";
            this.ButtonMinimize.Size = new System.Drawing.Size(21, 22);
            this.ButtonMinimize.TabIndex = 8;
            this.ButtonMinimize.TabStop = false;
            this.ButtonMinimize.Text = "_";
            this.ButtonMinimize.UseVisualStyleBackColor = false;
            this.ButtonMinimize.Click += new System.EventHandler(this.ButtonMinimize_Click);
            // 
            // ButtonClose
            // 
            this.ButtonClose.AutoSize = true;
            this.ButtonClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(87)))), ((int)(((byte)(154)))));
            this.ButtonClose.FlatAppearance.BorderSize = 0;
            this.ButtonClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonClose.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonClose.ForeColor = System.Drawing.Color.White;
            this.ButtonClose.Location = new System.Drawing.Point(620, 29);
            this.ButtonClose.Margin = new System.Windows.Forms.Padding(25, 3, 25, 3);
            this.ButtonClose.Name = "ButtonClose";
            this.ButtonClose.Size = new System.Drawing.Size(22, 22);
            this.ButtonClose.TabIndex = 9;
            this.ButtonClose.TabStop = false;
            this.ButtonClose.Text = "X";
            this.ButtonClose.UseVisualStyleBackColor = false;
            this.ButtonClose.Click += new System.EventHandler(this.ButtonClose_Click);
            // 
            // PanelBlocker
            // 
            this.PanelBlocker.BackColor = System.Drawing.Color.White;
            this.PanelBlocker.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelBlocker.Location = new System.Drawing.Point(4, 26);
            this.PanelBlocker.Name = "PanelBlocker";
            this.PanelBlocker.Size = new System.Drawing.Size(22, 43);
            this.PanelBlocker.TabIndex = 4;
            // 
            // TooltipsKeyboard
            // 
            this.TooltipsKeyboard.AutoSize = true;
            this.TooltipsKeyboard.BackColor = System.Drawing.Color.White;
            this.TooltipsKeyboard.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TooltipsKeyboard.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TooltipsKeyboard.Location = new System.Drawing.Point(619, 85);
            this.TooltipsKeyboard.Name = "TooltipsKeyboard";
            this.TooltipsKeyboard.Size = new System.Drawing.Size(506, 274);
            this.TooltipsKeyboard.TabIndex = 10;
            this.TooltipsKeyboard.Text = resources.GetString("TooltipsKeyboard.Text");
            this.TooltipsKeyboard.Visible = false;
            // 
            // TabButtonHutang
            // 
            this.TabButtonHutang.AutoSize = true;
            this.TabButtonHutang.BackColor = System.Drawing.Color.White;
            this.TabButtonHutang.FlatAppearance.BorderSize = 0;
            this.TabButtonHutang.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.TabButtonHutang.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TabButtonHutang.Location = new System.Drawing.Point(255, 20);
            this.TabButtonHutang.Margin = new System.Windows.Forms.Padding(25, 3, 25, 3);
            this.TabButtonHutang.Name = "TabButtonHutang";
            this.TabButtonHutang.Size = new System.Drawing.Size(141, 27);
            this.TabButtonHutang.TabIndex = 11;
            this.TabButtonHutang.TabStop = false;
            this.TabButtonHutang.Text = "Hutang / Piutang";
            this.TabButtonHutang.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.TabButtonHutang.UseVisualStyleBackColor = false;
            // 
            // PanelHutang
            // 
            this.PanelHutang.BackColor = System.Drawing.Color.White;
            this.PanelHutang.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelHutang.Controls.Add(this.HPPanelFooter);
            this.PanelHutang.Controls.Add(this.HPPanelContent);
            this.PanelHutang.Controls.Add(this.HPPanelButton);
            this.PanelHutang.Controls.Add(this.HPPanelHeader);
            this.PanelHutang.Location = new System.Drawing.Point(28, 43);
            this.PanelHutang.Name = "PanelHutang";
            this.PanelHutang.Size = new System.Drawing.Size(572, 238);
            this.PanelHutang.TabIndex = 4;
            // 
            // HPPanelFooter
            // 
            this.HPPanelFooter.Controls.Add(this.HPLabelJumlahPiutang);
            this.HPPanelFooter.Controls.Add(this.HPLabelJumlahHutang);
            this.HPPanelFooter.Controls.Add(this.HPLabelJumlahSisaPiutang);
            this.HPPanelFooter.Controls.Add(this.HPLabelJumlahSisaHutang);
            this.HPPanelFooter.Controls.Add(this.HPUnderlineTablePiutang);
            this.HPPanelFooter.Controls.Add(this.HPUnderlineTableHutang);
            this.HPPanelFooter.Location = new System.Drawing.Point(1, 204);
            this.HPPanelFooter.Name = "HPPanelFooter";
            this.HPPanelFooter.Size = new System.Drawing.Size(518, 32);
            this.HPPanelFooter.TabIndex = 9;
            // 
            // HPLabelJumlahPiutang
            // 
            this.HPLabelJumlahPiutang.AutoSize = true;
            this.HPLabelJumlahPiutang.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.HPLabelJumlahPiutang.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HPLabelJumlahPiutang.Location = new System.Drawing.Point(174, 15);
            this.HPLabelJumlahPiutang.Name = "HPLabelJumlahPiutang";
            this.HPLabelJumlahPiutang.Size = new System.Drawing.Size(18, 20);
            this.HPLabelJumlahPiutang.TabIndex = 3;
            this.HPLabelJumlahPiutang.Text = "0";
            // 
            // HPLabelJumlahHutang
            // 
            this.HPLabelJumlahHutang.AutoSize = true;
            this.HPLabelJumlahHutang.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.HPLabelJumlahHutang.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HPLabelJumlahHutang.Location = new System.Drawing.Point(156, 15);
            this.HPLabelJumlahHutang.Name = "HPLabelJumlahHutang";
            this.HPLabelJumlahHutang.Size = new System.Drawing.Size(18, 20);
            this.HPLabelJumlahHutang.TabIndex = 2;
            this.HPLabelJumlahHutang.Text = "0";
            // 
            // HPLabelJumlahSisaPiutang
            // 
            this.HPLabelJumlahSisaPiutang.AutoSize = true;
            this.HPLabelJumlahSisaPiutang.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.HPLabelJumlahSisaPiutang.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HPLabelJumlahSisaPiutang.Location = new System.Drawing.Point(137, 15);
            this.HPLabelJumlahSisaPiutang.Name = "HPLabelJumlahSisaPiutang";
            this.HPLabelJumlahSisaPiutang.Size = new System.Drawing.Size(18, 20);
            this.HPLabelJumlahSisaPiutang.TabIndex = 1;
            this.HPLabelJumlahSisaPiutang.Text = "0";
            // 
            // HPLabelJumlahSisaHutang
            // 
            this.HPLabelJumlahSisaHutang.AutoSize = true;
            this.HPLabelJumlahSisaHutang.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.HPLabelJumlahSisaHutang.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HPLabelJumlahSisaHutang.Location = new System.Drawing.Point(118, 15);
            this.HPLabelJumlahSisaHutang.Name = "HPLabelJumlahSisaHutang";
            this.HPLabelJumlahSisaHutang.Size = new System.Drawing.Size(18, 20);
            this.HPLabelJumlahSisaHutang.TabIndex = 0;
            this.HPLabelJumlahSisaHutang.Text = "0";
            // 
            // HPUnderlineTablePiutang
            // 
            this.HPUnderlineTablePiutang.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.HPUnderlineTablePiutang.Location = new System.Drawing.Point(246, 4);
            this.HPUnderlineTablePiutang.Name = "HPUnderlineTablePiutang";
            this.HPUnderlineTablePiutang.Size = new System.Drawing.Size(200, 1);
            this.HPUnderlineTablePiutang.TabIndex = 5;
            // 
            // HPUnderlineTableHutang
            // 
            this.HPUnderlineTableHutang.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.HPUnderlineTableHutang.Location = new System.Drawing.Point(9, 4);
            this.HPUnderlineTableHutang.Name = "HPUnderlineTableHutang";
            this.HPUnderlineTableHutang.Size = new System.Drawing.Size(200, 1);
            this.HPUnderlineTableHutang.TabIndex = 4;
            // 
            // HPPanelContent
            // 
            this.HPPanelContent.Controls.Add(this.HPTablePiutang);
            this.HPPanelContent.Controls.Add(this.HPScroll);
            this.HPPanelContent.Controls.Add(this.HPTableHutang);
            this.HPPanelContent.Location = new System.Drawing.Point(3, 142);
            this.HPPanelContent.Name = "HPPanelContent";
            this.HPPanelContent.Size = new System.Drawing.Size(480, 83);
            this.HPPanelContent.TabIndex = 8;
            // 
            // HPTablePiutang
            // 
            this.HPTablePiutang.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.HPTablePiutang.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader77,
            this.columnHeader78,
            this.columnHeader79,
            this.columnHeader80,
            this.columnHeader81,
            this.columnHeader82,
            this.columnHeader83,
            this.columnHeader84});
            this.HPTablePiutang.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HPTablePiutang.FullRowSelect = true;
            this.HPTablePiutang.GridLines = true;
            this.HPTablePiutang.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.HPTablePiutang.HideSelection = false;
            this.HPTablePiutang.LabelWrap = false;
            this.HPTablePiutang.Location = new System.Drawing.Point(96, 8);
            this.HPTablePiutang.Name = "HPTablePiutang";
            this.HPTablePiutang.Scrollable = false;
            this.HPTablePiutang.Size = new System.Drawing.Size(535, 175);
            this.HPTablePiutang.TabIndex = 4;
            this.HPTablePiutang.TabStop = false;
            this.HPTablePiutang.UseCompatibleStateImageBehavior = false;
            this.HPTablePiutang.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader77
            // 
            this.columnHeader77.Text = "No";
            // 
            // columnHeader78
            // 
            this.columnHeader78.Text = "Tanggal";
            // 
            // columnHeader79
            // 
            this.columnHeader79.Text = "Nama";
            // 
            // columnHeader80
            // 
            this.columnHeader80.Text = "Desa";
            // 
            // columnHeader81
            // 
            this.columnHeader81.Text = "Pembeli";
            // 
            // columnHeader82
            // 
            this.columnHeader82.Text = "Berat";
            // 
            // columnHeader83
            // 
            this.columnHeader83.Text = "Tembak";
            // 
            // columnHeader84
            // 
            this.columnHeader84.Text = "Netto";
            // 
            // HPScroll
            // 
            this.HPScroll.Location = new System.Drawing.Point(0, -14);
            this.HPScroll.Name = "HPScroll";
            this.HPScroll.Size = new System.Drawing.Size(17, 80);
            this.HPScroll.TabIndex = 3;
            // 
            // HPTableHutang
            // 
            this.HPTableHutang.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.HPTableHutang.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader89,
            this.columnHeader90,
            this.columnHeader91,
            this.columnHeader92,
            this.columnHeader93,
            this.columnHeader94,
            this.columnHeader95});
            this.HPTableHutang.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HPTableHutang.FullRowSelect = true;
            this.HPTableHutang.GridLines = true;
            this.HPTableHutang.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.HPTableHutang.LabelWrap = false;
            this.HPTableHutang.Location = new System.Drawing.Point(20, 18);
            this.HPTableHutang.Name = "HPTableHutang";
            this.HPTableHutang.Scrollable = false;
            this.HPTableHutang.Size = new System.Drawing.Size(535, 175);
            this.HPTableHutang.TabIndex = 2;
            this.HPTableHutang.TabStop = false;
            this.HPTableHutang.UseCompatibleStateImageBehavior = false;
            this.HPTableHutang.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader89
            // 
            this.columnHeader89.Text = "Tanggal";
            // 
            // columnHeader90
            // 
            this.columnHeader90.Text = "Pembeli";
            // 
            // columnHeader91
            // 
            this.columnHeader91.Text = "Berat";
            this.columnHeader91.Width = 185;
            // 
            // columnHeader92
            // 
            this.columnHeader92.Text = "Tembak";
            // 
            // columnHeader93
            // 
            this.columnHeader93.Text = "Harga";
            // 
            // columnHeader94
            // 
            this.columnHeader94.Text = "Jumlah";
            // 
            // columnHeader95
            // 
            this.columnHeader95.Text = "Laba/Rugi";
            // 
            // HPPanelButton
            // 
            this.HPPanelButton.Controls.Add(this.HPPanelFilter);
            this.HPPanelButton.Controls.Add(this.HPButtonAutoSize);
            this.HPPanelButton.Controls.Add(this.HPIconKeyboard);
            this.HPPanelButton.Controls.Add(this.HPButtonLaporan);
            this.HPPanelButton.Controls.Add(this.HPButtonFilter);
            this.HPPanelButton.Controls.Add(this.HPButtonSimpan);
            this.HPPanelButton.Location = new System.Drawing.Point(11, 10);
            this.HPPanelButton.Name = "HPPanelButton";
            this.HPPanelButton.Size = new System.Drawing.Size(518, 91);
            this.HPPanelButton.TabIndex = 7;
            // 
            // HPPanelFilter
            // 
            this.HPPanelFilter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.HPPanelFilter.Controls.Add(this.HPFilterGroup1);
            this.HPPanelFilter.Controls.Add(this.HPFilterBox2);
            this.HPPanelFilter.Controls.Add(this.HPFilterBox1);
            this.HPPanelFilter.Controls.Add(this.HPFilterButtonSearch);
            this.HPPanelFilter.Controls.Add(this.HPFilterPicker2);
            this.HPPanelFilter.Controls.Add(this.HPFilterPicker1);
            this.HPPanelFilter.Controls.Add(this.HPFilterLabel4);
            this.HPPanelFilter.Controls.Add(this.HPFilterLabel3);
            this.HPPanelFilter.Controls.Add(this.HPFilterLabel2);
            this.HPPanelFilter.Controls.Add(this.HPFilterLabel1);
            this.HPPanelFilter.Controls.Add(this.HPFilterMode);
            this.HPPanelFilter.Controls.Add(this.HPFilterGroup2);
            this.HPPanelFilter.Controls.Add(this.comboBox1);
            this.HPPanelFilter.Location = new System.Drawing.Point(12, 41);
            this.HPPanelFilter.Name = "HPPanelFilter";
            this.HPPanelFilter.Size = new System.Drawing.Size(518, 59);
            this.HPPanelFilter.TabIndex = 9;
            // 
            // HPFilterGroup1
            // 
            this.HPFilterGroup1.Controls.Add(this.HPFilterGroup1Opt4);
            this.HPFilterGroup1.Controls.Add(this.HPFilterGroup1Opt3);
            this.HPFilterGroup1.Controls.Add(this.HPFilterGroup1Opt2);
            this.HPFilterGroup1.Controls.Add(this.HPFilterGroup1Opt1);
            this.HPFilterGroup1.Location = new System.Drawing.Point(296, 8);
            this.HPFilterGroup1.Name = "HPFilterGroup1";
            this.HPFilterGroup1.Size = new System.Drawing.Size(200, 100);
            this.HPFilterGroup1.TabIndex = 9;
            this.HPFilterGroup1.TabStop = false;
            // 
            // HPFilterGroup1Opt4
            // 
            this.HPFilterGroup1Opt4.AutoSize = true;
            this.HPFilterGroup1Opt4.Location = new System.Drawing.Point(70, 33);
            this.HPFilterGroup1Opt4.Name = "HPFilterGroup1Opt4";
            this.HPFilterGroup1Opt4.Size = new System.Drawing.Size(56, 17);
            this.HPFilterGroup1Opt4.TabIndex = 3;
            this.HPFilterGroup1Opt4.Text = "Nomor";
            this.HPFilterGroup1Opt4.UseVisualStyleBackColor = true;
            // 
            // HPFilterGroup1Opt3
            // 
            this.HPFilterGroup1Opt3.AutoSize = true;
            this.HPFilterGroup1Opt3.Location = new System.Drawing.Point(70, 13);
            this.HPFilterGroup1Opt3.Name = "HPFilterGroup1Opt3";
            this.HPFilterGroup1Opt3.Size = new System.Drawing.Size(62, 17);
            this.HPFilterGroup1Opt3.TabIndex = 2;
            this.HPFilterGroup1Opt3.Text = "Pembeli";
            this.HPFilterGroup1Opt3.UseVisualStyleBackColor = true;
            // 
            // HPFilterGroup1Opt2
            // 
            this.HPFilterGroup1Opt2.AutoSize = true;
            this.HPFilterGroup1Opt2.Location = new System.Drawing.Point(6, 33);
            this.HPFilterGroup1Opt2.Name = "HPFilterGroup1Opt2";
            this.HPFilterGroup1Opt2.Size = new System.Drawing.Size(55, 17);
            this.HPFilterGroup1Opt2.TabIndex = 1;
            this.HPFilterGroup1Opt2.Text = "Petani";
            this.HPFilterGroup1Opt2.UseVisualStyleBackColor = true;
            // 
            // HPFilterGroup1Opt1
            // 
            this.HPFilterGroup1Opt1.AutoSize = true;
            this.HPFilterGroup1Opt1.Checked = true;
            this.HPFilterGroup1Opt1.Location = new System.Drawing.Point(6, 13);
            this.HPFilterGroup1Opt1.Name = "HPFilterGroup1Opt1";
            this.HPFilterGroup1Opt1.Size = new System.Drawing.Size(64, 17);
            this.HPFilterGroup1Opt1.TabIndex = 0;
            this.HPFilterGroup1Opt1.TabStop = true;
            this.HPFilterGroup1Opt1.Text = "Tanggal";
            this.HPFilterGroup1Opt1.UseVisualStyleBackColor = true;
            // 
            // HPFilterBox2
            // 
            this.HPFilterBox2.Location = new System.Drawing.Point(136, 29);
            this.HPFilterBox2.MaxLength = 3;
            this.HPFilterBox2.Name = "HPFilterBox2";
            this.HPFilterBox2.Size = new System.Drawing.Size(29, 20);
            this.HPFilterBox2.TabIndex = 18;
            this.HPFilterBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.HPFilterBox2.Visible = false;
            // 
            // HPFilterBox1
            // 
            this.HPFilterBox1.Location = new System.Drawing.Point(40, 31);
            this.HPFilterBox1.MaxLength = 3;
            this.HPFilterBox1.Name = "HPFilterBox1";
            this.HPFilterBox1.Size = new System.Drawing.Size(29, 20);
            this.HPFilterBox1.TabIndex = 16;
            this.HPFilterBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.HPFilterBox1.Visible = false;
            // 
            // HPFilterButtonSearch
            // 
            this.HPFilterButtonSearch.AutoSize = true;
            this.HPFilterButtonSearch.BackColor = System.Drawing.Color.White;
            this.HPFilterButtonSearch.Location = new System.Drawing.Point(496, 24);
            this.HPFilterButtonSearch.Name = "HPFilterButtonSearch";
            this.HPFilterButtonSearch.Size = new System.Drawing.Size(65, 23);
            this.HPFilterButtonSearch.TabIndex = 13;
            this.HPFilterButtonSearch.Text = "Terapkan";
            this.HPFilterButtonSearch.UseVisualStyleBackColor = false;
            // 
            // HPFilterPicker2
            // 
            this.HPFilterPicker2.CustomFormat = "dd/MM/yyyy";
            this.HPFilterPicker2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.HPFilterPicker2.Location = new System.Drawing.Point(173, 34);
            this.HPFilterPicker2.Name = "HPFilterPicker2";
            this.HPFilterPicker2.Size = new System.Drawing.Size(95, 20);
            this.HPFilterPicker2.TabIndex = 13;
            // 
            // HPFilterPicker1
            // 
            this.HPFilterPicker1.CustomFormat = "dd/MM/yyyy";
            this.HPFilterPicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.HPFilterPicker1.Location = new System.Drawing.Point(40, 34);
            this.HPFilterPicker1.Name = "HPFilterPicker1";
            this.HPFilterPicker1.Size = new System.Drawing.Size(95, 20);
            this.HPFilterPicker1.TabIndex = 12;
            // 
            // HPFilterLabel4
            // 
            this.HPFilterLabel4.AutoSize = true;
            this.HPFilterLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HPFilterLabel4.Location = new System.Drawing.Point(114, 34);
            this.HPFilterLabel4.Name = "HPFilterLabel4";
            this.HPFilterLabel4.Size = new System.Drawing.Size(62, 18);
            this.HPFilterLabel4.TabIndex = 11;
            this.HPFilterLabel4.Text = "Sampai:";
            // 
            // HPFilterLabel3
            // 
            this.HPFilterLabel3.AutoSize = true;
            this.HPFilterLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HPFilterLabel3.Location = new System.Drawing.Point(6, 34);
            this.HPFilterLabel3.Name = "HPFilterLabel3";
            this.HPFilterLabel3.Size = new System.Drawing.Size(39, 18);
            this.HPFilterLabel3.TabIndex = 10;
            this.HPFilterLabel3.Text = "Dari:";
            // 
            // HPFilterLabel2
            // 
            this.HPFilterLabel2.AutoSize = true;
            this.HPFilterLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HPFilterLabel2.Location = new System.Drawing.Point(189, 9);
            this.HPFilterLabel2.Name = "HPFilterLabel2";
            this.HPFilterLabel2.Size = new System.Drawing.Size(96, 18);
            this.HPFilterLabel2.TabIndex = 8;
            this.HPFilterLabel2.Text = "Berdasarkan:";
            // 
            // HPFilterLabel1
            // 
            this.HPFilterLabel1.AutoSize = true;
            this.HPFilterLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HPFilterLabel1.Location = new System.Drawing.Point(6, 10);
            this.HPFilterLabel1.Name = "HPFilterLabel1";
            this.HPFilterLabel1.Size = new System.Drawing.Size(48, 18);
            this.HPFilterLabel1.TabIndex = 7;
            this.HPFilterLabel1.Text = "Tabel:";
            // 
            // HPFilterMode
            // 
            this.HPFilterMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.HPFilterMode.FormattingEnabled = true;
            this.HPFilterMode.Items.AddRange(new object[] {
            "Pembelian",
            "Penjualan"});
            this.HPFilterMode.Location = new System.Drawing.Point(58, 10);
            this.HPFilterMode.Name = "HPFilterMode";
            this.HPFilterMode.Size = new System.Drawing.Size(96, 21);
            this.HPFilterMode.TabIndex = 0;
            // 
            // HPFilterGroup2
            // 
            this.HPFilterGroup2.Controls.Add(this.HPFilterGroup2Opt3);
            this.HPFilterGroup2.Controls.Add(this.HPFilterGroup2Opt2);
            this.HPFilterGroup2.Controls.Add(this.HPFilterGroup2Opt1);
            this.HPFilterGroup2.Location = new System.Drawing.Point(327, 3);
            this.HPFilterGroup2.Name = "HPFilterGroup2";
            this.HPFilterGroup2.Size = new System.Drawing.Size(200, 100);
            this.HPFilterGroup2.TabIndex = 10;
            this.HPFilterGroup2.TabStop = false;
            this.HPFilterGroup2.Visible = false;
            // 
            // HPFilterGroup2Opt3
            // 
            this.HPFilterGroup2Opt3.AutoSize = true;
            this.HPFilterGroup2Opt3.Location = new System.Drawing.Point(70, 13);
            this.HPFilterGroup2Opt3.Name = "HPFilterGroup2Opt3";
            this.HPFilterGroup2Opt3.Size = new System.Drawing.Size(56, 17);
            this.HPFilterGroup2Opt3.TabIndex = 3;
            this.HPFilterGroup2Opt3.Text = "Nomor";
            this.HPFilterGroup2Opt3.UseVisualStyleBackColor = true;
            // 
            // HPFilterGroup2Opt2
            // 
            this.HPFilterGroup2Opt2.AutoSize = true;
            this.HPFilterGroup2Opt2.Location = new System.Drawing.Point(6, 33);
            this.HPFilterGroup2Opt2.Name = "HPFilterGroup2Opt2";
            this.HPFilterGroup2Opt2.Size = new System.Drawing.Size(62, 17);
            this.HPFilterGroup2Opt2.TabIndex = 2;
            this.HPFilterGroup2Opt2.Text = "Pembeli";
            this.HPFilterGroup2Opt2.UseVisualStyleBackColor = true;
            // 
            // HPFilterGroup2Opt1
            // 
            this.HPFilterGroup2Opt1.AutoSize = true;
            this.HPFilterGroup2Opt1.Checked = true;
            this.HPFilterGroup2Opt1.Location = new System.Drawing.Point(6, 13);
            this.HPFilterGroup2Opt1.Name = "HPFilterGroup2Opt1";
            this.HPFilterGroup2Opt1.Size = new System.Drawing.Size(64, 17);
            this.HPFilterGroup2Opt1.TabIndex = 0;
            this.HPFilterGroup2Opt1.TabStop = true;
            this.HPFilterGroup2Opt1.Text = "Tanggal";
            this.HPFilterGroup2Opt1.UseVisualStyleBackColor = true;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.IntegralHeight = false;
            this.comboBox1.Location = new System.Drawing.Point(117, 33);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(96, 21);
            this.comboBox1.TabIndex = 14;
            this.comboBox1.Visible = false;
            // 
            // HPButtonAutoSize
            // 
            this.HPButtonAutoSize.AutoSize = true;
            this.HPButtonAutoSize.BackColor = System.Drawing.Color.White;
            this.HPButtonAutoSize.Location = new System.Drawing.Point(270, 9);
            this.HPButtonAutoSize.Name = "HPButtonAutoSize";
            this.HPButtonAutoSize.Size = new System.Drawing.Size(83, 23);
            this.HPButtonAutoSize.TabIndex = 8;
            this.HPButtonAutoSize.Text = "Auto Size [F4]";
            this.HPButtonAutoSize.UseVisualStyleBackColor = false;
            this.HPButtonAutoSize.Click += new System.EventHandler(this.HPButtonAutoSize_Click);
            // 
            // HPIconKeyboard
            // 
            this.HPIconKeyboard.BackColor = System.Drawing.Color.White;
            this.HPIconKeyboard.BackgroundImage = global::MMT.Properties.Resources.keyboard;
            this.HPIconKeyboard.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.HPIconKeyboard.FlatAppearance.BorderSize = 0;
            this.HPIconKeyboard.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.HPIconKeyboard.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.HPIconKeyboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HPIconKeyboard.Location = new System.Drawing.Point(488, 0);
            this.HPIconKeyboard.Margin = new System.Windows.Forms.Padding(0);
            this.HPIconKeyboard.Name = "HPIconKeyboard";
            this.HPIconKeyboard.Size = new System.Drawing.Size(30, 30);
            this.HPIconKeyboard.TabIndex = 7;
            this.HPIconKeyboard.TabStop = false;
            this.HPIconKeyboard.UseVisualStyleBackColor = false;
            // 
            // HPButtonLaporan
            // 
            this.HPButtonLaporan.AutoSize = true;
            this.HPButtonLaporan.BackColor = System.Drawing.Color.White;
            this.HPButtonLaporan.Location = new System.Drawing.Point(9, 9);
            this.HPButtonLaporan.Name = "HPButtonLaporan";
            this.HPButtonLaporan.Size = new System.Drawing.Size(102, 23);
            this.HPButtonLaporan.TabIndex = 1;
            this.HPButtonLaporan.Text = "Buat Laporan [F1]";
            this.HPButtonLaporan.UseVisualStyleBackColor = false;
            this.HPButtonLaporan.Click += new System.EventHandler(this.HPButtonLaporan_Click);
            // 
            // HPButtonFilter
            // 
            this.HPButtonFilter.AutoSize = true;
            this.HPButtonFilter.BackColor = System.Drawing.Color.White;
            this.HPButtonFilter.Location = new System.Drawing.Point(183, 9);
            this.HPButtonFilter.Name = "HPButtonFilter";
            this.HPButtonFilter.Size = new System.Drawing.Size(90, 23);
            this.HPButtonFilter.TabIndex = 3;
            this.HPButtonFilter.Text = "Filter Tabel [F3]";
            this.HPButtonFilter.UseVisualStyleBackColor = false;
            this.HPButtonFilter.Click += new System.EventHandler(this.HPButtonFilter_Click);
            // 
            // HPButtonSimpan
            // 
            this.HPButtonSimpan.AutoSize = true;
            this.HPButtonSimpan.BackColor = System.Drawing.Color.White;
            this.HPButtonSimpan.Location = new System.Drawing.Point(96, 9);
            this.HPButtonSimpan.Name = "HPButtonSimpan";
            this.HPButtonSimpan.Size = new System.Drawing.Size(99, 23);
            this.HPButtonSimpan.TabIndex = 2;
            this.HPButtonSimpan.Text = "Simpan Data [F2]";
            this.HPButtonSimpan.UseVisualStyleBackColor = false;
            this.HPButtonSimpan.Click += new System.EventHandler(this.HPButtonSimpan_Click);
            // 
            // HPPanelHeader
            // 
            this.HPPanelHeader.BackColor = System.Drawing.Color.White;
            this.HPPanelHeader.Controls.Add(this.HPLabelPiutang);
            this.HPPanelHeader.Controls.Add(this.HPLabelHutang);
            this.HPPanelHeader.Controls.Add(this.HPHeaderPiutang);
            this.HPPanelHeader.Controls.Add(this.HPHeaderHutang);
            this.HPPanelHeader.Location = new System.Drawing.Point(3, 98);
            this.HPPanelHeader.Name = "HPPanelHeader";
            this.HPPanelHeader.Size = new System.Drawing.Size(514, 40);
            this.HPPanelHeader.TabIndex = 6;
            // 
            // HPLabelPiutang
            // 
            this.HPLabelPiutang.BackColor = System.Drawing.Color.White;
            this.HPLabelPiutang.Font = new System.Drawing.Font("Palatino Linotype", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HPLabelPiutang.Location = new System.Drawing.Point(149, 12);
            this.HPLabelPiutang.Name = "HPLabelPiutang";
            this.HPLabelPiutang.Size = new System.Drawing.Size(113, 28);
            this.HPLabelPiutang.TabIndex = 3;
            this.HPLabelPiutang.Text = "Piutang";
            this.HPLabelPiutang.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // HPLabelHutang
            // 
            this.HPLabelHutang.BackColor = System.Drawing.Color.White;
            this.HPLabelHutang.Font = new System.Drawing.Font("Palatino Linotype", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HPLabelHutang.Location = new System.Drawing.Point(4, 7);
            this.HPLabelHutang.Name = "HPLabelHutang";
            this.HPLabelHutang.Size = new System.Drawing.Size(113, 28);
            this.HPLabelHutang.TabIndex = 2;
            this.HPLabelHutang.Text = "Hutang";
            this.HPLabelHutang.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // HPHeaderPiutang
            // 
            this.HPHeaderPiutang.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.HPHeaderPiutang.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader32,
            this.columnHeader34,
            this.columnHeader35,
            this.columnHeader36,
            this.columnHeader37,
            this.columnHeader38,
            this.columnHeader39,
            this.columnHeader40});
            this.HPHeaderPiutang.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HPHeaderPiutang.FullRowSelect = true;
            this.HPHeaderPiutang.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.HPHeaderPiutang.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem5});
            this.HPHeaderPiutang.Location = new System.Drawing.Point(41, 20);
            this.HPHeaderPiutang.Name = "HPHeaderPiutang";
            this.HPHeaderPiutang.Scrollable = false;
            this.HPHeaderPiutang.Size = new System.Drawing.Size(535, 175);
            this.HPHeaderPiutang.TabIndex = 4;
            this.HPHeaderPiutang.TabStop = false;
            this.HPHeaderPiutang.UseCompatibleStateImageBehavior = false;
            this.HPHeaderPiutang.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader32
            // 
            this.columnHeader32.Text = "No";
            // 
            // columnHeader34
            // 
            this.columnHeader34.Text = "Tanggal";
            // 
            // columnHeader35
            // 
            this.columnHeader35.Text = "Nama";
            // 
            // columnHeader36
            // 
            this.columnHeader36.Text = "Keterangan";
            this.columnHeader36.Width = 56;
            // 
            // columnHeader37
            // 
            this.columnHeader37.Text = "Nominal";
            // 
            // columnHeader38
            // 
            this.columnHeader38.Text = "Bunga";
            // 
            // columnHeader39
            // 
            this.columnHeader39.Text = "Sisa";
            // 
            // columnHeader40
            // 
            this.columnHeader40.Text = "Cicilan";
            // 
            // HPHeaderHutang
            // 
            this.HPHeaderHutang.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.HPHeaderHutang.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader64,
            this.columnHeader65,
            this.columnHeader67,
            this.columnHeader68,
            this.columnHeader69,
            this.columnHeader70,
            this.columnHeader71});
            this.HPHeaderHutang.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HPHeaderHutang.FullRowSelect = true;
            this.HPHeaderHutang.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.HPHeaderHutang.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem6});
            this.HPHeaderHutang.Location = new System.Drawing.Point(6, 6);
            this.HPHeaderHutang.Name = "HPHeaderHutang";
            this.HPHeaderHutang.Scrollable = false;
            this.HPHeaderHutang.Size = new System.Drawing.Size(535, 175);
            this.HPHeaderHutang.TabIndex = 0;
            this.HPHeaderHutang.TabStop = false;
            this.HPHeaderHutang.UseCompatibleStateImageBehavior = false;
            this.HPHeaderHutang.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader64
            // 
            this.columnHeader64.Text = "No";
            // 
            // columnHeader65
            // 
            this.columnHeader65.Text = "Tanggal";
            // 
            // columnHeader67
            // 
            this.columnHeader67.Text = "Nama";
            // 
            // columnHeader68
            // 
            this.columnHeader68.Text = "Keterangan";
            // 
            // columnHeader69
            // 
            this.columnHeader69.Text = "Nominal";
            // 
            // columnHeader70
            // 
            this.columnHeader70.Text = "Sisa";
            // 
            // columnHeader71
            // 
            this.columnHeader71.Text = "Cicilan";
            // 
            // LabelScroll
            // 
            this.LabelScroll.AutoSize = true;
            this.LabelScroll.BackColor = System.Drawing.Color.White;
            this.LabelScroll.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabelScroll.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.LabelScroll.Location = new System.Drawing.Point(623, 62);
            this.LabelScroll.Name = "LabelScroll";
            this.LabelScroll.Size = new System.Drawing.Size(38, 18);
            this.LabelScroll.TabIndex = 12;
            this.LabelScroll.Text = "0000";
            this.LabelScroll.Visible = false;
            // 
            // TimerRefreshFooter
            // 
            this.TimerRefreshFooter.Enabled = true;
            this.TimerRefreshFooter.Interval = 25;
            this.TimerRefreshFooter.Tick += new System.EventHandler(this.TimerRefreshFooter_Tick);
            // 
            // TabButtonFitur
            // 
            this.TabButtonFitur.AutoSize = true;
            this.TabButtonFitur.BackColor = System.Drawing.Color.White;
            this.TabButtonFitur.FlatAppearance.BorderSize = 0;
            this.TabButtonFitur.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.TabButtonFitur.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TabButtonFitur.Location = new System.Drawing.Point(473, 17);
            this.TabButtonFitur.Margin = new System.Windows.Forms.Padding(25, 3, 25, 3);
            this.TabButtonFitur.Name = "TabButtonFitur";
            this.TabButtonFitur.Size = new System.Drawing.Size(112, 27);
            this.TabButtonFitur.TabIndex = 13;
            this.TabButtonFitur.TabStop = false;
            this.TabButtonFitur.Text = "Fitur";
            this.TabButtonFitur.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.TabButtonFitur.UseVisualStyleBackColor = false;
            // 
            // PanelFitur
            // 
            this.PanelFitur.BackColor = System.Drawing.Color.White;
            this.PanelFitur.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelFitur.Controls.Add(this.button2);
            this.PanelFitur.Controls.Add(this.button1);
            this.PanelFitur.Location = new System.Drawing.Point(0, 104);
            this.PanelFitur.Name = "PanelFitur";
            this.PanelFitur.Size = new System.Drawing.Size(572, 215);
            this.PanelFitur.TabIndex = 4;
            // 
            // button2
            // 
            this.button2.BackgroundImage = global::MMT.Properties.Resources.CPU_down;
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Location = new System.Drawing.Point(166, 28);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(128, 128);
            this.button2.TabIndex = 3;
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.BackgroundImage = global::MMT.Properties.Resources.CPU;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(25, 28);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(128, 128);
            this.button1.TabIndex = 2;
            this.button1.UseVisualStyleBackColor = true;
            // 
            // PanelTitleBarCorner
            // 
            this.PanelTitleBarCorner.Image = global::MMT.Properties.Resources.Corner8;
            this.PanelTitleBarCorner.Location = new System.Drawing.Point(591, 17);
            this.PanelTitleBarCorner.Name = "PanelTitleBarCorner";
            this.PanelTitleBarCorner.Size = new System.Drawing.Size(26, 26);
            this.PanelTitleBarCorner.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.PanelTitleBarCorner.TabIndex = 6;
            this.PanelTitleBarCorner.TabStop = false;
            this.PanelTitleBarCorner.Visible = false;
            // 
            // NNTableTyam
            // 
            this.NNTableTyam.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NNTableTyam.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader21,
            this.columnHeader22});
            this.NNTableTyam.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NNTableTyam.FullRowSelect = true;
            this.NNTableTyam.GridLines = true;
            this.NNTableTyam.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.NNTableTyam.HideSelection = false;
            this.NNTableTyam.LabelWrap = false;
            this.NNTableTyam.Location = new System.Drawing.Point(73, 3);
            this.NNTableTyam.Name = "NNTableTyam";
            this.NNTableTyam.Scrollable = false;
            this.NNTableTyam.Size = new System.Drawing.Size(119, 14);
            this.NNTableTyam.TabIndex = 5;
            this.NNTableTyam.TabStop = false;
            this.NNTableTyam.UseCompatibleStateImageBehavior = false;
            this.NNTableTyam.View = System.Windows.Forms.View.Details;
            this.NNTableTyam.Visible = false;
            // 
            // columnHeader21
            // 
            this.columnHeader21.Text = "No";
            // 
            // columnHeader22
            // 
            this.columnHeader22.Text = "Tanggal";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Green;
            this.ClientSize = new System.Drawing.Size(676, 292);
            this.Controls.Add(this.PanelHutang);
            this.Controls.Add(this.PanelJualBeli);
            this.Controls.Add(this.PanelFitur);
            this.Controls.Add(this.TabButtonSetting);
            this.Controls.Add(this.TabButtonFitur);
            this.Controls.Add(this.PanelPetani);
            this.Controls.Add(this.LabelScroll);
            this.Controls.Add(this.PanelPemasukan);
            this.Controls.Add(this.LabelTitle);
            this.Controls.Add(this.TabButtonPetani);
            this.Controls.Add(this.TabButtonHutang);
            this.Controls.Add(this.TooltipsKeyboard);
            this.Controls.Add(this.ButtonMinimize);
            this.Controls.Add(this.ButtonClose);
            this.Controls.Add(this.TabButtonJualBeli);
            this.Controls.Add(this.TabButtonPemasukan);
            this.Controls.Add(this.TabBlocker);
            this.Controls.Add(this.PanelTitleBar);
            this.Controls.Add(this.PanelTitleBarCorner);
            this.Controls.Add(this.PanelBlocker);
            this.Controls.Add(this.PanelSetting);
            this.Controls.Add(this.NNTableTyam);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.TransparencyKey = System.Drawing.Color.Green;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Activated += new System.EventHandler(this.MainForm_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.PanelJualBeli.ResumeLayout(false);
            this.JBPanelFooter.ResumeLayout(false);
            this.JBPanelFooter.PerformLayout();
            this.JBPanelHeader.ResumeLayout(false);
            this.JBPanelButton.ResumeLayout(false);
            this.JBPanelButton.PerformLayout();
            this.JBPanelFilter.ResumeLayout(false);
            this.JBPanelFilter.PerformLayout();
            this.JBFilterGroup1.ResumeLayout(false);
            this.JBFilterGroup1.PerformLayout();
            this.JBFilterGroup2.ResumeLayout(false);
            this.JBFilterGroup2.PerformLayout();
            this.JBPanelContent.ResumeLayout(false);
            this.PanelPetani.ResumeLayout(false);
            this.PPPanelContent.ResumeLayout(false);
            this.PPPanelHeader.ResumeLayout(false);
            this.PPPanelHeader.PerformLayout();
            this.PanelHutang.ResumeLayout(false);
            this.HPPanelFooter.ResumeLayout(false);
            this.HPPanelFooter.PerformLayout();
            this.HPPanelContent.ResumeLayout(false);
            this.HPPanelButton.ResumeLayout(false);
            this.HPPanelButton.PerformLayout();
            this.HPPanelFilter.ResumeLayout(false);
            this.HPPanelFilter.PerformLayout();
            this.HPFilterGroup1.ResumeLayout(false);
            this.HPFilterGroup1.PerformLayout();
            this.HPFilterGroup2.ResumeLayout(false);
            this.HPFilterGroup2.PerformLayout();
            this.HPPanelHeader.ResumeLayout(false);
            this.PanelFitur.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PanelTitleBarCorner)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel PanelJualBeli;
        private System.Windows.Forms.ListView JBHeaderPembelian;
        private System.Windows.Forms.Button TabButtonJualBeli;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.Button TabButtonPemasukan;
        private System.Windows.Forms.Panel PanelPemasukan;
        private System.Windows.Forms.Panel JBPanelContent;
        private System.Windows.Forms.VScrollBar JBScroll;
        private System.Windows.Forms.Button JBButtonFilter;
        private System.Windows.Forms.Button JBButtonLaporan;
        private System.Windows.Forms.Button JBButtonSimpan;
        private System.Windows.Forms.Panel JBPanelHeader;
        private System.Windows.Forms.Panel JBPanelButton;
        private System.Windows.Forms.ListView JBTablePenjualan;
        private System.Windows.Forms.ColumnHeader columnHeader41;
        private System.Windows.Forms.ColumnHeader columnHeader42;
        private System.Windows.Forms.ColumnHeader columnHeader43;
        private System.Windows.Forms.ColumnHeader columnHeader44;
        private System.Windows.Forms.ColumnHeader columnHeader45;
        private System.Windows.Forms.ColumnHeader columnHeader46;
        private System.Windows.Forms.ColumnHeader columnHeader47;
        private System.Windows.Forms.ListView JBTablePembelian;
        private System.Windows.Forms.ColumnHeader columnHeader23;
        private System.Windows.Forms.ColumnHeader columnHeader24;
        private System.Windows.Forms.ColumnHeader columnHeader25;
        private System.Windows.Forms.ColumnHeader columnHeader26;
        private System.Windows.Forms.ColumnHeader columnHeader27;
        private System.Windows.Forms.ColumnHeader columnHeader28;
        private System.Windows.Forms.ColumnHeader columnHeader29;
        private System.Windows.Forms.ColumnHeader columnHeader30;
        private System.Windows.Forms.ColumnHeader columnHeader31;
        private System.Windows.Forms.ColumnHeader columnHeader33;
        private System.Windows.Forms.ColumnHeader columnHeader48;
        private System.Windows.Forms.ColumnHeader columnHeader49;
        private System.Windows.Forms.Button TabButtonPetani;
        private System.Windows.Forms.Panel PanelPetani;
        private System.Windows.Forms.Button TabButtonSetting;
        private System.Windows.Forms.Panel PanelSetting;
        private System.Windows.Forms.Panel TabBlocker;
        private System.Windows.Forms.Label JBLabelPenjualan;
        private System.Windows.Forms.Label JBLabelPembelian;
        private System.Windows.Forms.ListView PPTablePetani;
        private System.Windows.Forms.ColumnHeader columnHeader50;
        private System.Windows.Forms.ColumnHeader columnHeader51;
        private System.Windows.Forms.ColumnHeader columnHeader52;
        private System.Windows.Forms.ListView PPHeaderPetani;
        private System.Windows.Forms.ColumnHeader columnHeader57;
        private System.Windows.Forms.ColumnHeader columnHeader58;
        private System.Windows.Forms.ColumnHeader columnHeader59;
        private System.Windows.Forms.TextBox PPInputSearchPetani;
        private System.Windows.Forms.Button PPButtonSearchPetani;
        private System.Windows.Forms.Panel PPPanelHeader;
        private System.Windows.Forms.Label PPLabelPembeli;
        private System.Windows.Forms.Label PPLabelPetani;
        private System.Windows.Forms.Panel PPPanelContent;
        private System.Windows.Forms.ListView PPTablePembeli;
        private System.Windows.Forms.ColumnHeader columnHeader60;
        private System.Windows.Forms.ColumnHeader columnHeader61;
        private System.Windows.Forms.ColumnHeader columnHeader62;
        private System.Windows.Forms.ColumnHeader columnHeader63;
        private System.Windows.Forms.VScrollBar PPScroll;
        private System.Windows.Forms.ListView PPHeaderPembeli;
        private System.Windows.Forms.ColumnHeader columnHeader53;
        private System.Windows.Forms.ColumnHeader columnHeader54;
        private System.Windows.Forms.ColumnHeader columnHeader55;
        private System.Windows.Forms.ColumnHeader columnHeader56;
        private System.Windows.Forms.Button PPButtonSearchPembeli;
        private System.Windows.Forms.TextBox PPInputSearchPembeli;
        private System.Windows.Forms.ColumnHeader columnHeader66;
        private System.Windows.Forms.Panel PanelTitleBar;
        private System.Windows.Forms.PictureBox PanelTitleBarCorner;
        private System.Windows.Forms.Label LabelTitle;
        private System.Windows.Forms.Button ButtonMinimize;
        private System.Windows.Forms.Button ButtonClose;
        private System.Windows.Forms.Panel PanelBlocker;
        private System.Windows.Forms.Label TooltipsKeyboard;
        private System.Windows.Forms.Button JBIconKeyboard;
        private System.Windows.Forms.Button TabButtonHutang;
        private System.Windows.Forms.Panel PanelHutang;
        private System.Windows.Forms.Label LabelScroll;
        private System.Windows.Forms.Panel JBPanelFooter;
        private System.Windows.Forms.Label JBLabelJumlahBeli;
        private System.Windows.Forms.Label JBLabelJumlahNettoBeli;
        private System.Windows.Forms.Label JBLabelJumlahNettoJual;
        private System.Windows.Forms.Label JBLabelJumlahJual;
        private System.Windows.Forms.Panel JBUnderlineTableBeli;
        private System.Windows.Forms.Panel JBUnderlineTableJual;
        private System.Windows.Forms.ListView JBHeaderPenjualan;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.ColumnHeader columnHeader19;
        private System.Windows.Forms.Timer TimerRefreshFooter;
        private System.Windows.Forms.Label JBLabelJumlah;
        private System.Windows.Forms.Label JBLabelRugi;
        private System.Windows.Forms.Label JBLabelLabaBersih;
        private System.Windows.Forms.Label JBLabelLaba;
        private System.Windows.Forms.Label JBLabelSisa;
        private System.Windows.Forms.Label JBLabelTerjual;
        private System.Windows.Forms.Button JBButtonAutoSize;
        private System.Windows.Forms.Panel JBPanelFilter;
        private System.Windows.Forms.ComboBox JBFilterMode;
        private System.Windows.Forms.Label JBFilterLabel1;
        private System.Windows.Forms.GroupBox JBFilterGroup1;
        private System.Windows.Forms.RadioButton JBFilterGroup1Opt1;
        private System.Windows.Forms.Label JBFilterLabel2;
        private System.Windows.Forms.RadioButton JBFilterGroup1Opt3;
        private System.Windows.Forms.RadioButton JBFilterGroup1Opt2;
        private System.Windows.Forms.RadioButton JBFilterGroup1Opt4;
        private System.Windows.Forms.Label JBFilterLabel4;
        private System.Windows.Forms.Label JBFilterLabel3;
        private System.Windows.Forms.DateTimePicker JBFilterPicker2;
        private System.Windows.Forms.DateTimePicker JBFilterPicker1;
        private System.Windows.Forms.Button JBFilterButtonSearch;
        private System.Windows.Forms.GroupBox JBFilterGroup2;
        private System.Windows.Forms.RadioButton JBFilterGroup2Opt3;
        private System.Windows.Forms.RadioButton JBFilterGroup2Opt2;
        private System.Windows.Forms.RadioButton JBFilterGroup2Opt1;
        private System.Windows.Forms.ComboBox JBFilterDropdown;
        private System.Windows.Forms.TextBox JBFilterBox1Block2;
        private System.Windows.Forms.TextBox JBFilterBox1Block1;
        private System.Windows.Forms.TextBox JBFilterBox2Block2;
        private System.Windows.Forms.TextBox JBFilterBox2Block1;
        private System.Windows.Forms.Button TabButtonFitur;
        private System.Windows.Forms.Panel PanelFitur;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader20;
        private System.Windows.Forms.ListView NNTableTyam;
        private System.Windows.Forms.ColumnHeader columnHeader21;
        private System.Windows.Forms.ColumnHeader columnHeader22;
        private System.Windows.Forms.Panel HPPanelContent;
        private System.Windows.Forms.ListView HPTablePiutang;
        private System.Windows.Forms.ColumnHeader columnHeader77;
        private System.Windows.Forms.ColumnHeader columnHeader78;
        private System.Windows.Forms.ColumnHeader columnHeader79;
        private System.Windows.Forms.ColumnHeader columnHeader80;
        private System.Windows.Forms.ColumnHeader columnHeader81;
        private System.Windows.Forms.ColumnHeader columnHeader82;
        private System.Windows.Forms.ColumnHeader columnHeader83;
        private System.Windows.Forms.ColumnHeader columnHeader84;
        private System.Windows.Forms.VScrollBar HPScroll;
        private System.Windows.Forms.ListView HPTableHutang;
        private System.Windows.Forms.ColumnHeader columnHeader89;
        private System.Windows.Forms.ColumnHeader columnHeader90;
        private System.Windows.Forms.ColumnHeader columnHeader91;
        private System.Windows.Forms.ColumnHeader columnHeader92;
        private System.Windows.Forms.ColumnHeader columnHeader93;
        private System.Windows.Forms.ColumnHeader columnHeader94;
        private System.Windows.Forms.ColumnHeader columnHeader95;
        private System.Windows.Forms.Panel HPPanelButton;
        private System.Windows.Forms.Panel HPPanelFilter;
        private System.Windows.Forms.TextBox HPFilterBox2;
        private System.Windows.Forms.TextBox HPFilterBox1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.GroupBox HPFilterGroup1;
        private System.Windows.Forms.RadioButton HPFilterGroup1Opt4;
        private System.Windows.Forms.RadioButton HPFilterGroup1Opt3;
        private System.Windows.Forms.RadioButton HPFilterGroup1Opt2;
        private System.Windows.Forms.RadioButton HPFilterGroup1Opt1;
        private System.Windows.Forms.Button HPFilterButtonSearch;
        private System.Windows.Forms.DateTimePicker HPFilterPicker2;
        private System.Windows.Forms.DateTimePicker HPFilterPicker1;
        private System.Windows.Forms.Label HPFilterLabel4;
        private System.Windows.Forms.Label HPFilterLabel3;
        private System.Windows.Forms.Label HPFilterLabel2;
        private System.Windows.Forms.Label HPFilterLabel1;
        private System.Windows.Forms.ComboBox HPFilterMode;
        private System.Windows.Forms.GroupBox HPFilterGroup2;
        private System.Windows.Forms.RadioButton HPFilterGroup2Opt3;
        private System.Windows.Forms.RadioButton HPFilterGroup2Opt2;
        private System.Windows.Forms.RadioButton HPFilterGroup2Opt1;
        private System.Windows.Forms.Button HPButtonAutoSize;
        private System.Windows.Forms.Button HPIconKeyboard;
        private System.Windows.Forms.Button HPButtonLaporan;
        private System.Windows.Forms.Button HPButtonFilter;
        private System.Windows.Forms.Button HPButtonSimpan;
        private System.Windows.Forms.Panel HPPanelHeader;
        private System.Windows.Forms.ListView HPHeaderPiutang;
        private System.Windows.Forms.ColumnHeader columnHeader32;
        private System.Windows.Forms.ColumnHeader columnHeader34;
        private System.Windows.Forms.ColumnHeader columnHeader35;
        private System.Windows.Forms.ColumnHeader columnHeader36;
        private System.Windows.Forms.ColumnHeader columnHeader37;
        private System.Windows.Forms.ColumnHeader columnHeader38;
        private System.Windows.Forms.ColumnHeader columnHeader39;
        private System.Windows.Forms.ColumnHeader columnHeader40;
        private System.Windows.Forms.Label HPLabelHutang;
        private System.Windows.Forms.Label HPLabelPiutang;
        private System.Windows.Forms.ListView HPHeaderHutang;
        private System.Windows.Forms.ColumnHeader columnHeader64;
        private System.Windows.Forms.ColumnHeader columnHeader65;
        private System.Windows.Forms.ColumnHeader columnHeader67;
        private System.Windows.Forms.ColumnHeader columnHeader68;
        private System.Windows.Forms.ColumnHeader columnHeader69;
        private System.Windows.Forms.ColumnHeader columnHeader70;
        private System.Windows.Forms.ColumnHeader columnHeader71;
        private System.Windows.Forms.Panel HPPanelFooter;
        private System.Windows.Forms.Label HPLabelJumlahPiutang;
        private System.Windows.Forms.Label HPLabelJumlahHutang;
        private System.Windows.Forms.Label HPLabelJumlahSisaPiutang;
        private System.Windows.Forms.Label HPLabelJumlahSisaHutang;
        private System.Windows.Forms.Panel HPUnderlineTablePiutang;
        private System.Windows.Forms.Panel HPUnderlineTableHutang;
    }
}

