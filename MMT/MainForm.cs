﻿using System;
using System.Drawing.Printing;
using System.IO;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Configuration;
using System.Text.RegularExpressions;

namespace MMT
{
    public partial class MainForm : Form
    {
        enum DataType
        {
            Date, String, Currency, Integer, Decimal, Dropdown, Popup, ReadOnly, SavedReadOnly
        }

        enum SearchMethod
        {
            Date, Index, Keyword
        }

        /***********************************
         * Configs
         * 
         ***********************************/
        #region

        // Pre-defined
        public static string ENCRYPTION_KEY = "01aA21KLxa90mdmKAz8Z81d";
        static string APP_PATH = AppDomain.CurrentDomain.BaseDirectory;
        static string SAVE_DIRECTORY = "Data/";
        static string YEAR_INDEX = "2018";
        static string EXTENSION = ".mmt";
        static string TEXT_LUNAS = "- LUNAS -";
        static int PANEL_TOP_MARGIN = 0;
        static int PANEL_LEFT_MARGIN = 0;
        static int PANEL_RIGHT_MARGIN = 0;
        static int PANEL_BOTTOM_MARGIN = -1;
        static int CONTENT_TOP_MARGIN = 10;
        static int CONTENT_LEFT_MARGIN = 10;
        static int CONTENT_RIGHT_MARGIN = 10;
        static int CONTENT_BOTTOM_MARGIN = 10;
        static int PRINT_TOP_MARGIN = 35;
        static int PRINT_LEFT_MARGIN = 0;
        static int PRINT_RIGHT_MARGIN = 0;
        static int PRINT_BOTTOM_MARGIN = 35;
        static int GENERAL_SPACING = 4;
        static int TAB_JUAL_BELI_TABLE_SPACING = 2;
        static int TABLE_ROW_SELECTION_AUTO_SCROLL_MARGIN = 10;
        static float PRINT_MINIMUM_BLACK_COLOR = 0.8f;
        static Color TABLE_HOVER_COLOR_READONLY = Color.LightGray;
        static Color TABLE_HOVER_COLOR_OTHER = Color.LightGreen;
        static Color ACTIVE_TAB_COLOR = Color.White;
        static Color INACTIVE_TAB_COLOR = Color.FromArgb(43, 87, 154);
        static Color ACTIVE_TAB_FONT_COLOR = Color.Black;
        static Color INACTIVE_TAB_FONT_COLOR = Color.White;
        static Color EMPTY_CELL_WARNING_BG_COLOR = Color.FromArgb(255, 200, 200);
        static FlatStyle ACTIVE_TAB_FLAT_STYLE = FlatStyle.Popup;
        static FlatStyle INACTIVE_TAB_FLAT_STYLE = FlatStyle.Flat;
        static FontStyle ACTIVE_TAB_STYLE = FontStyle.Bold;
        static FontStyle INACTIVE_TAB_STYLE = FontStyle.Regular;
        static Regex ALPHA_NUMERICS_ONLY = new Regex("[^a-zA-Z0-9 -]");
        static Regex ALPHA_DECIMAL_ONLY = new Regex("[^a-zA-Z0-9., -]");
        static List<char> NOTA_PREFIXES = new List<char> { '\0', 'A', 'B', 'C', 'D', 'E', 'F',
                            'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
                            'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
        static Keys[] ARROW_KEYS = new Keys[] { Keys.Up, Keys.Down, Keys.Right, Keys.Left };

        // Column index group event trigger
        static int[] JB_TABLE_PEMBELIAN_GROUP_1 = new int[] { 5, 6, 9, 10 }; // Update jumlah
        static int[] JB_TABLE_PEMBELIAN_GROUP_2 = new int[] { 5, 6 }; // Update netto
        static int[] JB_TABLE_PENJUALAN_GROUP_1 = new int[] { 2, 3, 5 }; // Update jumlah
        static int[] JB_TABLE_PENJUALAN_GROUP_2 = new int[] { 2, 3 }; // Update netto
        static int[] HP_TABLE_HUTANG_GROUP_1 = new int[] { 4, 6 }; // Update sisa
        static int[] HP_TABLE_PIUTANG_GROUP_1 = new int[] { 4, 5, 7 }; // Update sisa

        // Run-time
        float TabJualBeliRatio = 0.575f;
        float TabPetaniRatio = 0.5f;
        float TabHutangRatio = 0.5f;
        #endregion

        /***********************************
         * Variables
         * 
         ***********************************/
        #region
        Dictionary<ListView, Dictionary<int, KeyValuePair<List<List<string>>, int>>> DataSources = new Dictionary<ListView, Dictionary<int, KeyValuePair<List<List<string>>, int>>>();
        Dictionary<ListView, DataType[]> TableDataTypes = new Dictionary<ListView, DataType[]>();
        Dictionary<ListView, ListView> Tables = new Dictionary<ListView, ListView>();
        Dictionary<Button, Panel> Tabs = new Dictionary<Button, Panel>();
        Dictionary<Timer, Label> WarningMessages = new Dictionary<Timer, Label>();
        Dictionary<Control, VScrollBar> ControlScroller = new Dictionary<Control, VScrollBar>();
        Dictionary<VScrollBar, Panel> ScrollParentPanel = new Dictionary<VScrollBar, Panel>();
        Dictionary<ListView, List<List<string>>> TableData = new Dictionary<ListView, List<List<string>>>();
        Dictionary<ListView, List<List<string>>> SearchResult = new Dictionary<ListView, List<List<string>>>();
        Dictionary<ListView, List<int>> SearchResultSource = new Dictionary<ListView, List<int>>();

        Button CurrentTab;
        Screen ActiveScreen;
        Font ActiveTabFont;
        Font InactiveTabFont;

        int ActiveTabHeight;
        int TableHeaderHeight;
        int TableRowHeight;
        bool Canceling = false;
        bool Searching = false;
        bool Recovering = false;
        ListViewItem.ListViewSubItem PrevHoveredItem;

        // Printing
        Panel PrintTargetPanel;
        Size PrintOriginalPanelSize;
        List<Bitmap> PrintPanelImage;
        Bitmap PrintHeaderImage;
        Bitmap PrintFooterImage;
        PrintPreviewDialog PrintDialog;
        PrintDocument PrintDocument;
        int PrintPageIndex;
        int PrintTotalHeight;
        bool PrintExtraPage;

        // Drag controls
        bool IsDragging;
        int DragStartRow;
        int DragEndRow;
        List<int> SelectedIndexes = new List<int>();
        ListView SelectedTable = null;

        // Editors
        DateTimePicker datePicker;
        TextBox stringField;
        TextBox intField;
        TextBox decimalField;
        ComboBox dropdownField;
        TransparentPanel editorBlocker;
        ListView CurrentTable;
        Object CurrentEditor;
        int TargetRow;
        int TargetCol;
        KeyValuePair<List<List<string>>, int> TargetSource;

        // Search table index
        int SearchIndexInput = 0;
        int SearchLetterInput = 0;
        Timer SearchIndexReset;
        ListView SearchIndexTable;

        // JB Footers
        int JBFJumlahCount;
        int JBFJumlah;
        int JBFLabaCount;
        int JBFLaba;
        int JBFRugiCount;
        int JBFRugi;
        int JBFLabaBersih;
        int JBFNettoBeli;
        int JBFNettoJual;
        int JBFJumlahJual;
        int JBFJumlahJualCount;

        // HP Footers
        int HPFSisaHutang;
        int HPFHutang;
        int HPFSisaPiutang;
        int HPFPiutang;

        // JB Footers BU
        int JBFJumlahCountBU;
        int JBFJumlahBU;
        int JBFLabaCountBU;
        int JBFLabaBU;
        int JBFRugiCountBU;
        int JBFRugiBU;
        int JBFLabaBersihBU;
        int JBFNettoBeliBU;
        int JBFNettoJualBU;
        int JBFJumlahJualBU;
        int JBFJumlahJualCountBU;

        // Presets
        static Size NO_SIZE = new Size();
        static PaperSize PRINT_PAPER_SIZE = new PaperSize("Epson LX-300+ii", 850, 1100);
        static float[][] GRAY_MATRIX = new float[][] {
                new float[] { 0.299f, 0.299f, 0.299f, 0, 0 },
                new float[] { 0.587f, 0.587f, 0.587f, 0, 0 },
                new float[] { 0.114f, 0.114f, 0.114f, 0, 0 },
                new float[] { 0,      0,      0,      1, 0 },
                new float[] { 0,      0,      0,      0, 1 }
            };
        static System.Drawing.Imaging.ImageAttributes IMAGE_ATTRIBUTE = new System.Drawing.Imaging.ImageAttributes();
        #endregion


        /***********************************
         * Registry
         * 
         * System inits
         * 
         ***********************************/
        #region
        public MainForm()
        {
            InitializeComponent();

            // Init vars
            ActiveScreen = Screen.FromControl(this);
            ActiveTabFont = new Font(TabButtonJualBeli.Font.FontFamily, 9f, ACTIVE_TAB_STYLE);
            InactiveTabFont = new Font(TabButtonJualBeli.Font.FontFamily, 7f, INACTIVE_TAB_STYLE);
            ActiveTabHeight = TabButtonJualBeli.Height;
            JBHeaderPembelian.Location = new Point();
            TableHeaderHeight = JBHeaderPembelian.TopItem.Bounds.Top - JBHeaderPembelian.Top + 1;
            TableRowHeight = JBHeaderPembelian.TopItem.Bounds.Height;
            IMAGE_ATTRIBUTE.SetColorMatrix(new System.Drawing.Imaging.ColorMatrix(GRAY_MATRIX));
            IMAGE_ATTRIBUTE.SetThreshold(PRINT_MINIMUM_BLACK_COLOR);
            
            // Init editors
            datePicker = new DateTimePicker();
            datePicker.Parent = this;
            datePicker.Format = DateTimePickerFormat.Custom;
            datePicker.Font = JBTablePembelian.Font;
            datePicker.CustomFormat = "dd/MM/yyyy";
            //datePicker.Size = new Size(datePicker.Width / 2, datePicker.Height);
            datePicker.Hide();
            intField = new TextBox();
            intField.Parent = this;
            intField.Multiline = false;
            intField.MaxLength = 32767;
            intField.Font = JBTablePembelian.Font;
            intField.Hide();
            decimalField = new TextBox();
            decimalField.Parent = this;
            decimalField.Multiline = false;
            decimalField.MaxLength = 32767;
            decimalField.Font = JBTablePembelian.Font;
            decimalField.Hide();
            stringField = new TextBox();
            stringField.Parent = this;
            stringField.Multiline = false;
            stringField.MaxLength = 32767;
            stringField.Font = JBTablePembelian.Font;
            stringField.Hide();
            dropdownField = new ComboBox();
            dropdownField.Parent = this;
            dropdownField.MaxLength = 32767;
            dropdownField.MaxDropDownItems = 10;
            dropdownField.IntegralHeight = false;
            dropdownField.Font = JBTablePembelian.Font;
            dropdownField.Hide();

            // Init editor events
            intField.KeyPress += new KeyPressEventHandler(NumberInputOnly);
            decimalField.KeyPress += new KeyPressEventHandler(DecimalInputOnly);
            intField.KeyPress += new KeyPressEventHandler(OnConfirm);
            decimalField.KeyPress += new KeyPressEventHandler(OnConfirm);
            stringField.KeyPress += new KeyPressEventHandler(OnConfirm);
            datePicker.KeyPress += new KeyPressEventHandler(OnConfirm);
            dropdownField.KeyPress += new KeyPressEventHandler(OnConfirm);
            datePicker.KeyPress += new KeyPressEventHandler(OnDelete);
            intField.LostFocus += new EventHandler(OnLostFocus);
            decimalField.LostFocus += new EventHandler(OnLostFocus);
            stringField.LostFocus += new EventHandler(OnLostFocus);
            datePicker.LostFocus += new EventHandler(OnLostFocus);
            dropdownField.LostFocus += new EventHandler(OnLostFocus);
            intField.KeyDown += new KeyEventHandler(OnEditorKeypress);
            decimalField.KeyDown += new KeyEventHandler(OnEditorKeypress);
            stringField.KeyDown += new KeyEventHandler(OnEditorKeypress);
            datePicker.KeyDown += new KeyEventHandler(OnEditorKeypress);
            dropdownField.KeyDown += new KeyEventHandler(OnEditorKeypress);
            dropdownField.KeyUp += new KeyEventHandler(DropdownUpdate);

            // Init editor blocker
            editorBlocker = new TransparentPanel();
            editorBlocker.Parent = this;
            editorBlocker.Size = ActiveScreen.Bounds.Size;
            editorBlocker.BackColor = Color.Transparent;
            editorBlocker.Location = new Point(0, 0);
            editorBlocker.MouseDown += new MouseEventHandler(OnBlockerClick);
            //editorBlocker.BringToFront();
            editorBlocker.Hide();

            TableData.Add(NNTableTyam, new List<List<string>>(NNTableTyam.Columns.Count)); // ⭕ △ □
            InitDataTypes();
            InitTabs();
            InitTables();
            RegisterEvents();
            InitDataSources();
            InitTableTyam();

            // Init individual tabs
            InitTabJualBeli();
            InitTabHutang();
            InitTabPetani();

            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(OnCrashDetected);

            string temp = APP_PATH + SAVE_DIRECTORY + "temp";
            if (Directory.Exists(temp))
            {
                var result = MessageBox.Show("Telah terdeteksi bahwa pada sesi sebelumnya program tidak tertutup dengan benar. Apakah Anda ingin memulihkan data yang belum tersimpan pada sesi tersebut?\n\nJika memilih \"No\" maka semua data tersebut akan dihapus dan tidak dapat dipulihkan kembali!", "Peringatan", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (result == DialogResult.Yes)
                {
                    Recovering = true;
                }
                else
                {
                    foreach (DirectoryInfo d in new DirectoryInfo(temp).GetDirectories())
                    {
                        foreach (FileInfo f in d.GetFiles())
                        {
                            f.Delete();
                        }
                        d.Delete();
                    }
                    Directory.Delete(temp);
                }
            }
            else
            {
                Directory.CreateDirectory(temp).Attributes = FileAttributes.Hidden;
            }

            // Load values
            LoadTableData(JBTablePembelian);
            LoadTableData(JBTablePenjualan);
            LoadTableData(PPTablePembeli);
            LoadTableData(PPTablePetani);
            LoadTableData(HPTableHutang);
            LoadTableData(HPTablePiutang);

            if (Recovering)
            {
                TotalUpdateTable(JBHeaderPembelian);
                TotalUpdateTable(JBTablePenjualan);
                MessageBox.Show("Pemulihan data berhasil.", "Informasi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            Recovering = false;

            // Add empty line
            InsertEmptyToTable(JBTablePembelian, false);
            InsertEmptyToTable(PPTablePembeli, false);
            InsertEmptyToTable(PPTablePetani, false);
            InsertEmptyToTable(HPTableHutang, false);
            InsertEmptyToTable(HPTablePiutang, false);

            // Auto size Columns
            foreach (ListView table in Tables.Keys)
            {
                AutoSizeTableColumnWidths(table, Tables[table], true);
            }

            // Init title bar
            PanelTitleBar.Size = new Size(PanelJualBeli.Width, PanelTitleBarCorner.Height); // - PanelTitleBarCorner.Width + 8
            PanelTitleBar.Location = new Point(PANEL_LEFT_MARGIN, PanelJualBeli.Top - PanelTitleBarCorner.Height + 1);
            PanelTitleBarCorner.Location = new Point(PanelJualBeli.Right - PanelTitleBarCorner.Width - 1, PanelJualBeli.Top - PanelTitleBarCorner.Height + 1);
            LabelTitle.Location = new Point(TabButtonSetting.Right, PanelTitleBar.Location.Y);
            LabelTitle.Size = new Size(LabelTitle.Width, PanelTitleBar.Height);
            LabelTitle.Text += DateTime.Now.Year;
            ButtonClose.Location = new Point(PanelTitleBar.Width - ButtonClose.Width - GENERAL_SPACING, PanelTitleBar.Bottom - ButtonClose.Height);
            ButtonMinimize.Location = new Point(ButtonClose.Left - ButtonMinimize.Width, PanelTitleBar.Bottom - ButtonMinimize.Height);
            PanelBlocker.Size = new Size(ActiveScreen.Bounds.Width - PANEL_LEFT_MARGIN - PANEL_RIGHT_MARGIN, ActiveScreen.Bounds.Height - PanelTitleBar.Height - PANEL_TOP_MARGIN - PANEL_BOTTOM_MARGIN);
            PanelBlocker.Location = PanelJualBeli.Location;

            // Init printing
            PrintDocument = new PrintDocument();
            PrintDocument.PrintPage += new PrintPageEventHandler(OnPrintDocument);
            PrintDocument.DefaultPageSettings.Margins.Left = PRINT_LEFT_MARGIN; //PrintDocument.DefaultPageSettings.Margins.Left * 0.25 + 10
            PrintDocument.DefaultPageSettings.Margins.Right = PRINT_RIGHT_MARGIN; //PrintDocument.DefaultPageSettings.Margins.Right * 0.25
            PrintDocument.DefaultPageSettings.Margins.Top = PRINT_TOP_MARGIN; //PrintDocument.DefaultPageSettings.Margins.Top * 0.25f;
            PrintDocument.DefaultPageSettings.Margins.Bottom = PRINT_BOTTOM_MARGIN; //PrintDocument.DefaultPageSettings.Margins.Top * 0.25f;
            PrintDocument.DefaultPageSettings.PrinterResolution.Kind = PrinterResolutionKind.High;
            PrintDocument.DefaultPageSettings.Color = false;
            PrintDocument.DefaultPageSettings.PaperSize = PRINT_PAPER_SIZE;
            PrintDialog = new PrintPreviewDialog();
            PrintDialog.Document = PrintDocument;

            // Init table index searching
            SearchIndexReset = new Timer();
            SearchIndexReset.Interval = 1000;
            SearchIndexReset.Tick += new EventHandler(ResetSearchTableIndexInput);
        }

        void AddToTableTyam(List<List<string>> data, string str)
        {
            int row = data.Count;
            List<string> col = new List<string>() { (row + 1) + "", str };
            data.Add(col);
        }

        void InitTableTyam()
        {
            // Init tabel tyamz
            List<List<string>> data = TableData[NNTableTyam];
            AddToTableTyam(data, "A+ △");
            AddToTableTyam(data, "A+ ⭕");
            AddToTableTyam(data, "A+ □");
            AddToTableTyam(data, "A △");
            AddToTableTyam(data, "A ⭕");
            AddToTableTyam(data, "A □");
            AddToTableTyam(data, "B+ △");
            AddToTableTyam(data, "B+ ⭕");
            AddToTableTyam(data, "B+ □");
            AddToTableTyam(data, "B △");
            AddToTableTyam(data, "B ⭕");
            AddToTableTyam(data, "B □");
            AddToTableTyam(data, "C+ △");
            AddToTableTyam(data, "C+ ⭕");
            AddToTableTyam(data, "C+ □");
            AddToTableTyam(data, "C △");
            AddToTableTyam(data, "C ⭕");
            AddToTableTyam(data, "C □");
            AddToTableTyam(data, "D+ △");
            AddToTableTyam(data, "D+ ⭕");
            AddToTableTyam(data, "D+ □");
            AddToTableTyam(data, "D △");
            AddToTableTyam(data, "D ⭕");
            AddToTableTyam(data, "D □");
            AddToTableTyam(data, "E+ △");
            AddToTableTyam(data, "E+ ⭕");
            AddToTableTyam(data, "E+ □");
            AddToTableTyam(data, "E △");
            AddToTableTyam(data, "E ⭕");
            AddToTableTyam(data, "E □");
            AddToTableTyam(data, "F+ △");
            AddToTableTyam(data, "F+ ⭕");
            AddToTableTyam(data, "F+ □");
            AddToTableTyam(data, "F △");
            AddToTableTyam(data, "F ⭕");
            AddToTableTyam(data, "F □");
            AddToTableTyam(data, "G+ △");
            AddToTableTyam(data, "G+ ⭕");
            AddToTableTyam(data, "G+ □");
            AddToTableTyam(data, "G △");
            AddToTableTyam(data, "G ⭕");
            AddToTableTyam(data, "G □");
            AddToTableTyam(data, "");
        }

        void InitDataSources()
        {
            // TablePembelian sources
            Dictionary<int, KeyValuePair<List<List<string>>, int>> columnSources = new Dictionary<int, KeyValuePair<List<List<string>>, int>>();
            columnSources.Add(2, new KeyValuePair<List<List<string>>, int>(TableData[PPTablePetani], 1)); // Petani
            columnSources.Add(3, new KeyValuePair<List<List<string>>, int>(TableData[PPTablePetani], 2)); // Desa
            columnSources.Add(4, new KeyValuePair<List<List<string>>, int>(TableData[PPTablePembeli], 1)); // Pembeli
            columnSources.Add(8, new KeyValuePair<List<List<string>>, int>(TableData[NNTableTyam], 1)); // Tyam
            DataSources.Add(JBTablePembelian, columnSources);

            // TablePenjualan sources
            columnSources = new Dictionary<int, KeyValuePair<List<List<string>>, int>>();
            columnSources.Add(1, new KeyValuePair<List<List<string>>, int>(TableData[PPTablePembeli], 1));
            DataSources.Add(JBTablePenjualan, columnSources);

            // TableHutang sources
            columnSources = new Dictionary<int, KeyValuePair<List<List<string>>, int>>();
            columnSources.Add(2, new KeyValuePair<List<List<string>>, int>(TableData[PPTablePetani], 1)); // Nama
            DataSources.Add(HPTableHutang, columnSources);

            // TablePiutang sources
            columnSources = new Dictionary<int, KeyValuePair<List<List<string>>, int>>();
            columnSources.Add(2, new KeyValuePair<List<List<string>>, int>(TableData[PPTablePetani], 1)); // Nama
            DataSources.Add(HPTablePiutang, columnSources);
        }

        void InitDataTypes()
        {
            // Tabel pembelian
            TableDataTypes.Add(JBTablePembelian, new DataType[] { DataType.SavedReadOnly, DataType.Date, DataType.Dropdown, DataType.Dropdown, DataType.Dropdown, DataType.Integer, DataType.Integer, DataType.ReadOnly, DataType.Dropdown, DataType.Currency, DataType.Currency, DataType.ReadOnly });
            TableDataTypes.Add(JBTablePenjualan, new DataType[] { DataType.Date, DataType.String, DataType.Integer, DataType.Integer, DataType.ReadOnly, DataType.Currency, DataType.ReadOnly, DataType.ReadOnly, DataType.ReadOnly, DataType.ReadOnly });
            TableDataTypes.Add(PPTablePetani, new DataType[] { DataType.SavedReadOnly, DataType.String, DataType.String });
            TableDataTypes.Add(PPTablePembeli, new DataType[] { DataType.SavedReadOnly, DataType.String, DataType.String, DataType.String });
            TableDataTypes.Add(HPTableHutang, new DataType[] { DataType.SavedReadOnly, DataType.Date, DataType.Dropdown, DataType.String, DataType.Currency, DataType.SavedReadOnly, DataType.Currency });
            TableDataTypes.Add(HPTablePiutang, new DataType[] { DataType.SavedReadOnly, DataType.Date, DataType.Dropdown, DataType.String, DataType.Currency, DataType.Decimal, DataType.SavedReadOnly, DataType.Currency });
            TableDataTypes.Add(NNTableTyam, new DataType[] { DataType.SavedReadOnly, DataType.String });
        }

        void InitTabs()
        {
            AddNewTab(TabButtonJualBeli, PanelJualBeli);
            AddNewTab(TabButtonPemasukan, PanelPemasukan);
            AddNewTab(TabButtonHutang, PanelHutang);
            AddNewTab(TabButtonPetani, PanelPetani);
            AddNewTab(TabButtonFitur, PanelFitur);
            AddNewTab(TabButtonSetting, PanelSetting);
            SetActiveTab(Tabs.Keys.ElementAt(0));
            for (int i = 1; i < Tabs.Values.Count; i++)
                Tabs.Values.ElementAt(i).Visible = false;
        }

        void InitTables()
        {
            AddNewTable(JBHeaderPembelian, JBTablePembelian);
            AddNewTable(JBHeaderPenjualan, JBTablePenjualan);
            AddNewTable(HPHeaderHutang, HPTableHutang);
            AddNewTable(HPHeaderPiutang, HPTablePiutang);
            AddNewTable(PPHeaderPetani, PPTablePetani);
            AddNewTable(PPHeaderPembeli, PPTablePembeli);
        }

        void RegisterEvents()
        {
            //JBHeaderPembelian.ColumnWidthChanging += new ColumnWidthChangingEventHandler(ColumnSizeChanging);
            //JBHeaderPenjualan.ColumnWidthChanging += new ColumnWidthChangingEventHandler(ColumnSizeChanging);
            //JBTablePembelian.MouseClick += new MouseEventHandler(OnTableClick);
            //JBTablePenjualan.MouseClick += new MouseEventHandler(OnTableClick);
        }

        void InitTabPetani()
        {
            int ContentAreaWidth = PanelPetani.Width - CONTENT_LEFT_MARGIN - CONTENT_RIGHT_MARGIN - PPScroll.Width;

            // Init header panel
            PPPanelHeader.Location = new Point(0, CONTENT_TOP_MARGIN);
            PPPanelHeader.Size = new Size(PanelPetani.Width, TableHeaderHeight + PPLabelPembeli.Height + GENERAL_SPACING * 2 - 2);
            PPLabelPetani.Location = new Point(CONTENT_LEFT_MARGIN, 0);
            PPLabelPetani.Size = new Size((int)(ContentAreaWidth * TabPetaniRatio) - TAB_JUAL_BELI_TABLE_SPACING, PPLabelPetani.Height);
            PPLabelPembeli.Location = new Point(PPLabelPetani.Right + TAB_JUAL_BELI_TABLE_SPACING * 2, PPLabelPetani.Location.Y);
            PPLabelPembeli.Size = new Size(ContentAreaWidth - PPLabelPetani.Width, PPLabelPembeli.Height);
            PPHeaderPetani.Location = new Point(CONTENT_LEFT_MARGIN, PPLabelPembeli.Bottom + GENERAL_SPACING);
            PPHeaderPetani.Size = new Size(PPLabelPetani.Width, TableHeaderHeight);
            PPHeaderPembeli.Location = new Point(PPHeaderPetani.Right + TAB_JUAL_BELI_TABLE_SPACING * 2, PPHeaderPetani.Location.Y);
            PPHeaderPembeli.Size = new Size(ContentAreaWidth - PPHeaderPetani.Width, TableHeaderHeight);
            PPButtonSearchPetani.Location = new Point(PPHeaderPetani.Right - PPButtonSearchPetani.Width, PPHeaderPetani.Top - PPButtonSearchPetani.Height);
            PPInputSearchPetani.Location = new Point(PPButtonSearchPetani.Left - PPInputSearchPetani.Width - GENERAL_SPACING, PPHeaderPetani.Top - PPInputSearchPetani.Height - 1);
            PPButtonSearchPembeli.Location = new Point(PPHeaderPembeli.Right - PPButtonSearchPembeli.Width, PPHeaderPembeli.Top - PPButtonSearchPembeli.Height);
            PPInputSearchPembeli.Location = new Point(PPButtonSearchPembeli.Left - PPInputSearchPembeli.Width - GENERAL_SPACING, PPHeaderPembeli.Top - PPInputSearchPembeli.Height - 1);

            // Init content panel
            PPPanelContent.Location = new Point(0, PPPanelHeader.Bottom);
            PPPanelContent.Size = new Size(PanelPetani.Width, PanelPetani.Height - PPPanelHeader.Bottom - CONTENT_BOTTOM_MARGIN);
            PPScroll.Location = new Point(PPPanelContent.Width - PPScroll.Width, 0);
            PPScroll.Size = new Size(PPScroll.Width, PPPanelContent.Height);
            PPTablePetani.Location = new Point(PPHeaderPetani.Location.X, 0);
            PPTablePetani.Size = new Size(PPHeaderPetani.Width, PPPanelContent.Height);
            PPTablePembeli.Location = new Point(PPHeaderPembeli.Location.X, PPTablePetani.Location.Y);
            PPTablePembeli.Size = new Size(PPHeaderPembeli.Width, PPPanelContent.Height);

            // Init scroll
            PPScroll.Minimum = 0;
            PPScroll.Maximum = 0;
            PPScroll.SmallChange = PPScroll.LargeChange = 1;
            PPScroll.ValueChanged += new EventHandler(ScrollValueChanged);
            PPScroll.Scroll += new ScrollEventHandler(ScrollScrolled);
            PPScroll.MouseMove += new MouseEventHandler(OnScrollHover);
            PPScroll.MouseLeave += new EventHandler(ScrollMouseLeave);

            RegisterPanelScrollEvents(PanelPetani, PPScroll, false);
            RegisterPanelScrollEvents(PPPanelHeader, PPScroll, false);
            RegisterPanelScrollEvents(PPPanelContent, PPScroll, false);

            ScrollParentPanel.Add(PPScroll, PPPanelContent);
        }

        void InitTabJualBeli()
        {
            // Init button panel
            JBPanelButton.Location = new Point(0, 0);
            JBPanelButton.Size = new Size(PanelJualBeli.Width, JBButtonLaporan.Height + CONTENT_TOP_MARGIN);
            JBButtonLaporan.Location = new Point(CONTENT_LEFT_MARGIN, CONTENT_TOP_MARGIN);
            JBButtonSimpan.Location = new Point(JBButtonLaporan.Right + GENERAL_SPACING, CONTENT_TOP_MARGIN);
            JBButtonFilter.Location = new Point(JBButtonSimpan.Right + GENERAL_SPACING, CONTENT_TOP_MARGIN);
            JBButtonAutoSize.Location = new Point(JBButtonFilter.Right + GENERAL_SPACING, CONTENT_TOP_MARGIN);

            // Init filter panel
            JBPanelFilter.Location = new Point(CONTENT_LEFT_MARGIN, JBPanelButton.Bottom);
            JBPanelFilter.Size = new Size(JBPanelButton.Width - CONTENT_RIGHT_MARGIN - CONTENT_LEFT_MARGIN, TableRowHeight * 3);
            JBPanelFilter.Visible = false;
            JBFilterLabel1.Location = new Point(CONTENT_LEFT_MARGIN, GENERAL_SPACING);
            JBFilterMode.Location = new Point(JBFilterLabel1.Right + GENERAL_SPACING, GENERAL_SPACING);
            JBFilterMode.SelectedIndex = 0;
            JBFilterLabel2.Location = new Point(JBFilterMode.Right + 25, GENERAL_SPACING);
            JBFilterGroup1.Location = JBFilterGroup2.Location = new Point(JBFilterLabel2.Right + GENERAL_SPACING, -2);
            JBFilterGroup1.Size = JBFilterGroup2.Size = new Size(JBFilterGroup1Opt3.Right + GENERAL_SPACING, JBPanelFilter.Height - GENERAL_SPACING * 2);
            JBFilterLabel3.Location = new Point(JBFilterGroup1.Right + 25, GENERAL_SPACING);
            JBFilterPicker1.Location = new Point(JBFilterLabel3.Right + GENERAL_SPACING, GENERAL_SPACING);
            JBFilterLabel4.Location = new Point(JBFilterPicker1.Right + GENERAL_SPACING, GENERAL_SPACING);
            JBFilterPicker2.Location = new Point(JBFilterLabel4.Right + GENERAL_SPACING, GENERAL_SPACING);
            JBFilterButtonSearch.Location = new Point(JBFilterPicker2.Right + 25, GENERAL_SPACING);
            JBFilterGroup1Opt1.CheckedChanged += new EventHandler(JBFilterRadioBoxChecked);
            JBFilterGroup1Opt2.CheckedChanged += new EventHandler(JBFilterRadioBoxChecked);
            JBFilterGroup1Opt3.CheckedChanged += new EventHandler(JBFilterRadioBoxChecked);
            JBFilterGroup1Opt4.CheckedChanged += new EventHandler(JBFilterRadioBoxChecked);
            JBFilterGroup2Opt1.CheckedChanged += new EventHandler(JBFilterRadioBoxChecked);
            JBFilterGroup2Opt2.CheckedChanged += new EventHandler(JBFilterRadioBoxChecked);
            JBFilterGroup2Opt3.CheckedChanged += new EventHandler(JBFilterRadioBoxChecked);
            JBFilterBox1Block1.KeyPress += new KeyPressEventHandler(FilterBoxHandler);
            JBFilterBox2Block1.KeyPress += new KeyPressEventHandler(FilterBoxHandler);
            JBFilterBox1Block2.KeyPress += new KeyPressEventHandler(NumberInputOnly);
            JBFilterBox2Block2.KeyPress += new KeyPressEventHandler(NumberInputOnly);

            RescaleTabJualBeli();

            // Init footer panel
            JBPanelFooter.Location = new Point(0, JBPanelContent.Bottom);
            JBPanelFooter.Size = new Size(PanelJualBeli.Width, TableRowHeight * 2);
            JBUnderlineTableBeli.Location = new Point(JBTablePembelian.Left, 0);
            JBUnderlineTableBeli.Size = new Size(JBTablePembelian.Width, 1);
            JBUnderlineTableJual.Location = new Point(JBTablePenjualan.Left, 0);
            JBUnderlineTableJual.Size = new Size(JBTablePenjualan.Width, 1);
            JBLabelJumlah.Location = new Point(JBUnderlineTableBeli.Left, JBUnderlineTableBeli.Bottom + GENERAL_SPACING);
            JBLabelTerjual.Location = new Point(JBUnderlineTableBeli.Left, JBLabelJumlah.Bottom);
            JBLabelSisa.Location = new Point(JBLabelTerjual.Right, JBLabelJumlah.Bottom);
            JBLabelLabaBersih.Location = new Point(JBUnderlineTableJual.Left, JBUnderlineTableJual.Bottom + GENERAL_SPACING);
            JBLabelLaba.Location = new Point(JBUnderlineTableJual.Left, JBLabelLabaBersih.Bottom);
            JBLabelRugi.Location = new Point(JBLabelLaba.Right, JBLabelLabaBersih.Bottom);

            // Init miscs
            JBIconKeyboard.Click += new EventHandler(OnTooltipClick);
            JBIconKeyboard.DoubleClick += new EventHandler(OnTooltipClick);
            JBIconKeyboard.MouseLeave += new EventHandler(TooltipLostFocus);
            JBIconKeyboard.Location = new Point(JBPanelButton.Right - JBIconKeyboard.Width - CONTENT_RIGHT_MARGIN, 0);

            // Init scroll
            JBScroll.Minimum = 0;
            JBScroll.Maximum = 0;
            JBScroll.SmallChange = JBScroll.LargeChange = 1;
            JBScroll.ValueChanged += new EventHandler(ScrollValueChanged);
            JBScroll.Scroll += new ScrollEventHandler(ScrollScrolled);
            JBScroll.MouseMove += new MouseEventHandler(OnScrollHover);
            JBScroll.MouseLeave += new EventHandler(ScrollMouseLeave);

            RegisterPanelScrollEvents(PanelJualBeli, JBScroll, false);
            RegisterPanelScrollEvents(JBPanelButton, JBScroll, false);
            RegisterPanelScrollEvents(JBPanelContent, JBScroll, false);
            RegisterPanelScrollEvents(JBPanelFooter, JBScroll, false);
            RegisterPanelScrollEvents(JBPanelHeader, JBScroll, false);

            ScrollParentPanel.Add(JBScroll, JBPanelContent);
        }

        void InitTabHutang()
        {
            // Init button panel
            HPPanelButton.Location = new Point(0, 0);
            HPPanelButton.Size = new Size(PanelJualBeli.Width, HPButtonLaporan.Height + CONTENT_TOP_MARGIN);
            HPButtonLaporan.Location = new Point(CONTENT_LEFT_MARGIN, CONTENT_TOP_MARGIN);
            HPButtonSimpan.Location = new Point(HPButtonLaporan.Right + GENERAL_SPACING, CONTENT_TOP_MARGIN);
            HPButtonFilter.Location = new Point(HPButtonSimpan.Right + GENERAL_SPACING, CONTENT_TOP_MARGIN);
            HPButtonAutoSize.Location = new Point(HPButtonFilter.Right + GENERAL_SPACING, CONTENT_TOP_MARGIN);

            // Init filter panel
            HPPanelFilter.Location = new Point(CONTENT_LEFT_MARGIN, HPPanelButton.Bottom);
            HPPanelFilter.Size = new Size(HPPanelButton.Width - CONTENT_RIGHT_MARGIN - CONTENT_LEFT_MARGIN, TableRowHeight * 3);
            HPPanelFilter.Visible = false;
            HPFilterLabel1.Location = new Point(CONTENT_LEFT_MARGIN, GENERAL_SPACING);
            HPFilterMode.Location = new Point(HPFilterLabel1.Right + GENERAL_SPACING, GENERAL_SPACING);
            HPFilterMode.SelectedIndex = 0;
            HPFilterLabel2.Location = new Point(HPFilterMode.Right + 25, GENERAL_SPACING);
            HPFilterGroup1.Location = HPFilterGroup2.Location = new Point(HPFilterLabel2.Right + GENERAL_SPACING, -2);
            HPFilterGroup1.Size = HPFilterGroup2.Size = new Size(HPFilterGroup1Opt3.Right + GENERAL_SPACING, HPPanelFilter.Height - GENERAL_SPACING * 2);
            HPFilterLabel3.Location = new Point(HPFilterGroup1.Right + 25, GENERAL_SPACING);
            HPFilterPicker1.Location = new Point(HPFilterLabel3.Right + GENERAL_SPACING, GENERAL_SPACING);
            HPFilterLabel4.Location = new Point(HPFilterPicker1.Right + GENERAL_SPACING, GENERAL_SPACING);
            HPFilterPicker2.Location = new Point(HPFilterLabel4.Right + GENERAL_SPACING, GENERAL_SPACING);
            HPFilterButtonSearch.Location = new Point(HPFilterPicker2.Right + 25, GENERAL_SPACING);
            HPFilterGroup1Opt1.CheckedChanged += new EventHandler(HPFilterRadioBoxChecked);
            HPFilterGroup1Opt2.CheckedChanged += new EventHandler(HPFilterRadioBoxChecked);
            HPFilterGroup1Opt3.CheckedChanged += new EventHandler(HPFilterRadioBoxChecked);
            HPFilterGroup1Opt4.CheckedChanged += new EventHandler(HPFilterRadioBoxChecked);
            HPFilterGroup2Opt1.CheckedChanged += new EventHandler(HPFilterRadioBoxChecked);
            HPFilterGroup2Opt2.CheckedChanged += new EventHandler(HPFilterRadioBoxChecked);
            HPFilterGroup2Opt3.CheckedChanged += new EventHandler(HPFilterRadioBoxChecked);
            HPFilterBox1.KeyPress += new KeyPressEventHandler(NumberInputOnly);
            HPFilterBox2.KeyPress += new KeyPressEventHandler(NumberInputOnly);

            RescaleTabHutangPiutang();

            // Init footer panel
            HPPanelFooter.Location = new Point(0, HPPanelContent.Bottom);
            HPPanelFooter.Size = new Size(PanelJualBeli.Width, TableRowHeight * 2);
            HPUnderlineTableHutang.Location = new Point(HPTableHutang.Left, 0);
            HPUnderlineTableHutang.Size = new Size(HPTableHutang.Width, 1);
            HPUnderlineTablePiutang.Location = new Point(HPTablePiutang.Left, 0);
            HPUnderlineTablePiutang.Size = new Size(HPTablePiutang.Width, 1);

            // Init miscs
            HPIconKeyboard.Click += new EventHandler(OnTooltipClick);
            HPIconKeyboard.DoubleClick += new EventHandler(OnTooltipClick);
            HPIconKeyboard.MouseLeave += new EventHandler(TooltipLostFocus);
            HPIconKeyboard.Location = new Point(HPPanelButton.Right - HPIconKeyboard.Width - CONTENT_RIGHT_MARGIN, 0);

            // Init scroll
            HPScroll.Minimum = 0;
            HPScroll.Maximum = 0;
            HPScroll.SmallChange = HPScroll.LargeChange = 1;
            HPScroll.ValueChanged += new EventHandler(ScrollValueChanged);
            HPScroll.Scroll += new ScrollEventHandler(ScrollScrolled);
            HPScroll.MouseMove += new MouseEventHandler(OnScrollHover);
            HPScroll.MouseLeave += new EventHandler(ScrollMouseLeave);

            RegisterPanelScrollEvents(PanelJualBeli, HPScroll, false);
            RegisterPanelScrollEvents(HPPanelButton, HPScroll, false);
            RegisterPanelScrollEvents(HPPanelContent, HPScroll, false);
            RegisterPanelScrollEvents(HPPanelFooter, HPScroll, false);
            RegisterPanelScrollEvents(HPPanelHeader, HPScroll, false);

            ScrollParentPanel.Add(HPScroll, HPPanelContent);
        }
        #endregion


        /***********************************
         * User Interface
         * 
         * Deals with UI
         * 
         ***********************************/
        #region

        void RescaleTabJualBeli()
        {
            int ContentAreaWidth = PanelJualBeli.Width - CONTENT_LEFT_MARGIN - CONTENT_RIGHT_MARGIN - JBScroll.Width;
            // Init header panel
            JBPanelHeader.Location = new Point(0, JBPanelButton.Bottom);
            JBPanelHeader.Size = new Size(PanelJualBeli.Width, TableHeaderHeight + JBLabelPenjualan.Height + GENERAL_SPACING * 2 - 2);
            JBLabelPembelian.Location = new Point(CONTENT_LEFT_MARGIN, 0);
            JBLabelPembelian.Size = new Size((int)(ContentAreaWidth * TabJualBeliRatio) - TAB_JUAL_BELI_TABLE_SPACING, JBLabelPembelian.Height);
            JBLabelPenjualan.Location = new Point(JBLabelPembelian.Right + TAB_JUAL_BELI_TABLE_SPACING * 2, JBLabelPembelian.Location.Y);
            JBLabelPenjualan.Size = new Size(ContentAreaWidth - JBLabelPembelian.Width, JBLabelPenjualan.Height);
            JBHeaderPembelian.Location = new Point(CONTENT_LEFT_MARGIN, JBLabelPenjualan.Bottom + GENERAL_SPACING);
            JBHeaderPembelian.Size = new Size(JBLabelPembelian.Width, TableHeaderHeight);
            JBHeaderPenjualan.Location = new Point(JBHeaderPembelian.Right + TAB_JUAL_BELI_TABLE_SPACING * 2, JBHeaderPembelian.Location.Y);
            JBHeaderPenjualan.Size = new Size(ContentAreaWidth - JBHeaderPembelian.Width, TableHeaderHeight);

            // Init content panel
            JBPanelContent.Location = new Point(0, JBPanelHeader.Bottom);
            JBPanelContent.Size = new Size(PanelJualBeli.Width, PanelJualBeli.Height - JBPanelHeader.Bottom - TableRowHeight * 2 - CONTENT_BOTTOM_MARGIN);
            JBScroll.Location = new Point(JBPanelContent.Width - JBScroll.Width, 0);//JBPanelContent.Top
            JBScroll.Size = new Size(JBScroll.Width, JBPanelContent.Height);
            JBTablePembelian.Location = new Point(JBHeaderPembelian.Location.X, 0);
            JBTablePembelian.Size = new Size(JBHeaderPembelian.Width, JBPanelContent.Height);
            JBTablePenjualan.Location = new Point(JBHeaderPenjualan.Location.X, JBTablePembelian.Location.Y);
            JBTablePenjualan.Size = new Size(JBHeaderPenjualan.Width, JBPanelContent.Height);
        }

        void RescaleTabHutangPiutang()
        {
            int ContentAreaWidth = PanelHutang.Width - CONTENT_LEFT_MARGIN - CONTENT_RIGHT_MARGIN - HPScroll.Width;
            // Init header panel
            HPPanelHeader.Location = new Point(0, HPPanelButton.Bottom);
            HPPanelHeader.Size = new Size(PanelJualBeli.Width, TableHeaderHeight + HPLabelPiutang.Height + GENERAL_SPACING * 2 - 2);
            HPLabelHutang.Location = new Point(CONTENT_LEFT_MARGIN, 0);
            HPLabelHutang.Size = new Size((int)(ContentAreaWidth * TabHutangRatio) - TAB_JUAL_BELI_TABLE_SPACING, HPLabelHutang.Height);
            HPLabelPiutang.Location = new Point(HPLabelHutang.Right + TAB_JUAL_BELI_TABLE_SPACING * 2, HPLabelHutang.Location.Y);
            HPLabelPiutang.Size = new Size(ContentAreaWidth - HPLabelHutang.Width, HPLabelPiutang.Height);
            HPHeaderHutang.Location = new Point(CONTENT_LEFT_MARGIN, HPLabelPiutang.Bottom + GENERAL_SPACING);
            HPHeaderHutang.Size = new Size(HPLabelHutang.Width, TableHeaderHeight);
            HPHeaderPiutang.Location = new Point(HPHeaderHutang.Right + TAB_JUAL_BELI_TABLE_SPACING * 2, HPHeaderHutang.Location.Y);
            HPHeaderPiutang.Size = new Size(ContentAreaWidth - HPHeaderHutang.Width, TableHeaderHeight);

            // Init content panel
            HPPanelContent.Location = new Point(0, HPPanelHeader.Bottom);
            HPPanelContent.Size = new Size(PanelJualBeli.Width, PanelJualBeli.Height - HPPanelHeader.Bottom - TableRowHeight * 2 - CONTENT_BOTTOM_MARGIN);
            HPScroll.Location = new Point(HPPanelContent.Width - HPScroll.Width, 0);//HPPanelContent.Top
            HPScroll.Size = new Size(HPScroll.Width, HPPanelContent.Height);
            HPTableHutang.Location = new Point(HPHeaderHutang.Location.X, 0);
            HPTableHutang.Size = new Size(HPHeaderHutang.Width, HPPanelContent.Height);
            HPTablePiutang.Location = new Point(HPHeaderPiutang.Location.X, HPTableHutang.Location.Y);
            HPTablePiutang.Size = new Size(HPHeaderPiutang.Width, HPPanelContent.Height);
        }

        #endregion


        /***********************************
         * Searching
         * 
         * Table searching and filtering
         * 
         ***********************************/
        #region

        void FilterTable(ListView table, SearchMethod method, object param1, object param2)
        {
            DataType[] typeSeq = TableDataTypes[table];
            DataType[] typeSeq2 = null;
            List<List<string>> datas = TableData[table];
            List<List<string>> datas2 = null;
            if (table == JBTablePenjualan)
            {
                typeSeq2 = TableDataTypes[JBTablePembelian];
                datas2 = TableData[JBTablePembelian];
            }
            else if (table == JBTablePembelian)
            {
                typeSeq2 = TableDataTypes[JBTablePenjualan];
                datas2 = TableData[JBTablePenjualan];
            }

            if (!SearchResult.ContainsKey(table))
            {
                SearchResult.Add(table, new List<List<string>>());
                SearchResultSource.Add(table, new List<int>());
            }
            List<List<string>> result = SearchResult[table];
            List<int> source = SearchResultSource[table];
            List<List<string>> result2 = null;
            List<int> source2 = null;
            if (table == JBTablePenjualan)
            {
                if (!SearchResult.ContainsKey(JBTablePembelian))
                {
                    SearchResult.Add(JBTablePembelian, new List<List<string>>());
                    SearchResultSource.Add(JBTablePembelian, new List<int>());
                }
                result2 = SearchResult[JBTablePembelian];
                source2 = SearchResultSource[JBTablePembelian];
                result2.Clear();
                source2.Clear();
            }
            else if (table == JBTablePembelian)
            {
                if (!SearchResult.ContainsKey(JBTablePenjualan))
                {
                    SearchResult.Add(JBTablePenjualan, new List<List<string>>());
                    SearchResultSource.Add(JBTablePenjualan, new List<int>());
                }
                result2 = SearchResult[JBTablePenjualan];
                source2 = SearchResultSource[JBTablePenjualan];
                result2.Clear();
                source2.Clear();
            }
            result.Clear();
            source.Clear();
            for (int i = 0; i < datas.Count; i++)
            {
                bool include = false;
                if (method == SearchMethod.Date)
                {
                    string content = GetTableContent(datas, i, table == JBTablePembelian ? 1 : 0);
                    if (content.Length > 0)
                    {
                        DateTime date = Convert.ToDateTime(content);
                        if (DateTime.Compare(date, (DateTime)param1) >= 0 && DateTime.Compare(date, (DateTime)param2) <= 0)
                        {
                            include = true;
                        }
                    }
                }
                else if (method == SearchMethod.Keyword)
                {
                    string content = GetTableContent(datas, i, (int)param1);
                    if (content == (string)param2 && ((string)param2).Length > 0)
                    {
                        include = true;
                    }
                }
                else if (method == SearchMethod.Index)
                {
                    if (i + 1 >= (int)param1 && i + 1 <= (int)param2 && i < datas.Count - 1)
                    {
                        include = true;
                    }
                }
                if (include)
                {
                    List<string> row = new List<string>();
                    for (int j = 0; j < datas[i].Count; j++)
                    {
                        if (typeSeq[j] != DataType.ReadOnly)
                            row.Add(GetTableContent(datas, i, j));
                        else
                            row.Add("");
                    }
                    result.Add(row);
                    source.Add(i);

                    if (table == JBTablePembelian || table == JBTablePenjualan)
                    {
                        row = new List<string>();
                        for (int j = 0; j < datas2[i].Count; j++)
                        {
                            if (typeSeq2[j] != DataType.ReadOnly)
                                row.Add(GetTableContent(datas2, i, j));
                            else
                                row.Add("");
                        }
                        result2.Add(row);
                        source2.Add(i);
                    }
                }
            }
            if (table == JBTablePembelian || table == JBTablePenjualan)
            {
                for (int i = 0; i < result.Count; i++)
                {
                    UpdateTablePembelian(i, JB_TABLE_PEMBELIAN_GROUP_2[0], false);
                    UpdateTablePenjualan(i, JB_TABLE_PENJUALAN_GROUP_2[0], false);
                }
                if (table == JBTablePenjualan)
                    RefreshTable(JBTablePembelian, false);
                else if (table == JBTablePembelian)
                    RefreshTable(JBTablePenjualan, false);
                JBScroll.Maximum = Math.Max(result.Count - (JBPanelContent.Height / TableRowHeight), JBScroll.Minimum);
                JBScroll.Visible = JBScroll.Maximum > JBScroll.Minimum;
            }
            RefreshTable(table, false);
            RefreshPanelJualBeliFooter();
        }
        #endregion


        /***********************************
         * Core
         * 
         * Codes operating the main program
         * 
         ***********************************/
        #region

        List<List<string>> GetDataSource(ListView table)
        {
            return Searching && (table == JBTablePembelian || table == JBTablePenjualan) ? SearchResult[table] : TableData[table];
        }

        void TotalUpdateTable(ListView table)
        {
            if (table == JBTablePembelian || table == JBTablePenjualan)
            {
                if (table == JBTablePembelian)
                {
                    JBFJumlahCount = 0;
                    JBFJumlah = 0;
                    JBFNettoBeli = 0;
                }
                else
                {
                    JBFLabaCount = 0;
                    JBFLaba = 0;
                    JBFRugiCount = 0;
                    JBFRugi = 0;
                    JBFLabaBersih = 0;
                    JBFNettoJual = 0;
                    JBFJumlahJual = 0;
                    JBFJumlahJualCount = 0;
                }
                List<List<string>> datas = GetDataSource(table);
                for (int i = 0; i < datas.Count; i++)
                {
                    if (table == JBTablePembelian)
                    {
                        SetTableContent(table, datas, i, 7, "");
                        SetTableContent(table, datas, i, 11, "");
                    }
                    else
                    {
                        SetTableContent(table, datas, i, 4, "");
                        SetTableContent(table, datas, i, 6, "");
                        SetTableContent(table, datas, i, 7, "");
                    }
                    if (table == JBTablePembelian)
                        UpdateTablePembelian(i, JB_TABLE_PEMBELIAN_GROUP_2[0], false);
                    else
                        UpdateTablePenjualan(i, JB_TABLE_PENJUALAN_GROUP_2[0], false);
                }
                RefreshPanelJualBeliFooter();
            }
            else if (table == HPTableHutang || table == HPTablePiutang)
            {
                if (table == HPTableHutang)
                {
                    HPFHutang = 0;
                    HPFSisaHutang = 0;
                }
                else
                {
                    HPFPiutang = 0;
                    HPFSisaPiutang = 0;
                }
                List<List<string>> datas = GetDataSource(table);
                for (int i = 0; i < datas.Count; i++)
                {
                    if (table == HPTableHutang)
                        SetTableContent(table, datas, i, 5, "");
                    else
                        SetTableContent(table, datas, i, 6, "");

                    if (table == HPTableHutang)
                        UpdateTableHutang(i, HP_TABLE_HUTANG_GROUP_1[0], false);
                    else
                        UpdateTablePiutang(i, HP_TABLE_PIUTANG_GROUP_1[0], false);
                }
                RefreshPanelHutangFooter();
            }
            RefreshTable(table, false);
        }

        void UpdateTable(ListView table, int row, int col, bool refresh)
        {
            if (table == JBTablePembelian)
                UpdateTablePembelian(row, col, refresh);
            else if (table == JBTablePenjualan)
                UpdateTablePenjualan(row, col, refresh);
            else if (table == HPTableHutang)
                UpdateTableHutang(row, col, refresh);
            else if (table == HPTablePiutang)
                UpdateTablePiutang(row, col, refresh);
        }

        void UpdateTablePembelian(int row, int col, bool refresh)
        {
            if (Array.Exists(JB_TABLE_PEMBELIAN_GROUP_1, e => e == col))
            {
                List<List<string>> datas = GetDataSource(JBTablePembelian);
                string content7 = GetTableContent(datas, row, 7);
                string content8 = GetTableContent(datas, row, 9);
                string content9 = GetTableContent(datas, row, 10);
                string content10 = GetTableContent(datas, row, 11);
                if (Array.Exists(JB_TABLE_PEMBELIAN_GROUP_2, e => e == col))
                {
                    // Update JBPembelian Netto
                    string content5 = GetTableContent(datas, row, 5);
                    string content6 = GetTableContent(datas, row, 6);
                    int bruto = content5.Length > 0 ? PotongBerat(Convert.ToInt32(content5)) : 0;
                    int tembak = content6.Length > 0 ? Convert.ToInt32(content6) : 0;
                    int prev2 = content7.Length > 0 ? Convert.ToInt32(content7) : 0;
                    int nett = Math.Max(bruto - tembak, 0);
                    JBFNettoBeli -= prev2;
                    if (content5.Length > 0)
                    {
                        JBFNettoBeli += nett;
                        content7 = nett + "";
                        SetTableContent(JBTablePembelian, datas, row, 7, content7);
                    }
                    else
                    {
                        content7 = "";
                        SetTableContent(JBTablePembelian, datas, row, 7, "");
                    }
                }
                // Update JBPembelian Jumlah
                int harga = content8.Length > 0 ? Convert.ToInt32(ALPHA_NUMERICS_ONLY.Replace(content8, "")) : 0;
                int tumplek = content9.Length > 0 ? Convert.ToInt32(ALPHA_NUMERICS_ONLY.Replace(content9, "")) : 0;
                int netto = content7.Length > 0 ? Convert.ToInt32(content7) : 0;
                int prev = content10.Length > 0 ? Convert.ToInt32(ALPHA_NUMERICS_ONLY.Replace(content10, "")) : 0;
                int jumlah = Math.Max(harga * netto - tumplek, 0);
                JBFJumlah -= prev;
                if (content8.Length > 0)
                {
                    if (content10.Length == 0)
                    {
                        JBFJumlahCount++;
                    }
                    JBFJumlah += jumlah;
                    SetTableContent(JBTablePembelian, datas, row, 11, ToCurrency(jumlah + ""));
                }
                else
                {
                    if (content10.Length > 0)
                    {
                        JBFJumlahCount--;
                    }
                    SetTableContent(JBTablePembelian, datas, row, 11, "");
                }
                if (refresh)
                {
                    RefreshTable(JBTablePembelian, false);
                    RefreshPanelJualBeliFooter();
                }
            }
        }

        void UpdateTablePenjualan(int row, int col, bool refresh)
        {
            if (Array.Exists(JB_TABLE_PENJUALAN_GROUP_1, e => e == col))
            {
                List<List<string>> datas = GetDataSource(JBTablePenjualan);
                string content4 = GetTableContent(datas, row, 4);
                string content5 = GetTableContent(datas, row, 5);
                string content6 = GetTableContent(datas, row, 6);
                string content7 = GetTableContent(datas, row, 7);
                if (Array.Exists(JB_TABLE_PENJUALAN_GROUP_2, e => e == col))
                {
                    string content2 = GetTableContent(datas, row, 2);
                    string content3 = GetTableContent(datas, row, 3);
                    // Update JBPenjualan Netto
                    int bruto = content2.Length > 0 ? PotongBerat(Convert.ToInt32(content2)) : 0;
                    int tembak = content3.Length > 0 ? Convert.ToInt32(content3) : 0;
                    int prev2 = content4.Length > 0 ? Convert.ToInt32(content4) : 0;
                    int nett = Math.Max(bruto - tembak, 0);
                    JBFNettoJual -= prev2;
                    if (content2.Length > 0)
                    {
                        JBFNettoJual += nett;
                        content4 = nett + "";
                        SetTableContent(JBTablePenjualan, datas, row, 4, content4);
                    }
                    else
                    {
                        content4 = "";
                        SetTableContent(JBTablePenjualan, datas, row, 4, content4);
                    }
                }
                string target = GetTableContent(GetDataSource(JBTablePembelian), row, 11);
                // Update JBPenjualan Jumlah
                int harga = content5.Length > 0 ? Convert.ToInt32(ALPHA_NUMERICS_ONLY.Replace(content5, "")) : 0;
                int netto = content4.Length > 0 ? Convert.ToInt32(content4) : 0;
                int total = target.Length > 0 ? Math.Max(harga * netto, 0) : 0;
                int prev = content6.Length > 0 ? Convert.ToInt32(ALPHA_NUMERICS_ONLY.Replace(content6, "")) : 0;
                int beli = target.Length > 0 ? Convert.ToInt32(ALPHA_NUMERICS_ONLY.Replace(target, "")) : 0;
                int laba = (total - beli);
                int prev3 = content7.Length > 0 ? Convert.ToInt32(ALPHA_NUMERICS_ONLY.Replace(content7, "")) : 0;
                JBFJumlahJual -= prev;
                if (prev3 < 0)
                {
                    JBFRugiCount--;
                    JBFRugi += prev3;
                }
                else if (prev3 > 0)
                {
                    JBFLabaCount--;
                    JBFLaba -= prev3;
                }
                JBFLabaBersih += laba - prev3;
                if (laba < 0)
                {
                    JBFRugiCount++;
                    JBFRugi -= laba;
                }
                else if (laba > 0)
                {
                    JBFLabaCount++;
                    JBFLaba += laba;
                }
                if (content5.Length > 0 || target.Length > 0)
                {
                    if (content5.Length > 0)
                    {
                        if (content6.Length == 0 && target.Length > 0)
                            JBFJumlahJualCount++;
                        JBFJumlahJual += total;
                        SetTableContent(JBTablePenjualan, datas, row, 6, ToCurrency(total + ""));
                    }
                    else
                    {
                        if (content6.Length > 0)
                            JBFJumlahJualCount--;
                        SetTableContent(JBTablePenjualan, datas, row, 6, "");
                    }
                    SetTableContent(JBTablePenjualan, datas, row, 7, ToCurrency(laba + ""));

                }
                else
                {
                    if (content6.Length > 0)
                        JBFJumlahJualCount--;
                    SetTableContent(JBTablePenjualan, datas, row, 6, "");
                    SetTableContent(JBTablePenjualan, datas, row, 7, "");
                }
                if (refresh)
                {
                    RefreshTable(JBTablePenjualan, false);
                    RefreshPanelJualBeliFooter();
                }
            }
        }

        void UpdateTableHutang(int row, int col, bool refresh, string prev = "", bool replace = true)
        {
            if (Array.Exists(HP_TABLE_HUTANG_GROUP_1, e => e == col))
            {
                List<List<string>> datas = GetDataSource(HPTableHutang);
                string content4 = GetTableContent(datas, row, 4);
                string content5 = GetTableContent(datas, row, 5);
                string content6 = GetTableContent(datas, row, 6);
                int oldJumlah = prev.Length > 0 ? Convert.ToInt32(ALPHA_NUMERICS_ONLY.Replace(prev, "")) : 0;
                int jumlah = content4.Length > 0 ? Convert.ToInt32(ALPHA_NUMERICS_ONLY.Replace(content4, "")) : 0;
                int sisa = content5 == TEXT_LUNAS ? 0 : (content5.Length > 0 ? Convert.ToInt32(ALPHA_NUMERICS_ONLY.Replace(content5, "")) : 0);
                HPFSisaHutang -= sisa;
                HPFHutang -= oldJumlah;
                if (content4.Length > 0 && prev != content4)
                {
                    HPFHutang += jumlah;
                    if (content6.Length > 0)
                    {
                        int cicilan = content6.Length > 0 ? Convert.ToInt32(ALPHA_NUMERICS_ONLY.Replace(content6, "")) : 0;
                        if (cicilan > sisa)
                            cicilan += sisa - cicilan;
                        sisa -= cicilan;
                        HPFSisaHutang += sisa;
                        if (replace)
                        {
                            if (sisa > 0)
                                SetTableContent(HPTableHutang, datas, row, 5, ToCurrency(Math.Max(sisa, 0) + ""));
                            else
                                SetTableContent(HPTableHutang, datas, row, 5, TEXT_LUNAS);
                        }
                        SetTableContent(HPTableHutang, datas, row, 6, "");
                    }
                    else
                    {
                        HPFSisaHutang += jumlah;
                        SetTableContent(HPTableHutang, datas, row, 5, content4);
                    }
                }
                else
                {
                    SetTableContent(HPTableHutang, datas, row, 5, "");
                }
                if (refresh)
                {
                    RefreshTable(HPTableHutang, false);
                    RefreshPanelHutangFooter();
                }
            }
        }

        void UpdateTablePiutang(int row, int col, bool refresh, string prev = "", bool replace = true)
        {
            if (Array.Exists(HP_TABLE_PIUTANG_GROUP_1, e => e == col))
            {
                List<List<string>> datas = GetDataSource(HPTablePiutang);
                string content4 = GetTableContent(datas, row, 4);
                string content5 = GetTableContent(datas, row, 5);
                string content6 = GetTableContent(datas, row, 6);
                string content7 = GetTableContent(datas, row, 7);
                int oldJumlah = prev.Length > 0 ? Convert.ToInt32(ALPHA_NUMERICS_ONLY.Replace(prev, "")) : 0;
                int jumlah = content4.Length > 0 ? Convert.ToInt32(ALPHA_NUMERICS_ONLY.Replace(content4, "")) : 0;
                float bunga = 1f + (content5.Length > 0 ? float.Parse(ALPHA_DECIMAL_ONLY.Replace(content5, ""), System.Globalization.CultureInfo.InvariantCulture) / 100f : 0);
                int sisa = content6 == TEXT_LUNAS ? 0 : (content6.Length > 0 ? Convert.ToInt32(ALPHA_NUMERICS_ONLY.Replace(content6, "")) : 0);
                HPFSisaPiutang -= sisa;
                HPFPiutang -= oldJumlah;
                jumlah = (int)((float)jumlah * bunga);
                if (content4.Length > 0 && prev != content4)
                {
                    HPFSisaPiutang += jumlah;
                    HPFPiutang += jumlah;
                    if (content7.Length > 0)
                    {
                        int cicilan = content7.Length > 0 ? Convert.ToInt32(ALPHA_NUMERICS_ONLY.Replace(content7, "")) : 0;
                        sisa -= cicilan;
                        if (replace)
                        {
                            if (sisa > 0)
                                SetTableContent(HPTablePiutang, datas, row, 6, ToCurrency(Math.Max(sisa, 0) + ""));
                            else
                                SetTableContent(HPTablePiutang, datas, row, 6, TEXT_LUNAS);
                        }
                        SetTableContent(HPTablePiutang, datas, row, 7, "");
                    }
                    else
                        SetTableContent(HPTablePiutang, datas, row, 6, ToCurrency(jumlah + ""));
                }
                else
                {
                    SetTableContent(HPTablePiutang, datas, row, 6, "");
                }
                if (refresh)
                {
                    RefreshTable(HPTablePiutang, false);
                    RefreshPanelHutangFooter();
                }
            }
        }

        string GetTableContent(List<List<string>> datas, int row, int col)
        {
            return row < datas.Count && row >= 0 && col >= 0 && col < datas[row].Count ? datas[row][col] : "";
        }

        void SetTableContent(ListView table, List<List<string>> datas, int row, int col, string val)
        {
            while (datas.Count <= row)
            {
                int i = datas.Count;
                List<string> cols = new List<string>(table.Columns.Count);
                datas.Add(cols);
                while (cols.Count < cols.Capacity)
                    datas[i].Add("");
            }
            while (datas[row].Count < col)
                datas[row].Add("");
            bool wantSave = false;
            if (row < datas.Count && col < datas[row].Capacity)
            {
                wantSave = val != datas[row][col];
                datas[row][col] = val;
            }

            DataType[] typeSeq = TableDataTypes[table];
            if ((typeSeq[col] != DataType.ReadOnly && typeSeq[col] != DataType.SavedReadOnly || col > 0) && Searching && (table == JBTablePembelian || table == JBTablePenjualan))
            {
                List<List<string>> datas2 = TableData[table];
                row = SearchResultSource[table][row];
                while (datas2.Count <= row)
                {
                    int i = datas2.Count;
                    List<string> cols = new List<string>(table.Columns.Count);
                    datas2.Add(cols);
                    while (cols.Count < cols.Capacity)
                        datas2[i].Add("");
                }
                while (datas2[row].Count < col)
                    datas2[row].Add("");
                if (row < datas2.Count && col < datas2[row].Capacity)
                {
                    datas2[row][col] = val;
                }
                datas = datas2;
            }
            if (wantSave)
            {
                if (typeSeq[col] != DataType.ReadOnly && typeSeq[col] != DataType.SavedReadOnly)
                {
                    string file = APP_PATH + SAVE_DIRECTORY + "temp/" + table.Name.ToLower();
                    if (!Directory.Exists(file))
                    {
                        Directory.CreateDirectory(file);
                    }
                    file += "/entry-" + row + EXTENSION;
                    if (!File.Exists(file))
                    {
                        File.Create(file).Close();
                    }
                    StreamWriter writer = new StreamWriter(file);
                    writer.Write(""); // Clear file

                    string line = "";
                    for (int j = 0; j < datas[row].Count; j++)
                    {
                        if ((typeSeq[j] != DataType.ReadOnly))
                            line += GetTableContent(datas, row, j);
                        if (j < datas[row].Count - 1)
                            line += "|";
                    }
                    string save = Simple3Des.Encrypt(line, true);
                    writer.WriteLine(save);
                    writer.Close();

                    if (table == JBTablePembelian && row == datas.Count - 1)
                    {
                        file = APP_PATH + SAVE_DIRECTORY + "temp/" + JBTablePenjualan.Name.ToLower();
                        if (!Directory.Exists(file))
                        {
                            Directory.CreateDirectory(file);
                        }
                        file += "/entry-" + row + EXTENSION;
                        if (!File.Exists(file))
                        {
                            File.Create(file).Close();
                            writer = new StreamWriter(file);
                            typeSeq = TableDataTypes[JBTablePenjualan];
                            writer.Write(""); // Clear file

                            line = "";
                            datas = TableData[JBTablePenjualan];
                            for (int j = 0; j < JBTablePenjualan.Columns.Count; j++)
                            {
                                if (j < JBTablePenjualan.Columns.Count - 1)
                                    line += "|";
                            }
                            save = Simple3Des.Encrypt(line, true);
                            writer.WriteLine(save);
                            writer.Close();
                        }
                    }
                }
            }
        }

        void HideEditor(bool apply)
        {
            if (CurrentEditor != null)
            {
                List<List<string>> datas = GetDataSource(CurrentTable);
                int scrollOffset = ControlScroller[CurrentTable].Value;
                string prevValue = GetTableContent(datas, TargetRow + scrollOffset, TargetCol);
                bool createNew = false;
                editorBlocker.Hide();
                var box = CurrentEditor as TextBox;
                if (box != null)
                {
                    DataType[] typeSeq = TableDataTypes[CurrentTable];
                    apply &= prevValue != (typeSeq[TargetCol] == DataType.Currency ? ToCurrency(box.Text) : box.Text);
                    if (apply)
                    {
                        if (typeSeq[TargetCol] == DataType.Currency)
                        {
                            SetTableContent(CurrentTable, datas, TargetRow + scrollOffset, TargetCol, ToCurrency(box.Text));
                        }
                        else if (typeSeq[TargetCol] == DataType.Decimal)
                        {
                            SetTableContent(CurrentTable, datas, TargetRow + scrollOffset, TargetCol, box.Text.Replace(',', '.'));
                        }
                        else
                        {
                            SetTableContent(CurrentTable, datas, TargetRow + scrollOffset, TargetCol, box.Text);
                        }
                        createNew = box.Text.Length > 0;
                    }
                    box.Hide();
                }
                else
                {
                    var picker = CurrentEditor as DateTimePicker;
                    if (picker != null)
                    {
                        apply &= prevValue != picker.Text;
                        if (apply)
                        {
                            SetTableContent(CurrentTable, datas, TargetRow + scrollOffset, TargetCol, picker.Text);
                            createNew = picker.Text.Length > 0;
                        }
                        picker.Hide();
                    }
                    else
                    {
                        var dropdown = CurrentEditor as ComboBox;
                        if (dropdown != null)
                        {
                            if (dropdownField.Items.IndexOf(dropdownField.Text) < 0)
                            {
                                dropdownField.Items.Add(dropdownField.Text);
                            }
                            dropdown.Items.Add("k(*&^5a*bnA12nD12S");
                            apply &= prevValue != dropdown.Text;
                            if (apply)
                            {
                                SetTableContent(CurrentTable, datas, TargetRow + scrollOffset, TargetCol, dropdown.Text);
                                createNew = dropdown.Text.Length > 0;
                            }
                            dropdown.Hide();
                        }
                    }
                }
                if (apply)
                {
                    // Auto-fill
                    if (CurrentTable == JBTablePembelian)
                    {
                        if (TargetCol == 2)
                        {
                            string txt = dropdownField.Text;
                            int dex = dropdownField.SelectedIndex - dropdownField.Items.IndexOf(txt);
                            if (dex > -1)
                            {
                                RefreshDropdownItems(CurrentTable, TargetCol, "");
                                int dex2 = dropdownField.Items.IndexOf(txt);
                                if (dex2 > -1)
                                {
                                    for (int i = dex + dex2; i < dropdownField.Items.Count && i > -1; i++)
                                    {
                                        if (txt == dropdownField.Items[i].ToString())
                                        {
                                            dex = i;
                                            break;
                                        }
                                    }
                                }
                                else
                                    dex = -1;
                            }
                            //if (dex < 0)
                            //    dex = dropdownField.Items.IndexOf(dropdownField.Text);
                            if (dex > -1)
                                SetTableContent(CurrentTable, datas, TargetRow + scrollOffset, 3, TargetSource.Key[dex][2]);
                            //else
                            //    CurrentTable.Items[TargetRow].SubItems[3].Text = "";
                        }
                        else
                        {
                            UpdateTablePembelian(TargetRow + scrollOffset, TargetCol, true);
                            UpdateTablePenjualan(TargetRow + scrollOffset, JB_TABLE_PENJUALAN_GROUP_2[0], true);
                        }
                    }
                    else if (CurrentTable == JBTablePenjualan)
                    {
                        UpdateTablePenjualan(TargetRow + scrollOffset, TargetCol, true);
                    }
                    else if (CurrentTable == PPTablePetani)
                    {
                        List<List<string>> datas2 = GetDataSource(JBTablePembelian);
                        string value = GetTableContent(datas, TargetRow + scrollOffset, TargetCol);
                        if (TargetCol == 1)
                        {
                            for (int i = 0; i < datas2.Count; i++)
                            {
                                string content = GetTableContent(datas2, i, 2);
                                if (content.Length > 0 && content == prevValue)
                                {
                                    SetTableContent(JBTablePembelian, datas2, i, 2, value);
                                }
                            }
                            if (Searching)
                            {
                                Searching = false;
                                datas2 = TableData[JBTablePembelian];
                                for (int i = 0; i < datas2.Count; i++)
                                {
                                    string content = GetTableContent(datas2, i, 2);
                                    if (content.Length > 0 && content == prevValue)
                                    {
                                        SetTableContent(JBTablePembelian, datas2, i, 2, value);
                                    }
                                }
                                Searching = true;
                            }
                        }
                        else if (TargetCol == 2)
                        {
                            for (int i = 0; i < datas2.Count; i++)
                            {
                                string content2 = GetTableContent(datas2, i, 2);
                                string content3 = GetTableContent(datas2, i, 3);
                                if (content2.Length > 0 && content2 == GetTableContent(datas, TargetRow + scrollOffset, TargetCol - 1) && content3.Length > 0 && content3 == prevValue)
                                {
                                    SetTableContent(JBTablePembelian, datas2, i, 3, value);
                                }
                            }
                            if (Searching)
                            {
                                Searching = false;
                                datas2 = TableData[JBTablePembelian];
                                for (int i = 0; i < datas2.Count; i++)
                                {
                                    string content2 = GetTableContent(datas2, i, 2);
                                    string content3 = GetTableContent(datas2, i, 3);
                                    if (content2.Length > 0 && content2 == GetTableContent(datas, TargetRow + scrollOffset, TargetCol - 1) && content3.Length > 0 && content3 == prevValue)
                                    {
                                        SetTableContent(JBTablePembelian, datas2, i, 3, value);
                                    }
                                }
                                Searching = true;
                            }
                        }
                        RefreshTable(JBTablePembelian, false);
                    }
                    else if (CurrentTable == PPTablePembeli)
                    {
                        List<List<string>> datas2 = GetDataSource(JBTablePembelian);
                        if (TargetCol == 1)
                        {
                            string value = GetTableContent(datas, TargetRow + scrollOffset, TargetCol);
                            for (int i = 0; i < datas2.Count; i++)
                            {
                                string content = GetTableContent(datas2, i, 4);
                                if (content.Length > 0 && content == prevValue)
                                {
                                    SetTableContent(JBTablePembelian, datas2, i, 4, value);
                                }
                            }
                            if (Searching)
                            {
                                Searching = false;
                                datas2 = TableData[JBTablePembelian];
                                for (int i = 0; i < datas2.Count; i++)
                                {
                                    string content = GetTableContent(datas2, i, 4);
                                    if (content.Length > 0 && content == prevValue)
                                    {
                                        SetTableContent(JBTablePembelian, datas2, i, 4, value);
                                    }
                                }
                                Searching = true;
                            }
                            RefreshTable(JBTablePembelian, false);
                        }
                    }
                    else if (CurrentTable == HPTableHutang)
                    {
                        UpdateTableHutang(TargetRow + scrollOffset, TargetCol, true, prevValue);
                    }
                    else if (CurrentTable == HPTablePiutang)
                    {
                        UpdateTablePiutang(TargetRow + scrollOffset, TargetCol, true, prevValue);
                    }
                    if (!Searching && (createNew || CurrentTable == JBTablePembelian) && TargetRow + scrollOffset == datas.Count - 1 && CurrentTable != JBTablePenjualan)
                    {
                        InsertEmptyToTable(CurrentTable, false);
                    }
                    RefreshTable(CurrentTable, false);
                }
                CurrentTable.Focus();
                CurrentTable.Items[TargetRow].Selected = true;
                CurrentTable.Items[TargetRow].Focused = true;
                CurrentEditor = null;
            }
        }

        void ShowEditor(ListView table, int col, int row, bool open)
        {
            if (TableDataTypes.ContainsKey(table) && table.SelectedItems.Count <= 1)
            {
                VScrollBar scroll = ControlScroller[table];
                List<List<string>> datas = GetDataSource(table);
                if (table == JBTablePenjualan)
                {
                    List<List<string>> datas2 = GetDataSource(JBTablePembelian);
                    if (GetTableContent(datas2, row + scroll.Value, 11).Length == 0)
                    {
                        Rectangle r = table.Items[row].SubItems[col].Bounds;
                        PrintWarning("x", table.Parent, table.Parent.PointToScreen(new Point(table.Left + r.Left, table.Top + r.Top)), col == 0 ? new Size(table.Columns[0].Width, r.Height) : r.Size, EMPTY_CELL_WARNING_BG_COLOR, 3000, false);
                        PrintWarning("Tidak dapat menyunting sel ini karena bagian pembelian belum diisi", table.Parent.Parent, table.Parent.PointToScreen(new Point(table.Left + r.Left, table.Top + r.Bottom)), NO_SIZE, Color.White, 0, true);
                        //if (JBTablePembelian.Items[row].SubItems[5].Text.Length == 0)
                        //{
                        //    r = JBTablePembelian.Items[row].SubItems[5].Bounds;
                        //    PrintWarning("?", table.Parent, table.Parent.PointToScreen(new Point(JBTablePembelian.Left + r.Left, JBTablePembelian.Top + r.Top)), r.Size, EMPTY_CELL_WARNING_BG_COLOR, 3000);
                        //}
                        if (GetTableContent(datas2, row + scroll.Value, 8).Length == 0)
                        {
                            r = JBTablePembelian.Items[row].SubItems[8].Bounds;
                            PrintWarning("?", table.Parent, table.Parent.PointToScreen(new Point(JBTablePembelian.Left + r.Left, JBTablePembelian.Top + r.Top)), r.Size, EMPTY_CELL_WARNING_BG_COLOR, 3000, true);
                        }
                        return;
                    }
                }
                if (CurrentEditor != null)
                    HideEditor(true);
                DataType[] typeSeq = TableDataTypes[table];
                if (typeSeq[col] != DataType.ReadOnly && typeSeq[col] != DataType.SavedReadOnly)
                {
                    ListViewItem itm = table.Items[row];
                    if (typeSeq[col] == DataType.Date)
                    {
                        CurrentEditor = datePicker;
                        datePicker.Location = table.PointToScreen(new Point(itm.SubItems[col].Bounds.Left, itm.SubItems[col].Bounds.Top));
                        if (col == 0)
                            datePicker.Size = new Size(table.Columns[col].Width + 17, itm.SubItems[col].Bounds.Height);
                        else
                            datePicker.Size = new Size(itm.SubItems[col].Bounds.Width + 17, itm.SubItems[col].Bounds.Height);
                        if (itm.SubItems[col].Text.Length > 0)
                            datePicker.Text = itm.SubItems[col].Text;
                        else
                            datePicker.Text = "";
                        datePicker.Show();
                        datePicker.BringToFront();
                        datePicker.Focus();
                        if (open)
                        {
                            datePicker.Select();
                            SendKeys.Send("%{DOWN}");
                        }
                    }
                    else if (typeSeq[col] == DataType.Currency || typeSeq[col] == DataType.Integer)
                    {
                        CurrentEditor = intField;
                        //intField.Parent = table;
                        intField.Location = table.PointToScreen(new Point(itm.SubItems[col].Bounds.Left, itm.SubItems[col].Bounds.Top));
                        intField.Size = itm.SubItems[col].Bounds.Size;
                        if (itm.SubItems[col].Text.Length > 0)
                        {
                            if (typeSeq[col] == DataType.Currency)
                                intField.Text = ALPHA_NUMERICS_ONLY.Replace(itm.SubItems[col].Text, "");
                            else
                                intField.Text = itm.SubItems[col].Text;
                        }
                        else
                            intField.Text = "";
                        intField.Show();
                        intField.BringToFront();
                        intField.Focus();
                    }
                    else if (typeSeq[col] == DataType.Decimal)
                    {
                        CurrentEditor = decimalField;
                        //intField.Parent = table;
                        decimalField.Location = table.PointToScreen(new Point(itm.SubItems[col].Bounds.Left, itm.SubItems[col].Bounds.Top));
                        decimalField.Size = itm.SubItems[col].Bounds.Size;
                        if (itm.SubItems[col].Text.Length > 0)
                        {
                            if (typeSeq[col] == DataType.Currency)
                                decimalField.Text = ALPHA_DECIMAL_ONLY.Replace(itm.SubItems[col].Text, "");
                            else
                                decimalField.Text = itm.SubItems[col].Text;
                        }
                        else
                            decimalField.Text = "";
                        decimalField.Show();
                        decimalField.BringToFront();
                        decimalField.Focus();
                    }
                    else if (typeSeq[col] == DataType.String)
                    {
                        CurrentEditor = stringField;
                        //stringField.Parent = table;
                        stringField.Location = table.PointToScreen(new Point(itm.SubItems[col].Bounds.Left, itm.SubItems[col].Bounds.Top));
                        stringField.Size = itm.SubItems[col].Bounds.Size;
                        if (itm.SubItems[col].Text.Length > 0)
                            stringField.Text = itm.SubItems[col].Text;
                        else
                            stringField.Text = "";
                        stringField.Show();
                        stringField.BringToFront();
                        stringField.Focus();
                    }
                    else if (typeSeq[col] == DataType.Dropdown)
                    {
                        CurrentEditor = dropdownField;
                        //dropdownField.Parent = table;
                        dropdownField.Location = table.PointToScreen(new Point(itm.SubItems[col].Bounds.Left, itm.SubItems[col].Bounds.Top));
                        dropdownField.Size = new Size(itm.SubItems[col].Bounds.Width + 17, itm.SubItems[col].Bounds.Height);
                        if (itm.SubItems[col].Text.Length > 0)
                            dropdownField.Text = itm.SubItems[col].Text;
                        else
                            dropdownField.Text = "";

                        RefreshDropdownItems(table, col, "");

                        //dropdownField.DroppedDown = true;
                        dropdownField.Show();
                        dropdownField.BringToFront();
                        dropdownField.Focus();
                    }
                }
                else
                {
                    Rectangle r = table.Items[row].SubItems[col].Bounds;
                    PrintWarning("x", table.Parent, table.Parent.PointToScreen(new Point(table.Left + r.Left, table.Top + r.Top)), col == 0 ? new Size(table.Columns[0].Width, r.Height) : r.Size, EMPTY_CELL_WARNING_BG_COLOR, 3000, false);
                    PrintWarning("Sel ini tidak dapat disunting (read-only field)", table.Parent.Parent, table.Parent.PointToScreen(new Point(table.Left + r.Left, table.Top + r.Bottom)), NO_SIZE, Color.White, 0, true);
                }
                if (CurrentEditor != null)
                {
                    CurrentTable = table;
                    TargetRow = row;
                    TargetCol = col;
                    //CurrentTable.Refresh();
                    //editorBlocker.Parent = table;
                    //editorBlocker.BringToFront();
                    //editorBlocker.Show();
                }
            }
        }

        void RefreshDropdownItems(ListView table, int col, string keyword)
        {
            keyword = keyword.ToLower();
            Dictionary<int, KeyValuePair<List<List<string>>, int>> source = DataSources[table];
            KeyValuePair<List<List<string>>, int> target = TargetSource = source[col];

            dropdownField.Items.Clear();
            for (int i = 0; i < target.Key.Count - 1; i++)
            {
                string txt = target.Key[i][target.Value].ToLower();
                if (txt.Length > 0 && (keyword.Length == 0 || txt.IndexOf(keyword) > -1))
                    dropdownField.Items.Add(target.Key[i][target.Value]);
            }
        }

        void AddNewTab(Button tab, Panel panel)
        {
            tab.BackColor = INACTIVE_TAB_COLOR;
            tab.ForeColor = INACTIVE_TAB_FONT_COLOR;
            tab.FlatStyle = INACTIVE_TAB_FLAT_STYLE;
            tab.Font = InactiveTabFont;
            tab.Size = new Size(tab.Width, 0);
            tab.Location = new Point(tab.Location.X, PanelPemasukan.Top - tab.Height + 1);
            int x = Tabs.Count > 0 ? Tabs.Keys.ElementAt(Tabs.Count - 1).Right : PANEL_LEFT_MARGIN;
            tab.Location = new Point(x, PANEL_TOP_MARGIN);
            panel.Location = new Point(PANEL_LEFT_MARGIN, tab.Bottom - 1);
            panel.Size = new Size(ActiveScreen.Bounds.Width - PANEL_LEFT_MARGIN - PANEL_RIGHT_MARGIN, ActiveScreen.Bounds.Height - tab.Height - PANEL_TOP_MARGIN - PANEL_BOTTOM_MARGIN);
            Tabs.Add(tab, panel);
            tab.Click += new EventHandler(TabButtonPressed);
        }

        void AddNewTable(ListView header, ListView content)
        {
            for (int i = 0; i < header.Columns.Count; i++)
            {
                content.Columns[i].Width = header.Columns[i].Width;
            }
            TableData.Add(content, new List<List<string>>(content.Columns.Count));
            Tables.Add(header, content);
            header.ColumnWidthChanging += new ColumnWidthChangingEventHandler(ColumnSizeChanging);
            content.MouseClick += new MouseEventHandler(OnTableClick);
            content.MouseDown += new MouseEventHandler(OnDragStart);
            content.MouseMove += new MouseEventHandler(OnMouseDrag);
            content.MouseUp += new MouseEventHandler(OnDragFinish);
            content.KeyDown += new KeyEventHandler(OnTableKeypress);
            content.LostFocus += new EventHandler(OnTableLostFocus);
            content.KeyDown += new KeyEventHandler(SearchTableIndex);
            content.ItemSelectionChanged += new ListViewItemSelectionChangedEventHandler(OnTableSelection);
            //content.MouseMove += new MouseEventHandler(OnTableHover);
        }

        void RegisterPanelScrollEvents(Panel panel, VScrollBar bar, bool registerSelf)
        {
            foreach (Control c in panel.Controls)
            {
                if (!ControlScroller.ContainsKey(c) && !(c is Panel) && !(c is VScrollBar))
                {
                    ControlScroller.Add(c, bar);
                    c.MouseWheel += new MouseEventHandler(OnControlScroll);
                }
            }
            if (registerSelf && !ControlScroller.ContainsKey(panel))
            {
                ControlScroller.Add(panel, bar);
                panel.MouseWheel += new MouseEventHandler(OnControlScroll);
            }
        }

        Control SetActiveTab(Button tab)
        {
            if (CurrentTab != null)
            {
                CurrentTab.BackColor = INACTIVE_TAB_COLOR;
                CurrentTab.ForeColor = INACTIVE_TAB_FONT_COLOR;
                CurrentTab.FlatStyle = INACTIVE_TAB_FLAT_STYLE;
                CurrentTab.Font = InactiveTabFont;
                CurrentTab.Size = new Size(CurrentTab.Width, 0);
                CurrentTab.Location = new Point(CurrentTab.Location.X, PanelPemasukan.Top - CurrentTab.Height + 1);
                Tabs[CurrentTab].Visible = false;
            }
            tab.BackColor = ACTIVE_TAB_COLOR;
            tab.ForeColor = ACTIVE_TAB_FONT_COLOR;
            tab.FlatStyle = ACTIVE_TAB_FLAT_STYLE;
            tab.Font = ActiveTabFont;
            tab.Size = new Size(tab.Width, ActiveTabHeight);
            tab.Location = new Point(tab.Location.X, PanelPemasukan.Top - tab.Height + 1);
            TabBlocker.Size = new Size(tab.Width - 2, 2);
            TabBlocker.Location = new Point(tab.Left + 1, tab.Bottom - 1);
            TabBlocker.BringToFront();
            Panel panel = Tabs[tab];
            panel.Visible = true;
            Control c = panel;
            if (panel.Controls.Count > 0)
            {
                foreach (Control c2 in panel.Controls)
                {
                    if (c2 is Panel)
                    {
                        foreach (Control c3 in c2.Controls)
                        {
                            if (!(c3 is Panel) && !(c3 is VScrollBar) && c3.Visible)
                            {
                                c = c3;
                                break;
                            }
                        }
                    }
                    else if (c2.Visible && !(c2 is VScrollBar))
                    {
                        c = c2;
                        break;
                    }
                }
            }
            CurrentTab = tab;
            c.Focus();
            return c;
        }

        void LoadTableData(ListView table)
        {
            string file;
            if (table == PPTablePembeli || table == PPTablePetani)
                file = APP_PATH + SAVE_DIRECTORY + "Shared";
            else
                file = APP_PATH + SAVE_DIRECTORY + YEAR_INDEX;
            if (!Directory.Exists(file))
            {
                Directory.CreateDirectory(file);
            }
            file += "/" + table.Name.ToLower() + EXTENSION;
            if (!File.Exists(file))
            {
                File.Create(file).Close();
            }
            DataType[] typeSeq = TableDataTypes[table];
            StreamReader stream = new StreamReader(file);
            string[] buffer;

            List<List<string>> datas = TableData[table];
            datas.Clear();

            while (stream.Peek() != -1)
            {
                string line = Simple3Des.Decrypt(stream.ReadLine(), true);
                buffer = line.Split('|');
                List<string> content = new List<string>();
                for (int i = 0; i < buffer.Length; i++)
                {
                    if (typeSeq[i] != DataType.ReadOnly)
                        content.Add(buffer[i]);
                    else
                        content.Add("");
                }
                datas.Add(content);
                if (table == JBTablePembelian)
                    UpdateTablePembelian(datas.Count - 1, JB_TABLE_PEMBELIAN_GROUP_2[0], false);
                else if (table == JBTablePenjualan)
                    UpdateTablePenjualan(datas.Count - 1, JB_TABLE_PENJUALAN_GROUP_2[0], false);
                //else if (table == HPTableHutang)
                //    UpdateTableHutang(datas.Count - 1, HP_TABLE_HUTANG_GROUP_1[0], false);
                //else if (table == HPTablePiutang)
                //    UpdateTablePiutang(datas.Count - 1, HP_TABLE_PIUTANG_GROUP_1[0], false);
            }
            stream.Close();

            if (Recovering)
            {
                string temp = APP_PATH + SAVE_DIRECTORY + "temp/" + table.Name.ToLower();
                if (Directory.Exists(temp))
                {
                    foreach (FileInfo f in new DirectoryInfo(temp).GetFiles())
                    {
                        string name = Path.GetFileNameWithoutExtension(f.Name);
                        int start = name.LastIndexOf('-') + 1;
                        int row = Convert.ToInt32(name.Substring(start));

                        stream = new StreamReader(temp + "/" + name + EXTENSION);
                        string line = Simple3Des.Decrypt(stream.ReadLine(), true);
                        stream.Close();
                        buffer = line.Split('|');
                        for (int i = 0; i < buffer.Length; i++)
                        {
                            SetTableContent(table, datas, row, i, buffer[i]);
                        }
                        if (table == JBTablePembelian)
                            UpdateTablePembelian(row, JB_TABLE_PEMBELIAN_GROUP_2[0], false);
                        else if (table == JBTablePenjualan)
                            UpdateTablePenjualan(row, JB_TABLE_PENJUALAN_GROUP_2[0], false);
                        //else if (table == HPTableHutang)
                        //    UpdateTableHutang(row, HP_TABLE_HUTANG_GROUP_1[0], false);
                        //else if (table == HPTablePiutang)
                        //    UpdateTablePiutang(row, HP_TABLE_PIUTANG_GROUP_1[0], false);
                    }
                }
            }

            RefreshTable(table, false);
            if (table == JBTablePembelian || table == JBTablePenjualan)
                RefreshPanelJualBeliFooter();
        }

        void SaveTableData(ListView table)
        {
            string file;
            string temp = APP_PATH + SAVE_DIRECTORY + "temp/" + table.Name.ToLower();
            if (table == PPTablePembeli || table == PPTablePetani)
                file = APP_PATH + SAVE_DIRECTORY + "Shared/" + table.Name.ToLower() + EXTENSION;
            else
                file = APP_PATH + SAVE_DIRECTORY + YEAR_INDEX + "/" + table.Name.ToLower() + EXTENSION;
            if (!File.Exists(file))
            {
                File.Create(file).Close();
            }
            StreamWriter writer = new StreamWriter(file);
            DataType[] typeSeq = TableDataTypes[table];
            writer.Write(""); // Clear file

            List<List<string>> datas = TableData[table];
            for (int i = 0; i < datas.Count - 1; i++)
            {
                string line = "";
                string entry = temp + "/entry-" + i + EXTENSION;
                if (File.Exists(entry))
                {
                    StreamReader stream = new StreamReader(entry);
                    while (stream.Peek() != -1)
                    {
                        line = stream.ReadLine();
                    }
                    stream.Close();
                    File.Delete(entry);
                }
                else
                {
                    for (int j = 0; j < datas[i].Count; j++)
                    {
                        if (typeSeq[j] != DataType.ReadOnly)
                            line += GetTableContent(datas, i, j);
                        if (j < datas[i].Count - 1)
                            line += "|";
                    }
                    line = Simple3Des.Encrypt(line, true);
                }
                writer.WriteLine(line);
            }
            writer.Close();
            if (Directory.Exists(temp))
            {
                foreach (FileInfo f in new DirectoryInfo(temp).GetFiles())
                {
                    f.Delete();
                }
                Directory.Delete(temp);
            }
        }

        string ConvertToNoNota(int dex)
        {
            int charDex = dex / 1000;
            char prefix = NOTA_PREFIXES[charDex];
            string suffix = dex - (charDex * 1000) + "";
            return charDex > 0 ? prefix + suffix.PadLeft(4, '0') : suffix.PadLeft(4, '0');
        }

        void DeleteFromTable(ListView table, int row)
        {
            DataType[] typeSeq = TableDataTypes[table];
            List<List<string>> datas = TableData[table];
            for (int i = 0; row < datas.Count && i < datas[row].Count && i < table.Columns.Count; i++)
            {
                if (typeSeq[i] != DataType.ReadOnly && typeSeq[i] != DataType.SavedReadOnly)
                {
                    SetTableContent(table, datas, row, i, "");
                    UpdateTable(table, row, i, false);
                }
            }
            RefreshTable(table, false);
            if (table == JBTablePembelian || table == JBTablePenjualan)
                RefreshPanelJualBeliFooter();
        }

        void InsertEmptyToTable(ListView table, bool save)
        {
            List<List<string>> datas = TableData[table];
            string index = table == JBTablePembelian ? ConvertToNoNota(datas.Count + 1) : table == JBTablePenjualan ? "" : (datas.Count + 1 + "");
            SetTableContent(table, datas, datas.Count, 0, index + "");
            RefreshTable(table, false);

            VScrollBar scroll = null;
            if (table == JBTablePembelian)
            {
                scroll = JBScroll;
                RefreshPanelJualBeliFooter();
                JBScroll.Maximum = Math.Max(datas.Count - (JBPanelContent.Height / TableRowHeight), JBScroll.Minimum);
                JBScroll.Visible = JBScroll.Maximum > JBScroll.Minimum;
                InsertEmptyToTable(JBTablePenjualan, save);
            }
            else
            {
                scroll = ControlScroller[table];
                scroll.Maximum = Math.Max(Math.Max(scroll.Maximum, datas.Count - (table.Parent.Height / TableRowHeight)), scroll.Minimum);
                scroll.Visible = scroll.Maximum > scroll.Minimum;
            }
        }

        void GetTableSubItemAt(ListView table, Point p, out int row, out int col)
        {
            int py = p.Y;
            //if (py < 0)
            //    py = 32767 + (32767 + py);
            int y = py + table.Top - table.Location.Y;
            row = y / TableRowHeight;
            col = -1;
            if (table.Items.Count > 0)
            {
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    if (p.X < table.Items[0].SubItems[i].Bounds.Left)
                    {
                        break;
                    }
                    col++;
                }
            }
        }

        void RefreshTable(ListView table, bool includeall, int startIndex)
        {
            if (TableData.ContainsKey(table) && ControlScroller.ContainsKey(table))
            {
                VScrollBar scroll = ControlScroller[table];
                table.Items.Clear();
                List<List<string>> datas = GetDataSource(table);
                for (int i = (includeall ? 0 : startIndex); i < (includeall ? datas.Count : Math.Min(startIndex + table.Height / TableRowHeight, datas.Count)); i++)
                {
                    ListViewItem itm = new ListViewItem(GetTableContent(datas, i, 0));
                    for (int j = 1; j < table.Columns.Count; j++)
                        itm.SubItems.Add(GetTableContent(datas, i, j));
                    table.Items.Add(itm);
                }
            }
        }

        void RefreshTable(ListView table, bool includeall)
        {
            if (TableData.ContainsKey(table) && ControlScroller.ContainsKey(table))
            {
                VScrollBar scroll = ControlScroller[table];
                table.Items.Clear();
                List<List<string>> datas = GetDataSource(table);
                for (int i = (includeall ? 0 : scroll.Value); i < (includeall ? datas.Count : Math.Min(scroll.Value + table.Height / TableRowHeight, datas.Count)); i++)
                {
                    ListViewItem itm = new ListViewItem(GetTableContent(datas, i, 0));
                    for (int j = 1; j < table.Columns.Count; j++)
                        itm.SubItems.Add(GetTableContent(datas, i, j));
                    table.Items.Add(itm);
                }
            }
        }

        void ScrollPanel(Panel panel, VScrollBar scroll)
        {
            foreach(Control c in panel.Controls)
            {
                if (c is ListView)
                {
                    var table = c as ListView;
                    RefreshTable(table, false);
                    
                    if (table == SelectedTable)
                    {
                        foreach (ListViewItem itm in table.Items)
                        {
                            itm.Selected = SelectedIndexes.Contains(itm.Index + scroll.Value);
                        }
                    }
                }
            }

            foreach (Timer t in WarningMessages.Keys)
            {
                t.Interval = 1;
            }
        }

        void RefreshPanelHutangFooter()
        {
            HPLabelJumlahHutang.Text = ToCurrency(HPFHutang + "");
            HPLabelJumlahPiutang.Text = ToCurrency(HPFPiutang + "");
            HPLabelJumlahSisaHutang.Text = ToCurrency(HPFSisaHutang + "");
            HPLabelJumlahSisaPiutang.Text = ToCurrency(HPFSisaPiutang + "");
        }

        void RefreshPanelJualBeliFooter()
        {
            JBLabelJumlahBeli.Text = ToCurrency(JBFJumlah + "");
            JBLabelJumlah.Text = "Jumlah pembelian: " + JBFJumlahCount + " keranjang";
            JBLabelJumlahNettoBeli.Text = JBFNettoBeli + "";

            JBLabelJumlahNettoJual.Text = JBFNettoJual + "";
            JBLabelJumlahJual.Text = ToCurrency(JBFJumlahJual + "");
            JBLabelTerjual.Text = "Terjual: " + JBFJumlahJualCount + " keranjang";
            JBLabelSisa.Text = "Sisa (belum terjual): " + (JBFJumlahCount - JBFJumlahJualCount) + " keranjang";
            JBLabelLabaBersih.Text = "Laba bersih: Rp " + ToCurrency(JBFLabaBersih + "");
            JBLabelLaba.Text = "Laba: " + JBFLabaCount + " keranjang (Rp " + ToCurrency((JBFLaba) + "") + ")";
            JBLabelRugi.Text = "Rugi: " + JBFRugiCount + " keranjang (Rp " + ToCurrency((JBFRugi) + "") + ")";

            // Update positions
            JBLabelJumlah.Location = new Point(JBUnderlineTableBeli.Left, JBUnderlineTableBeli.Bottom + GENERAL_SPACING);
            JBLabelTerjual.Location = new Point(JBUnderlineTableBeli.Left, JBLabelJumlah.Bottom);
            JBLabelSisa.Location = new Point(Math.Max(JBLabelTerjual.Right, JBLabelJumlah.Right), JBLabelJumlah.Bottom);
            JBLabelLabaBersih.Location = new Point(JBUnderlineTableJual.Left, JBUnderlineTableJual.Bottom + GENERAL_SPACING);
            JBLabelLaba.Location = new Point(JBUnderlineTableJual.Left, JBLabelLabaBersih.Bottom);
            JBLabelRugi.Location = new Point(JBLabelLaba.Right, JBLabelLabaBersih.Bottom);
        }

        void AutoSizeTableColumnWidths(ListView header, ListView content, bool includeHeader)
        {
            List<List<string>> datas = GetDataSource(content);
            for (int i = 0; i < header.Columns.Count; i++)
            {
                int max = 0;
                if (includeHeader)
                {
                    Size txtSize = TextRenderer.MeasureText(header.Columns[i].Text + "", header.Font);
                    max = txtSize.Width + 5;
                }
                else
                {
                    Size txtSize = TextRenderer.MeasureText(header.Columns[i].Text[0] + "", header.Font);
                    max = txtSize.Width + 5;
                }
                for (int j = 0; j < datas.Count; j++)
                {
                    Size txtSize = TextRenderer.MeasureText(GetTableContent(datas, j, i) + "", header.Font);
                    max = Math.Max(max, txtSize.Width + 5);
                }
                header.Columns[i].Width = content.Columns[i].Width = max;
            }
            DataType[] typeSeq = TableDataTypes[content];
            float maxWidth = header.Items[0].Bounds.Right;
            float tableWidth = header.Width + 2;
            float bonusWidth = (typeSeq[0] == DataType.ReadOnly || typeSeq[0] == DataType.SavedReadOnly ? (header.Columns[0].Width / maxWidth * tableWidth) - header.Columns[0].Width : 0);
            if (maxWidth > tableWidth)
                AutoSizeTableColumnWidths(header, content, false);
            else
            {
                for (int i = 0; i < header.Columns.Count; i++)
                {
                    if ((typeSeq[i] != DataType.ReadOnly && typeSeq[i] != DataType.SavedReadOnly) || i > 0)
                    {
                        header.Columns[i].Width = content.Columns[i].Width = (int)((header.Columns[i].Width / maxWidth) * (tableWidth + bonusWidth));
                    }
                }
            }
        }

        void HandleApplicationExit(bool save = true)
        {
            if (save)
            {
                foreach (ListView table in Tables.Values)
                {
                    SaveTableData(table);
                }
            }
            string temp = APP_PATH + SAVE_DIRECTORY + "temp";
            if (Directory.Exists(temp))
            {
                foreach (DirectoryInfo d in new DirectoryInfo(temp).GetDirectories())
                {
                    foreach (FileInfo f in d.GetFiles())
                    {
                        f.Delete();
                    }
                    d.Delete();
                }
                Directory.Delete(temp);
            }
        }
        #endregion


        /***********************************
         * Utils
         * 
         * Reused code blocks
         * 
         ***********************************/
        #region

        void print(string str)
        {
            MessageBox.Show(str);
        }

        void PrintWarning(string msg, Control parent, Point pos, Size minSize, Color backCOlor, int duration, bool hoverclose)
        {
            Label m = new Label();
            m.Text = msg;
            m.AutoSize = true;
            m.Parent = parent;
            m.Location = parent.PointToClient(pos);
            m.TextAlign = ContentAlignment.MiddleCenter;
            m.MaximumSize = new Size(parent.Right - m.Location.X, 1000);
            m.MinimumSize = minSize;
            m.Font = new Font(m.Font.FontFamily, 10f);
            m.BorderStyle = BorderStyle.FixedSingle;
            m.ForeColor = Color.Red;
            m.BackColor = backCOlor;
            m.Show();
            m.BringToFront();
            if (hoverclose)
                m.MouseMove += new MouseEventHandler(MessageMouseEnter);
            //m.Top = Math.Min(m.Top, parent.Bottom - m.Height);
            Timer t = new Timer();
            if (duration > 0)
                t.Interval = duration;
            else
                t.Interval = msg.Length * 100;
            t.Enabled = true;
            t.Tick += new EventHandler(WarningTimerExpired);
            WarningMessages.Add(t, m);
        }

        void PrintPanel(Panel panel, Panel header, Panel footer)
        {
            PrintTargetPanel = panel;
            PrintOriginalPanelSize = panel.Size;
            PrintTotalHeight = 0;
            int RowPerPage = (PRINT_PAPER_SIZE.Height - header.Height - PRINT_TOP_MARGIN - PRINT_BOTTOM_MARGIN) / TableRowHeight;
            int PrintArea = RowPerPage * TableRowHeight;
            VScrollBar scroll = null;
            List<ListView> tables = new List<ListView>();
            foreach (Control c in panel.Controls)
            {
                if (c is VScrollBar)
                {
                    scroll = c as VScrollBar;
                }
                else if (c is ListView)
                {
                    var table = c as ListView;
                    if (!tables.Contains(table))
                    {
                        List<List<string>> datas = GetDataSource(table);
                        if (!Searching)
                            datas.RemoveAt(datas.Count - 1);
                        PrintTotalHeight = Math.Max(TableRowHeight * datas.Count, PrintTotalHeight);
                        table.Height = PrintArea;
                        tables.Add(table);
                    }
                }
            }
            bool scrollVisible = scroll.Visible;
            int prevValue = scroll.Value;
            panel.Height = PrintArea;
            scroll.Value = 0;
            scroll.Value = scroll.Maximum;
            scroll.Value = 0;
            scroll.Hide();
            panel.Focus();
            Refresh();

            // Prepare printing
            PrintExtraPage = false;
            PrintPageIndex = 0;
            PrintPanelImage = new List<Bitmap>();
            int PageTotal = (PrintTotalHeight / PrintArea);
            for (int i = 0; i <= PageTotal; i++)
            {
                int ct = 0;
                foreach (ListView table in tables)
                {
                    RefreshTable(table, false, RowPerPage * i);
                    if (table.Items.Count > ct)
                        ct = table.Items.Count;

                    if (ct < RowPerPage)
                    {
                        if (ct == 0)
                            table.Height = panel.Height = PrintTotalHeight = 1;
                        else
                            table.Height = panel.Height = ct * TableRowHeight;
                    }
                }
                var img = new Bitmap(panel.Width, panel.Height);
                panel.DrawToBitmap(img, new Rectangle(0, 0, panel.Width, panel.Height));
                PrintPanelImage.Add(img);
            }
            //PrintPanelImage = new Bitmap(PrintPanelImage, new Size(PRINT_PAPER_SIZE.Width, PRINT_PAPER_SIZE.Height));
            //PrintPanelImage = ConvertToBlackWhite(PrintPanelImage);
            PrintHeaderImage = new Bitmap(header.Width, header.Height);
            header.DrawToBitmap(PrintHeaderImage, new Rectangle(0, 0, header.Width, header.Height));
            //PrintHeaderImage = new Bitmap(PrintHeaderImage, new Size(PRINT_PAPER_SIZE.Width, PrintHeaderImage.Height));
            //PrintHeaderImage = ConvertToBlackWhite(PrintHeaderImage);
            if (footer != null)
            {
                PrintFooterImage = new Bitmap(footer.Width, footer.Height);
                footer.DrawToBitmap(PrintFooterImage, new Rectangle(0, 0, footer.Width, footer.Height));
                //PrintFooterImage = new Bitmap(PrintFooterImage, new Size(PRINT_PAPER_SIZE.Width, PrintFooterImage.Height));
                //PrintFooterImage = ConvertToBlackWhite(PrintFooterImage);
            }
            else
                PrintFooterImage = null;

            // Restore form
            panel.Size = PrintOriginalPanelSize;
            if (scrollVisible)
                scroll.Show();
            ListView focus = null;
            foreach (ListView table in tables)
            {
                focus = table;
                if (table != JBTablePenjualan)
                {
                    if (!Searching)
                        InsertEmptyToTable(table, false);
                }
                table.Height = PrintOriginalPanelSize.Height;
                RefreshTable(table, false);
            }
            scroll.Value = 0;
            scroll.Value = scroll.Maximum;
            scroll.Value = prevValue;
            tables.Clear();
            
            PrintDialog.ShowDialog();
            //PrintDocument.Print();
            focus.Focus();
        }

        int PotongBerat(int bruto)
        {
            return bruto - Math.Max(Math.Min(7 + ((bruto - 31) / 5), 15), 7);
        }

        string ToCurrency(string s)
        {
            string total = "";
            for (int i = 0; i < s.Length; i++)
            {
                if (((i % 3) == 0) && (i > 0) && (i < s.Length - 1 || s.Substring(0, 1) != "-"))
                {
                    total = "." + total;
                }
                total = s.Substring(s.Length - i - 1, 1) + total;
            }
            return total;
        }


        Bitmap ResizeBitmap(Bitmap bmp, int width, int height)
        {
            Bitmap temp = new Bitmap(width, height);
            Graphics bmpGraph = Graphics.FromImage(temp);
            float scaleFactorX = (float)width / (float)bmp.Width;
            float scaleFactorY = (float)height / (float)bmp.Height;
            bmpGraph.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            bmpGraph.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            bmpGraph.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
            bmpGraph.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            bmpGraph.ScaleTransform(scaleFactorX, scaleFactorY);
            bmpGraph.DrawImage(bmp, 0, 0);
            return temp;
        }

        Bitmap CropBitmap(Bitmap src, int startX, int startY, int width, int height)
        {
            Rectangle cropRect = new Rectangle(startX, startY, width, height);
            Bitmap tmp = new Bitmap(width, height);

            using (Graphics g = Graphics.FromImage(tmp))
            {
                g.DrawImage(src, new Rectangle(0, 0, tmp.Width, tmp.Height),
                                 cropRect,
                                 GraphicsUnit.Pixel);
            }
            return tmp;
        }

        Bitmap ConvertToBlackWhite(Bitmap src)
        {
            Bitmap tmp = new Bitmap(src.Width, src.Height);
            using (Graphics g = Graphics.FromImage(tmp))
            {
                g.DrawImage(src, new Rectangle(0, 0, src.Width, src.Height), 0, 0, src.Width, src.Height, GraphicsUnit.Pixel, IMAGE_ATTRIBUTE);
            }
            return tmp;
        }

        [DllImport("kernel32", CharSet = CharSet.Unicode)]
        static extern long WritePrivateProfileString(string Section, string Key, string Value, string FilePath);

        [DllImport("kernel32", CharSet = CharSet.Unicode)]
        static extern int GetPrivateProfileString(string Section, string Key, string Default, StringBuilder RetVal, int Size, string FilePath);

        #endregion

        /***********************************
         * Event Responses
         * 
         ***********************************/
        #region
        void ScrollValueChanged(Object sender, EventArgs e)
        {
            HideEditor(true);
            var scroll = sender as VScrollBar;
            ScrollPanel(ScrollParentPanel[scroll], scroll);
            LabelScroll.Hide();
        }

        void ScrollScrolled(Object sender, ScrollEventArgs e)
        {
            HideEditor(true);
            var scroll = sender as VScrollBar;
            if (e.Type == ScrollEventType.LargeIncrement || e.Type == ScrollEventType.LargeDecrement)
            {
                int y = scroll.PointToClient(Cursor.Position).Y - 17;
                List<List<string>> datas = null;
                if (scroll == JBScroll)
                    datas = GetDataSource(JBTablePembelian);
                else if (scroll == HPScroll)
                {
                    if (GetDataSource(HPTableHutang).Count > GetDataSource(HPTablePiutang).Count)
                        datas = GetDataSource(HPTableHutang);
                    else
                        datas = GetDataSource(HPTablePiutang);
                }
                else if (scroll == PPScroll)
                {
                    if (GetDataSource(PPTablePembeli).Count > GetDataSource(PPTablePetani).Count)
                        datas = GetDataSource(PPTablePembeli);
                    else
                        datas = GetDataSource(PPTablePetani);
                }
                int row = (int)((float)datas.Count * ((float)y / ((float)scroll.Height - 34f)));
                e.NewValue = Math.Min(row, scroll.Maximum);
            }
            ScrollPanel(ScrollParentPanel[scroll], scroll);
        }

        void OnScrollHover(Object sender, MouseEventArgs a)
        {
            var scroll = sender as VScrollBar;
            if (a.Y > 17 && a.Y < scroll.Height - 17)
            {
                int y = a.Y - 17;
                List<List<string>> datas = null;
                if (scroll == JBScroll)
                    datas = GetDataSource(JBTablePembelian);
                else if (scroll == HPScroll)
                {
                    if (GetDataSource(HPTableHutang).Count > GetDataSource(HPTablePiutang).Count)
                        datas = GetDataSource(HPTableHutang);
                    else
                        datas = GetDataSource(HPTablePiutang);
                }
                else if (scroll == PPScroll)
                {
                    if (GetDataSource(PPTablePembeli).Count > GetDataSource(PPTablePetani).Count)
                        datas = GetDataSource(PPTablePembeli);
                    else
                        datas = GetDataSource(PPTablePetani);
                }

                int row = (int)((float)datas.Count * ((float)y / ((float)scroll.Height - 34f)));
                LabelScroll.Text = datas[row][0];
                LabelScroll.Location = scroll.Parent.PointToScreen(new Point(scroll.Left - LabelScroll.Width - GENERAL_SPACING, a.Y));
                LabelScroll.Show();
                LabelScroll.BringToFront();
            }
            else
                LabelScroll.Hide();
        }

        void ScrollMouseLeave(object sender, EventArgs a)
        {
            LabelScroll.Hide();
        }

        void OnScrollClick(Object sender, MouseEventArgs a)
        {
            if (a.Y > 17 && a.Y < JBScroll.Height - 17)
            {
                int y = a.Y - 17;
                int row = (int)((float)JBTablePembelian.Items.Count * ((float)y / ((float)JBScroll.Height - 34)));
                JBScroll.Value = (int)((float)JBScroll.Maximum * ((float)row / (float)JBTablePembelian.Items.Count));
            }
        }

        void OnControlScroll(Object sender, MouseEventArgs a)
        {
            var c = sender as Control;
            //print(c.Name);
            VScrollBar bar = ControlScroller[c];
            if (a.Delta > 0)
            {
                bar.Value = Math.Max(bar.Value - 1, bar.Minimum);
            }
            else
            {
                bar.Value = Math.Min(bar.Value + 1, bar.Maximum);
            }
        }

        void TabButtonPressed(Object sender, EventArgs e)
        {
            Button obj = (Button)sender;
            Control c = Tabs[obj];
            if (obj != CurrentTab)
            {
                c = SetActiveTab(obj);
            }
            c.Focus();
        }

        void ColumnSizeChanging(Object sender, ColumnWidthChangingEventArgs e)
        {
            ListView obj = (ListView)sender;
            if (obj != null)
            {
                //if (obj.Items[0].Bounds.Right > obj.Width)
                //{
                //    e.NewWidth = Tables[obj].Columns[e.ColumnIndex].Width - (obj.Items[0].Bounds.Right - obj.Width);
                //    e.Cancel = true;
                //}
                Tables[obj].Columns[e.ColumnIndex].Width = e.NewWidth;
                if (CurrentEditor != null)
                    HideEditor(true);

                if (obj == JBHeaderPembelian)
                {
                    JBLabelJumlahNettoBeli.Location = new Point(JBHeaderPembelian.TopItem.SubItems[7].Bounds.Left + JBHeaderPembelian.Left + 2, 0);
                    JBLabelJumlahNettoBeli.MinimumSize = JBHeaderPembelian.TopItem.SubItems[7].Bounds.Size;
                    JBLabelJumlahNettoBeli.MaximumSize = new Size(JBHeaderPembelian.TopItem.SubItems[7].Bounds.Width, JBHeaderPembelian.TopItem.SubItems[7].Bounds.Height * 2);
                    JBLabelJumlahBeli.Location = new Point(JBHeaderPembelian.TopItem.SubItems[11].Bounds.Left + JBHeaderPembelian.Left + 2, 0);
                    JBLabelJumlahBeli.MinimumSize = JBHeaderPembelian.TopItem.SubItems[11].Bounds.Size;
                    JBLabelJumlahBeli.MaximumSize = new Size(JBHeaderPembelian.TopItem.SubItems[11].Bounds.Width, JBHeaderPembelian.TopItem.SubItems[11].Bounds.Height * 2);
                }
                else if (obj == JBHeaderPenjualan)
                {
                    JBLabelJumlahNettoJual.Location = new Point(JBHeaderPenjualan.TopItem.SubItems[4].Bounds.Left + JBHeaderPenjualan.Left + 2, 0);
                    JBLabelJumlahNettoJual.MinimumSize = JBHeaderPenjualan.TopItem.SubItems[4].Bounds.Size;
                    JBLabelJumlahNettoJual.MaximumSize = new Size(JBHeaderPenjualan.TopItem.SubItems[4].Bounds.Width, JBHeaderPenjualan.TopItem.SubItems[4].Bounds.Height * 2);
                    JBLabelJumlahJual.Location = new Point(JBHeaderPenjualan.TopItem.SubItems[6].Bounds.Left + JBHeaderPenjualan.Left + 2, 0);
                    JBLabelJumlahJual.MinimumSize = JBHeaderPenjualan.TopItem.SubItems[6].Bounds.Size;
                    JBLabelJumlahJual.MaximumSize = new Size(JBHeaderPenjualan.TopItem.SubItems[6].Bounds.Width, JBHeaderPenjualan.TopItem.SubItems[6].Bounds.Height * 2);
                }
                else if (obj == HPHeaderHutang)
                {
                    HPLabelJumlahHutang.Location = new Point(HPHeaderHutang.TopItem.SubItems[4].Bounds.Left + HPHeaderHutang.Left + 2, 0);
                    HPLabelJumlahHutang.MinimumSize = HPHeaderHutang.TopItem.SubItems[4].Bounds.Size;
                    HPLabelJumlahHutang.MaximumSize = new Size(HPHeaderHutang.TopItem.SubItems[4].Bounds.Width, HPHeaderHutang.TopItem.SubItems[4].Bounds.Height * 2);
                    HPLabelJumlahSisaHutang.Location = new Point(HPHeaderHutang.TopItem.SubItems[5].Bounds.Left + HPHeaderHutang.Left + 2, 0);
                    HPLabelJumlahSisaHutang.MinimumSize = HPHeaderHutang.TopItem.SubItems[5].Bounds.Size;
                    HPLabelJumlahSisaHutang.MaximumSize = new Size(HPHeaderHutang.TopItem.SubItems[5].Bounds.Width, HPHeaderHutang.TopItem.SubItems[5].Bounds.Height * 2);
                }
                else if (obj == HPHeaderPiutang)
                {
                    HPLabelJumlahPiutang.Location = new Point(HPHeaderPiutang.TopItem.SubItems[4].Bounds.Left + HPHeaderPiutang.Left + 2, 0);
                    HPLabelJumlahPiutang.MinimumSize = HPHeaderPiutang.TopItem.SubItems[4].Bounds.Size;
                    HPLabelJumlahPiutang.MaximumSize = new Size(HPHeaderPiutang.TopItem.SubItems[4].Bounds.Width, HPHeaderPiutang.TopItem.SubItems[4].Bounds.Height * 2);
                    HPLabelJumlahSisaPiutang.Location = new Point(HPHeaderPiutang.TopItem.SubItems[6].Bounds.Left + HPHeaderPiutang.Left + 2, 0);
                    HPLabelJumlahSisaPiutang.MinimumSize = HPHeaderPiutang.TopItem.SubItems[6].Bounds.Size;
                    HPLabelJumlahSisaPiutang.MaximumSize = new Size(HPHeaderPiutang.TopItem.SubItems[6].Bounds.Width, HPHeaderPiutang.TopItem.SubItems[6].Bounds.Height * 2);
                }
            }
        }

        void OnBlockerClick(Object sender, MouseEventArgs a)
        {
            HideEditor(true);
        }

        void OnTableHover(Object sender, MouseEventArgs a)
        {
            ListView obj = (ListView)sender;
            if (obj != null)
            {
                int rowDex, colDex;
                GetTableSubItemAt(obj, a.Location, out rowDex, out colDex);
                if (PrevHoveredItem != obj.Items[rowDex].SubItems[colDex])
                {
                    if (PrevHoveredItem != null)
                    {
                        PrevHoveredItem.BackColor = Color.White;
                    }
                    PrevHoveredItem = obj.Items[rowDex].SubItems[colDex];
                    PrevHoveredItem.BackColor = TABLE_HOVER_COLOR_OTHER;
                }
            }
        }

        void OnTableClick(Object sender, MouseEventArgs a)
        {
            if (a.Button == MouseButtons.Left)
            {
                ListView obj = (ListView)sender;
                if (obj != null)
                {
                    int rowDex, colDex;
                    GetTableSubItemAt(obj, a.Location, out rowDex, out colDex);
                    if (rowDex >= 0 && rowDex < obj.Items.Count)
                    {
                        if (colDex >= 0 && colDex < obj.Columns.Count)
                        {
                            ShowEditor(obj, colDex, rowDex, false);
                        }
                    }
                }
            }
        }
        
        void DecimalInputOnly(Object sender, KeyPressEventArgs e)
        {
            e.Handled = !Char.IsNumber(e.KeyChar) && !Char.IsControl(e.KeyChar) && e.KeyChar != '.' && e.KeyChar != ',';
        }

        void NumberInputOnly(Object sender, KeyPressEventArgs e)
        {
            e.Handled = !Char.IsNumber(e.KeyChar) && !Char.IsControl(e.KeyChar);
        }

        void FilterBoxHandler(Object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
            var box = (sender as TextBox);
            if (Regex.IsMatch(e.KeyChar + "", "[A-Za-z]"))
            {
                box.Text = (e.KeyChar + "").ToUpper();
                SendKeys.Send("{TAB}");
            }
            else
                box.Text = "";
            box.SelectionStart = 1;
        }

        void OnTableKeypress(Object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.A && e.Control)
            {
                var table = sender as ListView;
                if (table != null)
                {
                    foreach (ListViewItem itm in table.Items)
                    {
                        itm.Selected = true;
                    }
                }
            }
            else if (e.KeyCode == Keys.Return)
            {
                var table = sender as ListView;
                if (table != null && table.SelectedIndices.Count > 0)
                {
                    DataType[] typeSeq = TableDataTypes[table];
                    int row = table.SelectedIndices[0], col = 0;
                    for (int i = 0; i < typeSeq.Length; i++)
                    {
                        if (typeSeq[i] == DataType.ReadOnly || typeSeq[i] == DataType.SavedReadOnly)
                        {

                        }
                        else
                        {
                            col = i;
                            break;
                        }
                    }
                    ShowEditor(table, col, row, false);
                }
            }
            else if (e.KeyCode == Keys.Delete && !Searching)
            {
                var table = sender as ListView;
                if (table != null)
                {
                    DataType[] typeSeq = TableDataTypes[table];
                    VScrollBar scroll = ControlScroller[table];
                    foreach (int dex in table.SelectedIndices)
                    {
                        DeleteFromTable(table, dex + scroll.Value);
                        if (table == JBTablePembelian)
                            DeleteFromTable(JBTablePenjualan, dex + scroll.Value);
                    }
                    foreach (int dex in SelectedIndexes)
                    {
                        int row = dex - scroll.Value;
                        if (row < 0 || row >= table.Items.Count - 1 || (row >= 0 && row < table.Items.Count && table.Items[row].Selected))
                        {
                            DeleteFromTable(table, dex);
                            if (table == JBTablePembelian)
                                DeleteFromTable(JBTablePenjualan, dex);
                        }
                    }
                    SelectedIndexes.Clear();

                    // Remove unused data
                    if (table != JBTablePenjualan)
                    {
                        List<int> ToRemove = new List<int>();
                        List<List<string>> datas = TableData[table];
                        for (int i = datas.Count - 1; i >= 0; i--)
                        {
                            bool filled = false;
                            for (int j = 0; j < datas[i].Count; j++)
                            {
                                if (typeSeq[j] != DataType.ReadOnly && typeSeq[j] != DataType.SavedReadOnly && GetTableContent(datas, i, j).Length > 0)
                                {
                                    filled = true;
                                    break;
                                }
                            }
                            if (filled)
                                break;
                            else
                                ToRemove.Add(i);
                        }

                        foreach (int itm in ToRemove)
                        {
                            datas.RemoveAt(itm);
                            if (table == JBTablePembelian)
                            {
                                TableData[JBTablePenjualan].RemoveAt(itm);
                            }
                        }

                        InsertEmptyToTable(table, true);

                        RefreshTable(table, false);
                        if (table == JBTablePembelian)
                        {
                            RefreshTable(JBTablePenjualan, false);
                        }
                    }
                    
                }
            }
        }

        void OnEditorKeypress(Object sender, KeyEventArgs e)
        {
            if (Array.Exists(ARROW_KEYS, k => k == e.KeyCode) && ModifierKeys == Keys.Shift)
            {
                List<List<string>> datas = TableData[CurrentTable];
                DataType[] typeSeq = TableDataTypes[CurrentTable];
                VScrollBar scroll = ControlScroller[CurrentTable];
                int col = TargetCol + (e.KeyCode == Keys.Left ? -1 : e.KeyCode == Keys.Right ? 1 : 0), row = TargetRow + (e.KeyCode == Keys.Up ? -1 : e.KeyCode == Keys.Down ? 1 : 0);
                bool found = e.KeyCode == Keys.Up || e.KeyCode == Keys.Down;
                if (e.KeyCode == Keys.Left || e.KeyCode == Keys.Right)
                {
                    bool shifting = e.KeyCode == Keys.Left;
                    for (int j = TargetCol; shifting ? j >= 0 : j < CurrentTable.Columns.Count; j += shifting ? -1 : 1)
                    {
                        if (j != TargetCol && typeSeq[j] != DataType.ReadOnly && typeSeq[j] != DataType.SavedReadOnly)
                        {
                            found = true;
                            col = j;
                            break;
                        }
                    }
                }
                if (e.KeyCode == Keys.Down && CurrentTable != JBTablePenjualan && row + scroll.Value == datas.Count - 1)
                {
                    if (CurrentTable == JBTablePembelian && row >= 0 && row + scroll.Value == datas.Count - 1)
                    {
                        for (int j = 1; j < 5; j++)
                        {
                            SetTableContent(CurrentTable, datas, row + scroll.Value, j, GetTableContent(datas, row + scroll.Value - 1, j));
                        }
                    }
                    InsertEmptyToTable(CurrentTable, true);
                }
                if (found)
                {
                    HideEditor(true);
                    if (row < 0)
                    {
                        row = 0;
                        scroll.Value = Math.Max(scroll.Value - 1, scroll.Minimum);
                    }
                    else if (row > CurrentTable.Items.Count - 1)
                    {
                        row = CurrentTable.Items.Count - 1;
                        scroll.Value = Math.Min(scroll.Value + 1, scroll.Maximum);
                    }
                    ShowEditor(CurrentTable, col, row, false);
                }
                e.Handled = true;
            }
        }

        void OnConfirm(Object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13) // Enter
            {
                e.Handled = true;
                HideEditor(true);
                DataType[] typeSeq = TableDataTypes[CurrentTable];
                List<List<string>> datas = TableData[CurrentTable];
                int col = TargetCol, row = TargetRow;
                bool shifting = ModifierKeys == Keys.Shift;
                for (int i = TargetCol; shifting ? i >= 0 : i < CurrentTable.Columns.Count; i += shifting ? -1 : 1)
                {
                    if (i != TargetCol && typeSeq[i] != DataType.ReadOnly && typeSeq[i] != DataType.SavedReadOnly)
                    {
                        col = i;
                        break;
                    }
                    // If at the end of table move to next row
                    if (shifting ? i < 1 : i >= CurrentTable.Columns.Count - 1)
                    {
                        row += (shifting ? -1 : 1);
                        for (int j = shifting ? CurrentTable.Columns.Count - 1 : 0; shifting ? j > 0 : j < CurrentTable.Columns.Count; j += shifting ? -1 : 1)
                        {
                            if (typeSeq[j] != DataType.ReadOnly && typeSeq[j] != DataType.SavedReadOnly && (j > 4 || CurrentTable != JBTablePembelian))
                            {
                                col = j;
                                break;
                            }
                        }
                        if (CurrentTable == JBTablePembelian && row > 0 && row == datas.Count - 1)
                        {
                            for (int j = 1; j < 5; j++)
                            {
                                SetTableContent(CurrentTable, datas, row, j, GetTableContent(datas, row - 1, j));
                            }
                            RefreshTable(CurrentTable, false);
                        }
                    }
                }

                if (row != TargetRow)
                {
                    VScrollBar scroll = ControlScroller[CurrentTable];
                    int val = scroll.Value;
                    if (TargetRow > 0 && TargetRow < CurrentTable.Items.Count - 1)
                        scroll.Value = Math.Min(Math.Max(scroll.Value + (shifting ? -1 : 1), scroll.Minimum), scroll.Maximum);
                    if (val != scroll.Value)
                        row = TargetRow;
                }
                if (row >= 0 && row < datas.Count)
                    ShowEditor(CurrentTable, col, row, false);
            }
            else if (e.KeyChar == (char)Keys.Escape)
            {
                Canceling = true;
                if (ModifierKeys != Keys.Control && CurrentTable == JBTablePembelian && TargetRow == CurrentTable.Items.Count - 1)
                {
                    List<List<string>> datas = TableData[CurrentTable];
                    for (int i = 1; i < JBTablePembelian.Columns.Count; i++)
                    {
                        SetTableContent(CurrentTable, datas, TargetRow, i, "");
                    }
                    RefreshTable(CurrentTable, false);
                }
                HideEditor(false);
                e.Handled = true;
            }
        }

        void OnDelete(Object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)8)
            {
                e.Handled = Canceling = true;
                SetTableContent(CurrentTable, GetDataSource(CurrentTable), TargetRow + ControlScroller[CurrentTable].Value, TargetCol, "");
                RefreshTable(CurrentTable, false);
                HideEditor(false);
            }
        }

        void OnLostFocus(object sender, EventArgs e)
        {
            HideEditor(!Canceling);
            Canceling = false;
        }

        void OnTableLostFocus(object sender, EventArgs e)
        {
            ListView obj = (ListView)sender;
            obj.SelectedItems.Clear();
        }

        void OnDragStart(Object sender, MouseEventArgs a)
        {
            if (a.Button == MouseButtons.Left)
            {
                IsDragging = true;
                ListView obj = (ListView)sender;
                if (obj != null)
                {
                    int colDex;
                    SelectedTable = obj;
                    GetTableSubItemAt(obj, a.Location, out DragStartRow, out colDex);
                    DragStartRow = DragEndRow = DragStartRow + ControlScroller[obj].Value;
                    SelectedIndexes.Clear();
                    SelectedIndexes.Add(DragStartRow);
                }
            }
        }

        void OnMouseDrag(Object sender, MouseEventArgs a)
        {
            if (IsDragging)
            {
                ListView obj = (ListView)sender;
                if (obj == SelectedTable)
                {
                    VScrollBar scroll = ControlScroller[obj];
                    int colDex;
                    GetTableSubItemAt(obj, a.Location, out DragEndRow, out colDex);
                    DragEndRow += scroll.Value;
                    int start, finish;
                    start = Math.Min(DragStartRow, DragEndRow);
                    finish = Math.Max(DragStartRow, DragEndRow);
                    SelectedIndexes.Clear();
                    for (int i = start; i <= finish; i++)
                    {
                        SelectedIndexes.Add(i);
                    }
                    foreach (ListViewItem itm in obj.Items)
                    {
                        itm.Selected = SelectedIndexes.Contains(itm.Index + scroll.Value);
                    }

                    Point mousePos = obj.Parent.PointToClient(Cursor.Position);
                    if (mousePos.Y <  TABLE_ROW_SELECTION_AUTO_SCROLL_MARGIN)
                    {
                        scroll.Value = Math.Max(scroll.Value - 1, scroll.Minimum);
                    }
                    else if (mousePos.Y > obj.Parent.Height - TABLE_ROW_SELECTION_AUTO_SCROLL_MARGIN)
                    {
                        scroll.Value = Math.Min(scroll.Value + 1, scroll.Maximum);
                    }

                }
            }
        }

        void OnDragFinish(Object sender, MouseEventArgs a)
        {
            if (a.Button == MouseButtons.Left)
            {
                IsDragging = false;
                DragEndRow = DragStartRow = -1;
            }
        }

        void OnTableSelection(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            //ListView obj = (ListView)sender;
            //if (obj != SelectedTable)
            //    SelectedIndexes.Clear();
            //SelectedTable = obj;
            //if (e.IsSelected)
            //    SelectedIndexes.Add(e.ItemIndex);
            //else
            //    SelectedIndexes.Remove(e.ItemIndex);
        }

        void MainForm_Activated(object sender, EventArgs e)
        {
            if (FormBorderStyle != FormBorderStyle.None)
                FormBorderStyle = FormBorderStyle.None;
        }

        void ButtonMinimize_Click(object sender, EventArgs e)
        {
            FormBorderStyle = FormBorderStyle.Fixed3D;
            WindowState = FormWindowState.Minimized;
        }

        void ButtonClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        void DropdownUpdate(Object sender, KeyEventArgs e)
        {
            char c = (char)e.KeyCode;
            if (Char.IsDigit(c) || Char.IsLetter(c) || e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
            {
                string txt = dropdownField.Text;
                int selection = dropdownField.SelectionStart;
                dropdownField.DroppedDown = true;
                RefreshDropdownItems(CurrentTable, TargetCol, dropdownField.Text);
                if (txt.Length > 0)
                {
                    bool found = false;
                    for (int i = 0; i < dropdownField.Items.Count; i++)
                    {
                        if (dropdownField.Items[i].ToString().ToLower() == txt.ToLower())
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                        dropdownField.Items.Add(txt);
                }
                dropdownField.SelectedIndex = -1;
                dropdownField.Text = txt;
                dropdownField.SelectionStart = selection;
            }
        }

        private void OnTooltipClick(object sender, EventArgs e)
        {
            var box = sender as Button;
            if (box != null)
            {
                TooltipsKeyboard.Visible = !TooltipsKeyboard.Visible;
                if (TooltipsKeyboard.Visible)
                {
                    TooltipsKeyboard.Location = box.Parent.PointToScreen(new Point(box.Right - TooltipsKeyboard.Width, box.Bottom));
                    TooltipsKeyboard.BringToFront();
                }
            }
        }

        void TooltipLostFocus(object sender, EventArgs e)
        {
            TooltipsKeyboard.Visible = false;
        }

        void TimerRefreshFooter_Tick(object sender, EventArgs e)
        {
            JBLabelJumlahNettoBeli.Location = new Point(JBHeaderPembelian.TopItem.SubItems[7].Bounds.Left + JBHeaderPembelian.Left + 2, 0);
            JBLabelJumlahNettoBeli.MinimumSize = JBHeaderPembelian.TopItem.SubItems[7].Bounds.Size;
            JBLabelJumlahNettoBeli.MaximumSize = new Size(JBHeaderPembelian.TopItem.SubItems[7].Bounds.Width, JBHeaderPembelian.TopItem.SubItems[7].Bounds.Height * 2);
            JBLabelJumlahBeli.Location = new Point(JBHeaderPembelian.TopItem.SubItems[11].Bounds.Left + JBHeaderPembelian.Left + 2, 0);
            JBLabelJumlahBeli.MinimumSize = JBHeaderPembelian.TopItem.SubItems[11].Bounds.Size;
            JBLabelJumlahBeli.MaximumSize = new Size(JBHeaderPembelian.TopItem.SubItems[11].Bounds.Width, JBHeaderPembelian.TopItem.SubItems[11].Bounds.Height * 2);
            JBLabelJumlahNettoJual.Location = new Point(JBHeaderPenjualan.TopItem.SubItems[4].Bounds.Left + JBHeaderPenjualan.Left + 2, 0);
            JBLabelJumlahNettoJual.MinimumSize = JBHeaderPenjualan.TopItem.SubItems[4].Bounds.Size;
            JBLabelJumlahNettoJual.MaximumSize = new Size(JBHeaderPenjualan.TopItem.SubItems[4].Bounds.Width, JBHeaderPenjualan.TopItem.SubItems[4].Bounds.Height * 2);
            JBLabelJumlahJual.Location = new Point(JBHeaderPenjualan.TopItem.SubItems[6].Bounds.Left + JBHeaderPenjualan.Left + 2, 0);
            JBLabelJumlahJual.MinimumSize = JBHeaderPenjualan.TopItem.SubItems[6].Bounds.Size;
            JBLabelJumlahJual.MaximumSize = new Size(JBHeaderPenjualan.TopItem.SubItems[6].Bounds.Width, JBHeaderPenjualan.TopItem.SubItems[6].Bounds.Height * 2);
            HPLabelJumlahHutang.Location = new Point(HPHeaderHutang.TopItem.SubItems[4].Bounds.Left + HPHeaderHutang.Left + 2, 0);
            HPLabelJumlahHutang.MinimumSize = HPHeaderHutang.TopItem.SubItems[4].Bounds.Size;
            HPLabelJumlahHutang.MaximumSize = new Size(HPHeaderHutang.TopItem.SubItems[4].Bounds.Width, HPHeaderHutang.TopItem.SubItems[4].Bounds.Height * 2);
            HPLabelJumlahSisaHutang.Location = new Point(HPHeaderHutang.TopItem.SubItems[5].Bounds.Left + HPHeaderHutang.Left + 2, 0);
            HPLabelJumlahSisaHutang.MinimumSize = HPHeaderHutang.TopItem.SubItems[5].Bounds.Size;
            HPLabelJumlahSisaHutang.MaximumSize = new Size(HPHeaderHutang.TopItem.SubItems[5].Bounds.Width, HPHeaderHutang.TopItem.SubItems[5].Bounds.Height * 2);
            HPLabelJumlahPiutang.Location = new Point(HPHeaderPiutang.TopItem.SubItems[4].Bounds.Left + HPHeaderPiutang.Left + 2, 0);
            HPLabelJumlahPiutang.MinimumSize = HPHeaderPiutang.TopItem.SubItems[4].Bounds.Size;
            HPLabelJumlahPiutang.MaximumSize = new Size(HPHeaderPiutang.TopItem.SubItems[4].Bounds.Width, HPHeaderPiutang.TopItem.SubItems[4].Bounds.Height * 2);
            HPLabelJumlahSisaPiutang.Location = new Point(HPHeaderPiutang.TopItem.SubItems[6].Bounds.Left + HPHeaderPiutang.Left + 2, 0);
            HPLabelJumlahSisaPiutang.MinimumSize = HPHeaderPiutang.TopItem.SubItems[6].Bounds.Size;
            HPLabelJumlahSisaPiutang.MaximumSize = new Size(HPHeaderPiutang.TopItem.SubItems[6].Bounds.Width, HPHeaderPiutang.TopItem.SubItems[6].Bounds.Height * 2);
        }

        void WarningTimerExpired(object sender, EventArgs e)
        {
            var t = sender as Timer;
            if (WarningMessages.ContainsKey(t))
            {
                Label msg = WarningMessages[t];
                if (msg != null)
                    msg.Dispose();
                WarningMessages.Remove(t);
                t.Dispose();
            }
        }

        void MessageMouseEnter(object sender, EventArgs e)
        {
            var msg = sender as Label;
            msg.Dispose();
        }

        void OnPrintDocument(object sender, PrintPageEventArgs e)
        {
            int visibleArea = e.MarginBounds.Height - PrintHeaderImage.Height;
            int visibleCt = visibleArea / TableRowHeight;
            e.Graphics.DrawImage(PrintHeaderImage, new Rectangle(e.MarginBounds.Left, e.MarginBounds.Top, e.MarginBounds.Width, PrintHeaderImage.Height));
            if (PrintExtraPage)
            {
                e.HasMorePages = false;
            }
            else
            {
                //print("print page " + PrintPageIndex + " : " + PrintPanelImage[PrintPageIndex].Height);
                e.Graphics.DrawImage(PrintPanelImage[PrintPageIndex], new Rectangle(e.MarginBounds.Left, e.MarginBounds.Top + PrintHeaderImage.Height, e.MarginBounds.Width, PrintPanelImage[PrintPageIndex].Height));
                e.HasMorePages = PrintPageIndex < PrintPanelImage.Count - 1;
            }
            if (!e.HasMorePages && PrintFooterImage != null)
            {
                if (PrintExtraPage)
                    e.Graphics.DrawImage(PrintFooterImage, new Rectangle(e.MarginBounds.Left, e.MarginBounds.Top + PrintHeaderImage.Height, e.MarginBounds.Width, PrintFooterImage.Height));
                else
                {
                    int mod = (PrintTotalHeight % (TableRowHeight * visibleCt));
                    //print(mod + "");
                    int placementY = e.MarginBounds.Top + PrintHeaderImage.Height + mod;
                    if (mod == 0 || placementY + PrintFooterImage.Height > e.MarginBounds.Bottom && !PrintExtraPage)
                        e.HasMorePages = PrintExtraPage = true;
                    else
                        e.Graphics.DrawImage(PrintFooterImage, new Rectangle(e.MarginBounds.Left, placementY, e.MarginBounds.Width, PrintFooterImage.Height));
                }
            }
            PrintPageIndex++;
            string txt = "Hal. " + PrintPageIndex;
            Size txtSize = TextRenderer.MeasureText(txt, JBLabelSisa.Font);
            e.Graphics.DrawString(txt, JBLabelSisa.Font, Brushes.Black, new RectangleF(e.MarginBounds.Right - txtSize.Width - 15, e.MarginBounds.Top + 10, txtSize.Width + 15, txtSize.Height));
            txt = "Tgl. " + DateTime.Now.ToShortDateString();
            txtSize = TextRenderer.MeasureText(txt, JBLabelSisa.Font);
            e.Graphics.DrawString(txt, JBLabelSisa.Font, Brushes.Black, new RectangleF(e.MarginBounds.Left + CONTENT_LEFT_MARGIN, e.MarginBounds.Top + 10, txtSize.Width + 15, txtSize.Height));
            if (!e.HasMorePages)
            {
                PrintExtraPage = false;
                PrintPageIndex = 0;
            }
        }

        private void JBButtonLaporan_Click(object sender, EventArgs e)
        {
            PrintPanel(JBPanelContent, JBPanelHeader, JBPanelFooter);
        }

        private void JBButtonAutoSize_Click(object sender, EventArgs e)
        {
            AutoSizeTableColumnWidths(JBHeaderPembelian, JBTablePembelian, true);
            AutoSizeTableColumnWidths(JBHeaderPenjualan, JBTablePenjualan, true);
        }

        private void HPButtonLaporan_Click(object sender, EventArgs e)
        {
            PrintPanel(HPPanelContent, HPPanelHeader, HPPanelFooter);
        }

        private void HPButtonAutoSize_Click(object sender, EventArgs e)
        {
            AutoSizeTableColumnWidths(HPHeaderHutang, HPTableHutang, true);
            AutoSizeTableColumnWidths(HPHeaderPiutang, HPTablePiutang, true);
        }

        void SearchTableIndex(Object sender, KeyEventArgs e)
        {
            if (e.Alt && !Searching)
            {
                char c = (char)e.KeyCode;
                bool operate = false;
                if (Char.IsDigit(c) && (SearchIndexInput + "").Length < 3)
                {
                    SearchIndexInput = Convert.ToInt32(SearchIndexInput + "" + c);
                    operate = true;
                }
                else if (Char.IsUpper(c) && SearchLetterInput == 0 && SearchIndexInput == 0)
                {
                    SearchLetterInput += NOTA_PREFIXES.IndexOf(c) * 1000;
                    operate = true;
                }
                if (operate)
                {
                    SearchIndexTable = sender as ListView;
                    SearchIndexReset.Stop();
                    SearchIndexReset.Start();
                    e.Handled = true;
                }
            }
        }

        void ResetSearchTableIndexInput(object sender, EventArgs e)
        {
            SearchIndexReset.Stop();
            SearchIndexInput += SearchLetterInput;
            List<List<string>> datas = TableData[SearchIndexTable];
            if (SearchIndexInput >= 27000)
                SearchIndexInput = 26999;
            else if (SearchIndexInput >= datas.Count)
            {
                if (SearchIndexTable == JBTablePembelian)
                {
                    List<List<string>> datas2 = TableData[JBTablePenjualan];
                    for (int i = datas.Count; i < SearchIndexInput - 1; i++)
                    {
                        string index = SearchIndexTable == JBTablePembelian ? ConvertToNoNota(datas.Count + 1) : SearchIndexTable == JBTablePenjualan ? "" : (datas.Count + 1 + "");
                        SetTableContent(SearchIndexTable, datas, datas.Count, 0, index + "");
                        if (SearchIndexTable == JBTablePembelian)
                        {
                            SetTableContent(JBTablePenjualan, datas2, datas2.Count, 0, "");
                        }
                    }
                    InsertEmptyToTable(SearchIndexTable, false);
                }
                SearchIndexInput = datas.Count;
            }

            VScrollBar bar = ControlScroller[SearchIndexTable];
            bar.Value = Math.Min(Math.Max(SearchIndexInput - 1, bar.Minimum), bar.Maximum);
            SearchIndexTable.SelectedIndices.Clear();
            SearchIndexTable.SelectedIndices.Add(Math.Max(SearchIndexInput - bar.Value - 1, 0));
            SearchLetterInput = 0;
            SearchIndexInput = 0;
            SearchIndexTable = null;
        }

        private void JBButtonFilter_Click(object sender, EventArgs e)
        {
            JBPanelFilter.Visible = !JBPanelFilter.Visible;
            if (JBPanelFilter.Visible)
            {
                JBButtonFilter.BackColor = Color.LightBlue;
                JBPanelButton.Height += JBPanelFilter.Height;
            }
            else
            {
                bool searched = Searching;
                Searching = false;
                if (searched)
                {
                    TotalUpdateTable(JBTablePembelian);
                    TotalUpdateTable(JBTablePenjualan);
                }
                JBButtonFilter.BackColor = Color.White;
                JBPanelButton.Height -= JBPanelFilter.Height;
            }
            RescaleTabJualBeli();
            RefreshTable(JBTablePembelian, false);
            RefreshTable(JBTablePenjualan, false);
            JBScroll.Maximum = Math.Max(TableData[JBTablePembelian].Count - (JBPanelContent.Height / TableRowHeight), JBScroll.Minimum);
            JBScroll.Visible = JBScroll.Maximum > JBScroll.Minimum;
        }

        private void JBFilterMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (JBFilterMode.SelectedIndex == 0)
            {
                JBFilterGroup1.Visible = true;
                JBFilterGroup2.Visible = false;
            }
            else
            {
                JBFilterGroup1.Visible = false;
                JBFilterGroup2.Visible = true;
            }
            JBFilterGroup1Opt1.Checked = true;
            JBFilterGroup2Opt1.Checked = true;
        }

        private void JBFilterButtonSearch_Click(object sender, EventArgs e)
        {
            if (!Searching)
            {
                JBFJumlahCountBU = JBFJumlahCount;
                JBFJumlahBU = JBFJumlah;
                JBFNettoBeliBU = JBFNettoBeli;
                JBFLabaCountBU = JBFLabaCount;
                JBFLabaBU = JBFLaba;
                JBFRugiCountBU = JBFRugiCount;
                JBFRugiBU = JBFRugi;
                JBFLabaBersihBU = JBFLabaBersih;
                JBFNettoJualBU = JBFNettoJual;
                JBFJumlahJualBU = JBFJumlahJual;
                JBFJumlahJualCountBU = JBFJumlahJualCount;
            }
            Searching = true;

            JBFJumlahCount = 0;
            JBFJumlah = 0;
            JBFNettoBeli = 0;
            JBFLabaCount = 0;
            JBFLaba = 0;
            JBFRugiCount = 0;
            JBFRugi = 0;
            JBFLabaBersih = 0;
            JBFNettoJual = 0;
            JBFJumlahJual = 0;
            JBFJumlahJualCount = 0;

            if (JBFilterMode.SelectedIndex == 0)
            {
                if (JBFilterGroup1Opt1.Checked)
                    FilterTable(JBTablePembelian, SearchMethod.Date, Convert.ToDateTime(JBFilterPicker1.Text), Convert.ToDateTime(JBFilterPicker2.Text));
                else if (JBFilterGroup1Opt2.Checked)
                    FilterTable(JBTablePembelian, SearchMethod.Keyword, 2, JBFilterDropdown.Text);
                else if (JBFilterGroup1Opt3.Checked)
                    FilterTable(JBTablePembelian, SearchMethod.Keyword, 4, JBFilterDropdown.Text);
                else if (JBFilterGroup1Opt4.Checked)
                {
                    int from = (JBFilterBox1Block1.Text.Length > 0 ? NOTA_PREFIXES.IndexOf(JBFilterBox1Block1.Text[0]) : 0) * 1000 + (JBFilterBox1Block2.Text.Length > 0 ? Convert.ToInt32(JBFilterBox1Block2.Text) : 0);
                    int to = (JBFilterBox2Block1.Text.Length > 0 ? NOTA_PREFIXES.IndexOf(JBFilterBox2Block1.Text[0]) : 0) * 1000 + (JBFilterBox2Block2.Text.Length > 0 ? Convert.ToInt32(JBFilterBox2Block2.Text) : 0);
                    if (to == 0)
                        to = ((JBFilterBox1Block1.Text.Length > 0 ? NOTA_PREFIXES.IndexOf(JBFilterBox1Block1.Text[0]) : 0) + 1) * 1000 - 1;
                    FilterTable(JBTablePembelian, SearchMethod.Index, from, to);
                }
            }
            else
            {
                if (JBFilterGroup2Opt1.Checked)
                    FilterTable(JBTablePenjualan, SearchMethod.Date, Convert.ToDateTime(JBFilterPicker1.Text), Convert.ToDateTime(JBFilterPicker2.Text));
                else if (JBFilterGroup2Opt2.Checked)
                    FilterTable(JBTablePenjualan, SearchMethod.Keyword, 1, JBFilterDropdown.Text);
                else if (JBFilterGroup2Opt3.Checked)
                {
                    int from = (JBFilterBox1Block1.Text.Length > 0 ? NOTA_PREFIXES.IndexOf(JBFilterBox1Block1.Text[0]) : 0) * 1000 + (JBFilterBox1Block2.Text.Length > 0 ? Convert.ToInt32(JBFilterBox1Block2.Text) : 0);
                    int to = (JBFilterBox2Block1.Text.Length > 0 ? NOTA_PREFIXES.IndexOf(JBFilterBox2Block1.Text[0]) : 0) * 1000 + (JBFilterBox2Block2.Text.Length > 0 ? Convert.ToInt32(JBFilterBox2Block2.Text) : 0);
                    if (to == 0)
                        to = ((JBFilterBox1Block1.Text.Length > 0 ? NOTA_PREFIXES.IndexOf(JBFilterBox1Block1.Text[0]) : 0) + 1) * 1000 - 1;
                    FilterTable(JBTablePenjualan, SearchMethod.Index, from, to);
                }
            }
        }

        void JBFilterRadioBoxChecked(object sender, EventArgs e)
        {
            var button = sender as RadioButton;
            if (button.Checked)
            {
                JBFilterPicker1.Visible = false;
                JBFilterLabel4.Visible = false;
                JBFilterPicker2.Visible = false;
                JBFilterPicker1.Visible = false;
                JBFilterDropdown.Visible = false;
                JBFilterBox1Block1.Visible = false;
                JBFilterBox1Block2.Visible = false;
                JBFilterBox2Block1.Visible = false;
                JBFilterBox2Block2.Visible = false;
                if (button == JBFilterGroup1Opt1 || button == JBFilterGroup2Opt1)
                {
                    JBFilterLabel3.Text = "Dari:";
                    JBFilterLabel4.Text = "Sampai:";
                    JBFilterPicker1.Location = new Point(JBFilterLabel3.Right + GENERAL_SPACING, GENERAL_SPACING);
                    JBFilterLabel4.Location = new Point(JBFilterPicker1.Right + GENERAL_SPACING, GENERAL_SPACING);
                    JBFilterPicker2.Location = new Point(JBFilterLabel4.Right + GENERAL_SPACING, GENERAL_SPACING);
                    JBFilterButtonSearch.Location = new Point(JBFilterPicker2.Right + 25, GENERAL_SPACING);
                    JBFilterPicker1.Visible = true;
                    JBFilterPicker2.Visible = true;
                    JBFilterLabel4.Visible = true;
                }
                else if (button == JBFilterGroup1Opt2 || button == JBFilterGroup1Opt3 || button == JBFilterGroup2Opt2)
                {
                    JBFilterLabel3.Text = "Nama:";
                    JBFilterDropdown.Location = new Point(JBFilterLabel3.Right + GENERAL_SPACING, GENERAL_SPACING);
                    JBFilterButtonSearch.Location = new Point(JBFilterDropdown.Right + 25, GENERAL_SPACING);
                    JBFilterDropdown.Visible = true;
                    JBFilterDropdown.Text = "";
                    JBFilterDropdown.Items.Clear();
                    Dictionary<int, KeyValuePair<List<List<string>>, int>> source = null;
                    KeyValuePair<List<List<string>>, int> target = default(KeyValuePair<List<List<string>>, int>);
                    if (button == JBFilterGroup1Opt2)
                    {
                        source = DataSources[JBTablePembelian];
                        target = source[2];
                    }
                    else if (button == JBFilterGroup1Opt3)
                    {
                        source = DataSources[JBTablePembelian];
                        target = source[4];
                    }

                    if (source != null)
                    {
                        for (int i = 0; i < target.Key.Count - 1; i++)
                        {
                            string txt = target.Key[i][target.Value].ToLower();
                            if (txt.Length > 0)
                                JBFilterDropdown.Items.Add(target.Key[i][target.Value]);
                        }
                    }
                }
                else if (button == JBFilterGroup1Opt4 || button == JBFilterGroup2Opt3)
                {
                    JBFilterLabel3.Text = "Dari:";
                    JBFilterLabel4.Text = "Sampai:";
                    JBFilterBox1Block1.Location = new Point(JBFilterLabel3.Right + GENERAL_SPACING, GENERAL_SPACING);
                    JBFilterBox1Block2.Location = new Point(JBFilterBox1Block1.Right + GENERAL_SPACING, GENERAL_SPACING);
                    JBFilterLabel4.Location = new Point(JBFilterBox1Block2.Right + GENERAL_SPACING, GENERAL_SPACING);
                    JBFilterBox2Block1.Location = new Point(JBFilterLabel4.Right + GENERAL_SPACING, GENERAL_SPACING);
                    JBFilterBox2Block2.Location = new Point(JBFilterBox2Block1.Right + GENERAL_SPACING, GENERAL_SPACING);
                    JBFilterButtonSearch.Location = new Point(JBFilterBox2Block2.Right + 25, GENERAL_SPACING);
                    JBFilterBox1Block1.Visible = true;
                    JBFilterBox1Block2.Visible = true;
                    JBFilterBox2Block1.Visible = true;
                    JBFilterBox2Block2.Visible = true;
                    JBFilterBox1Block1.Text = "";
                    JBFilterBox1Block2.Text = "";
                    JBFilterBox2Block1.Text = "";
                    JBFilterBox2Block2.Text = "";
                    JBFilterLabel4.Visible = true;
                }
            }
        }

        void HPFilterRadioBoxChecked(object sender, EventArgs e)
        {
            var button = sender as RadioButton;
            if (button.Checked)
            {
                JBFilterPicker1.Visible = false;
                JBFilterLabel4.Visible = false;
                JBFilterPicker2.Visible = false;
                JBFilterPicker1.Visible = false;
                JBFilterDropdown.Visible = false;
                JBFilterBox1Block1.Visible = false;
                JBFilterBox1Block2.Visible = false;
                JBFilterBox2Block1.Visible = false;
                JBFilterBox2Block2.Visible = false;
                if (button == JBFilterGroup1Opt1 || button == JBFilterGroup2Opt1)
                {
                    JBFilterLabel3.Text = "Dari:";
                    JBFilterLabel4.Text = "Sampai:";
                    JBFilterPicker1.Location = new Point(JBFilterLabel3.Right + GENERAL_SPACING, GENERAL_SPACING);
                    JBFilterLabel4.Location = new Point(JBFilterPicker1.Right + GENERAL_SPACING, GENERAL_SPACING);
                    JBFilterPicker2.Location = new Point(JBFilterLabel4.Right + GENERAL_SPACING, GENERAL_SPACING);
                    JBFilterButtonSearch.Location = new Point(JBFilterPicker2.Right + 25, GENERAL_SPACING);
                    JBFilterPicker1.Visible = true;
                    JBFilterPicker2.Visible = true;
                    JBFilterLabel4.Visible = true;
                }
                else if (button == JBFilterGroup1Opt2 || button == JBFilterGroup1Opt3 || button == JBFilterGroup2Opt2)
                {
                    JBFilterLabel3.Text = "Nama:";
                    JBFilterDropdown.Location = new Point(JBFilterLabel3.Right + GENERAL_SPACING, GENERAL_SPACING);
                    JBFilterButtonSearch.Location = new Point(JBFilterDropdown.Right + 25, GENERAL_SPACING);
                    JBFilterDropdown.Visible = true;
                    JBFilterDropdown.Text = "";
                    JBFilterDropdown.Items.Clear();
                    Dictionary<int, KeyValuePair<List<List<string>>, int>> source = null;
                    KeyValuePair<List<List<string>>, int> target = default(KeyValuePair<List<List<string>>, int>);
                    if (button == JBFilterGroup1Opt2)
                    {
                        source = DataSources[JBTablePembelian];
                        target = source[2];
                    }
                    else if (button == JBFilterGroup1Opt3)
                    {
                        source = DataSources[JBTablePembelian];
                        target = source[4];
                    }

                    if (source != null)
                    {
                        for (int i = 0; i < target.Key.Count - 1; i++)
                        {
                            string txt = target.Key[i][target.Value].ToLower();
                            if (txt.Length > 0)
                                JBFilterDropdown.Items.Add(target.Key[i][target.Value]);
                        }
                    }
                }
                else if (button == JBFilterGroup1Opt4 || button == JBFilterGroup2Opt3)
                {
                    JBFilterLabel3.Text = "Dari:";
                    JBFilterLabel4.Text = "Sampai:";
                    JBFilterBox1Block1.Location = new Point(JBFilterLabel3.Right + GENERAL_SPACING, GENERAL_SPACING);
                    JBFilterBox1Block2.Location = new Point(JBFilterBox1Block1.Right + GENERAL_SPACING, GENERAL_SPACING);
                    JBFilterLabel4.Location = new Point(JBFilterBox1Block2.Right + GENERAL_SPACING, GENERAL_SPACING);
                    JBFilterBox2Block1.Location = new Point(JBFilterLabel4.Right + GENERAL_SPACING, GENERAL_SPACING);
                    JBFilterBox2Block2.Location = new Point(JBFilterBox2Block1.Right + GENERAL_SPACING, GENERAL_SPACING);
                    JBFilterButtonSearch.Location = new Point(JBFilterBox2Block2.Right + 25, GENERAL_SPACING);
                    JBFilterBox1Block1.Visible = true;
                    JBFilterBox1Block2.Visible = true;
                    JBFilterBox2Block1.Visible = true;
                    JBFilterBox2Block2.Visible = true;
                    JBFilterBox1Block1.Text = "";
                    JBFilterBox1Block2.Text = "";
                    JBFilterBox2Block1.Text = "";
                    JBFilterBox2Block2.Text = "";
                    JBFilterLabel4.Visible = true;
                }
            }
        }

        void OnCrashDetected(object sender, UnhandledExceptionEventArgs ex)
        {
            string file = APP_PATH + "Errors";
            if (!Directory.Exists(file))
            {
                Directory.CreateDirectory(file);
            }
            file += "\\exception-" + DateTime.Now.Ticks + ".txt";
            
            DialogResult result = MessageBox.Show("Error telah terdeteksi dan program harus ditutup. Program akan mencoba untuk memulihkan data yang belum tersimpan pada sesi ini ketika program dijalankan kembali. Catatan error telah disimpan pada:\n\n" + file + "\n\nLaporkan dan kirimkan file tersebut kepada developer supaya error ini dapat diperbaiki.\n\nTekan \"Ok\" untuk membuka lokasi file tersebut.", "Peringatan", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
            if (result == DialogResult.OK)
            {
                System.Diagnostics.Process.Start("explorer.exe", "/select," + @file);
            }

            if (!File.Exists(file))
            {
                File.Create(file).Close();
            }

            StreamWriter writer = new StreamWriter(file);
            writer.Write(""); // Clear file
            writer.WriteLine("---------------");
            writer.WriteLine("Activity log");
            writer.WriteLine("---------------");
            writer.WriteLine("");
            writer.WriteLine("---------------");
            writer.WriteLine("Error message");
            writer.WriteLine("---------------");
            writer.WriteLine(ex.ExceptionObject.ToString());
            writer.Close();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            var result = MessageBox.Show("Keluar dari program sekaligus menyimpan data?\n\n\"Yes\" untuk keluar dan simpan.\n\"No\" untuk keluar tanpa menyimpan data.\n\"Cancel\" untuk batal.", "Konfirmasi", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            if (result == DialogResult.Cancel)
            {
                e.Cancel = true;
            }
            else if (result == DialogResult.No)
            {
                var result2 = MessageBox.Show("Anda hendak keluar dari program tanpa menyimpan data yang belum tersimpan (jika ada). Data-data yang belum tersimpan tesebut akan dihapus secara permanen. Lanjutkan?", "Konfirmasi", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (result2 == DialogResult.Yes)
                    HandleApplicationExit(false);
                else
                    e.Cancel = true;
            }
            else
            {
                HandleApplicationExit(true);
            }
        }

        private void JBButtonRefresh_Click(object sender, EventArgs e)
        {
            SaveTableData(JBTablePembelian);
            SaveTableData(JBTablePenjualan);
            MessageBox.Show("Data pembelian dan penjualan telah disimpan.", "Informasi", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void HPButtonFilter_Click(object sender, EventArgs e)
        {
            HPPanelFilter.Visible = !HPPanelFilter.Visible;
            if (HPPanelFilter.Visible)
            {
                HPButtonFilter.BackColor = Color.LightBlue;
                HPPanelButton.Height += HPPanelFilter.Height;
            }
            else
            {
                bool searched = Searching;
                Searching = false;
                if (searched)
                {
                    TotalUpdateTable(HPTableHutang);
                    TotalUpdateTable(HPTablePiutang);
                }
                HPButtonFilter.BackColor = Color.White;
                HPPanelButton.Height -= HPPanelFilter.Height;
            }
            RescaleTabHutangPiutang();
            RefreshTable(HPTableHutang, false);
            RefreshTable(HPTablePiutang, false);
            HPScroll.Maximum = Math.Max(Math.Max(TableData[HPTableHutang].Count, TableData[HPTablePiutang].Count) - (HPPanelContent.Height / TableRowHeight), HPScroll.Minimum);
            HPScroll.Visible = HPScroll.Maximum > HPScroll.Minimum;
        }

        private void HPButtonSimpan_Click(object sender, EventArgs e)
        {
            SaveTableData(HPTableHutang);
            SaveTableData(HPTablePiutang);
            MessageBox.Show("Data hutang dan piutang telah disimpan.", "Informasi", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }

    public class Simple3Des
    {
        public static string Encrypt(string toEncrypt, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);
            //MessageBox.Show(key);
            //If hashing use get hashcode regards to your key
            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(MainForm.ENCRYPTION_KEY));
                //Always release the resources and flush data
                // of the Cryptographic service provide. Best Practice

                hashmd5.Clear();
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(MainForm.ENCRYPTION_KEY);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            //set the secret key for the tripleDES algorithm
            tdes.Key = keyArray;
            //mode of operation. there are other 4 modes.
            //We choose ECB(Electronic code Book)
            tdes.Mode = CipherMode.ECB;
            //padding mode(if any extra byte added)

            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateEncryptor();
            //transform the specified region of bytes array to resultArray
            byte[] resultArray =
              cTransform.TransformFinalBlock(toEncryptArray, 0,
              toEncryptArray.Length);
            //Release resources held by TripleDes Encryptor
            tdes.Clear();
            //Return the encrypted data into unreadable string format
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        public static string Decrypt(string cipherString, bool useHashing)
        {
            byte[] keyArray;
            //get the byte code of the string

            byte[] toEncryptArray = Convert.FromBase64String(cipherString);

            if (useHashing)
            {
                //if hashing was used get the hash code with regards to your key
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(MainForm.ENCRYPTION_KEY));
                //release any resource held by the MD5CryptoServiceProvider

                hashmd5.Clear();
            }
            else
            {
                //if hashing was not implemented get the byte code of the key
                keyArray = UTF8Encoding.UTF8.GetBytes(MainForm.ENCRYPTION_KEY);
            }

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            //set the secret key for the tripleDES algorithm
            tdes.Key = keyArray;
            //mode of operation. there are other 4 modes. 
            //We choose ECB(Electronic code Book)

            tdes.Mode = CipherMode.ECB;
            //padding mode(if any extra byte added)
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(
                                 toEncryptArray, 0, toEncryptArray.Length);
            //Release resources held by TripleDes Encryptor                
            tdes.Clear();
            //return the Clear decrypted TEXT
            return UTF8Encoding.UTF8.GetString(resultArray);
        }
    }

    public class TransparentPanel : Panel
    {
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x00000020; // WS_EX_TRANSPARENT
                return cp;
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.FillRectangle(new SolidBrush(this.BackColor), this.ClientRectangle);
        }
    }
    #endregion
}